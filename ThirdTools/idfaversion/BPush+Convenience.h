//
//  BPush+Convenience.h
//  DataStatistics
//
//  Created by Kang on 16/5/6.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "BPush.h"

@interface BPush (Convenience)

/**
 *  毋庸置疑 --- 删除全部标签
 */
+ (void)YTYangK_DelAllListTags;

@end
