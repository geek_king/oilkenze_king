//
//  BPush+Convenience.m
//  DataStatistics
//
//  Created by Kang on 16/5/6.
//  Copyright © 2016年 YTYangK. All rights reserved.
//  ... 还没想清楚。。。

#import "BPush+Convenience.h"

@implementation BPush (Convenience)


+ (void)YTYangK_DelAllListTags {
    
            [BPush listTagsWithCompleteHandler:^(id result, NSError *error) {
    
                NSMutableArray * tagAry = [[NSMutableArray alloc] init];
    
                NSUInteger tag_num = [[[result objectForKey:@"response_params"] objectForKey:@"tag_num"] integerValue];
                for (int i = 0; i < tag_num; i++ ){
                    NSArray *ary = [[NSArray alloc] initWithArray:[[result objectForKey:@"response_params"] objectForKey:@"tags"]];
                    [tagAry addObject:[ary[i] objectForKey:@"name"]];
                }
                [BPush delTags:tagAry withCompleteHandler:^(id result, NSError *error) {
                    if (result) {
                        NSLog(@"tags进行全部删除成功!");
                    }else {
                        NSLog(@"tags删除错误 :%@",error.localizedDescription);
                    }
                    
                }];
            }];
}




@end
