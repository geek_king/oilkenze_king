//
//  DDRichTextViewController.m
//  WFCoretext
//
//  Created by David on 15/2/6.
//  Copyright (c) 2015年 tigerwf. All rights reserved.
//
#import "UIImageView+WebCache.h"
#import "DDRichTextViewController.h"

#define EMOJI_CODE_TO_SYMBOL(x) ((((0x808080F0 | (x & 0x3F000) >> 4) | (x & 0xFC0) << 10) | (x & 0x1C0000) << 18) | (x & 0x3F) << 24);



@interface DDRichTextViewController()<UITableViewDataSource,UITableViewDelegate,cellDelegate,InputDelegate>
{
//    UITableView *mainTable;
    UIButton *replyBtn;
    YMReplyInputView *replyView ;
    BOOL hideReply;
    BOOL returnValue;
}


@end

@implementation DDRichTextViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self initTableview];
}

#pragma mark - 计算高度
- (YMTextData*)calculateHeights:(YMTextData *)ymData{
    ymData.shuoshuoHeight = [ymData calculateShuoshuoHeightWithWidth:self.view.frame.size.width-8-50-8-12 withUnFoldState:NO];//折叠
    ymData.unFoldShuoHeight = [ymData calculateShuoshuoHeightWithWidth:self.view.frame.size.width-8-50-8-12 withUnFoldState:YES];//展开
    ymData.replyHeight = [ymData calculateReplyHeightWithWidth:self.view.frame.size.width];
    return ymData;
}


- (void) initTableview{
   self.mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height-10) style:UITableViewStyleGrouped];
    self.mainTable.backgroundColor = [UIColor groupTableViewBackgroundColor];
     self.mainTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.mainTable.backgroundColor = RGB(17, 18, 18, 1);
    self.mainTable.delegate = self;
    self.mainTable.dataSource = self;
    [self.view addSubview:self.mainTable];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return  [[self dataSource] numberOfRowsInDDRichText];
    return [[self dataSource] numberOfRowsInDDRichSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"------->>>%@",indexPath);
    YMTextData *ym = [self calculateHeights:[[self dataSource] dataForRowAtIndex:[indexPath row]]];
    BOOL unfold = ym.foldOrNot;
    CGFloat height = TableHeader+ kLocationToBottom + ym.replyHeight + ym.showImageHeight  + kDistance  + (unfold?ym.shuoshuoHeight:ym.unFoldShuoHeight) + kReplyBtnDistance+(ym.islessLimit?0:30);
	if ([self.delegate respondsToSelector:NSSelectorFromString(@"hideReplyButtonForIndex:")]) {
		if ([[self delegate] hideReplyButtonForIndex:indexPath.row]) {
			height -= 40;
		}
	}
    return  height;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"--->%@",indexPath);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"显示的是------>>%@",indexPath);
    static NSString *CellIdentifier = @"ILTableViewCell";
    YMTableViewCell *cell = (YMTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[YMTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = RGB(17, 18, 19, 1);
    }
    cell.stamp = indexPath.row;
    cell.replyBtn.tag = indexPath.row;
    cell.replyBtn.appendIndexPath = indexPath;
    [cell.replyBtn addTarget:self action:@selector(replyAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.delegate = self;
	if ([self.delegate respondsToSelector:NSSelectorFromString(@"hideReplyButtonForIndex:")]) {
		if ([[self delegate] hideReplyButtonForIndex:indexPath.row]) {
			cell.hideReply = YES;
		}
	}
    YMTextData *data = [[self dataSource] dataForRowAtIndex:[indexPath row]];
    if ([self.delegate respondsToSelector:NSSelectorFromString(@"hiddenDeleteBtnFromText:name:")]) {
        cell.deleteBtn.hidden =[self.delegate hiddenDeleteBtnFromText:indexPath.row name:data.name];
    }
	//这句话让头像 支持异步加载
	[cell.headerImage sd_setImageWithURL:[NSURL URLWithString:data.headPicURL] placeholderImage:[UIImage imageNamed:@"默认头像"]];
//    cell.headerImage.image = data.headPic;
    cell.nameLbl.text = data.name;
    cell.introLbl.text = data.intro;
    [cell setYMViewWith:data];
    return cell;
}


#pragma mark - 按钮动画

- (void)replyAction:(YMButton *)sender{
//    CGRect rectInTableView = [self.mainTable rectForRowAtIndexPath:sender.appendIndexPath];
//    float origin_Y = rectInTableView.origin.y + sender.frame.origin.y;
//    if (replyBtn) {
//        [UIView animateWithDuration:0.25f animations:^{
//            replyBtn.frame = CGRectMake(sender.frame.origin.x, origin_Y - 10 , 0, 38);
//        } completion:^(BOOL finished) {
//            NSLog(@"销毁");
//            [replyBtn removeFromSuperview];
//            replyBtn = nil;
//		}];
//    }else{
//        replyBtn = [UIButton buttonWithType:0];
//        replyBtn.layer.cornerRadius = 5;
//        replyBtn.backgroundColor = [UIColor colorWithRed:33/255.0 green:37/255.0 blue:38/255.0 alpha:0.8];
//        replyBtn.frame = CGRectMake(sender.frame.origin.x , origin_Y - 10 , 0, 38);
//        [replyBtn setTitleColor:[UIColor whiteColor] forState:0];
//        replyBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
//        replyBtn.tag = sender.tag;
//        [self.mainTable addSubview:replyBtn];
//        [replyBtn addTarget:self action:@selector(replyMessage:) forControlEvents:UIControlEventTouchUpInside];
//        [UIView animateWithDuration:0.25f animations:^{
//            replyBtn.frame = CGRectMake(sender.frame.origin.x - 60, origin_Y  - 10 , 60, 38);
//        } completion:^(BOOL finished) {
//            [replyBtn setTitle:@"评论" forState:0];
//        }];
//    }
    [self replyMessage:sender];
    
}

#pragma mark - 真の评论
- (void)replyMessage:(UIButton *)sender{
    //NSLog(@"TAG === %d",sender.tag);
    if (replyBtn){
        [replyBtn removeFromSuperview];
        replyBtn = nil;
    }
    // NSLog(@"alloc reply");
    
    replyView = [[YMReplyInputView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 44, screenWidth,44) andAboveView:self.view];
    replyView.delegate = self;
    replyView.replyTag = sender.tag;
    [self.view addSubview:replyView];
}


#pragma mark -移除评论按钮
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (replyBtn) {
        [replyBtn removeFromSuperview];
        replyBtn = nil;
    }
}


#pragma mark -cellDelegate
- (void)changeFoldState:(YMTextData *)ymD onCellRow:(NSInteger)cellStamp{
    YMTableViewCell *cell = (YMTableViewCell*)[self.mainTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:cellStamp]];
    [cell setYMViewWith:ymD];
    
    [self.mainTable reloadData];
}

//头像被点击
- (void)didHeadPicPressForIndex:(NSInteger)index{
	if ([self.delegate respondsToSelector:NSSelectorFromString(@"didPromulgatorPressForIndex:name:")]) {
		[self.delegate didPromulgatorPressForIndex:index name:@""];
	}
}

//头像或名字点击
- (void)didPromulgatorNameOrHeadPicPressedForIndex:(NSInteger)index name:(NSString *)name{
	if ([self.delegate respondsToSelector:NSSelectorFromString(@"didPromulgatorPressForIndex:name:")]) {
		[self.delegate didPromulgatorPressForIndex:index name:name];
	}
}

//正文点击回调
- (void)didRichTextPress:(NSString *)text index:(NSInteger)index{
	if ([self.delegate respondsToSelector:NSSelectorFromString(@"didRichTextPressedFromText:index:")]) {
		[self.delegate didRichTextPressedFromText:text index:index];
	}
}

//评论被点击回调
- (void)didRichTextPress:(NSString *)text index:(NSInteger)index replyIndex:(NSInteger)replyIndex{
    YMTableViewCell *cell = (YMTableViewCell *)[self.mainTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    YMTextData *ymData = [cell getYMTextData];
    //清空属性数组。否则会重复添加
    [ymData.completionReplySource removeAllObjects];
    [ymData.attributedData removeAllObjects];
	if ([self.delegate respondsToSelector:NSSelectorFromString(@"didRichTextPressedFromText:index:replyIndex:")]) {
#warning 这里修改过：--原来传text   --现在修改 [text stringByAppendingFormat:[[ymData replyDataSource] objectAtIndex:replyIndex]]
        NSString *str = [NSString stringWithFormat:@"%@",[ymData.replyDataSource objectAtIndex:replyIndex]];
        NSRange range = [str rangeOfString:@":"];//获取":"的位置
        NSString *subName = [str substringToIndex:range.location +
                       range.length -1];//开始截取
		[self.delegate didRichTextPressedFromText:[NSString stringWithFormat:@"%@->%@",text,subName] index:index replyIndex:replyIndex];
	}
}


//删除整条cell
- (void)didDeletelCellIndex:(UIButton *)index
{
    YMTableViewCell * cell =(YMTableViewCell *)[[index superview] superview];
    NSIndexPath *indexPath = [self.mainTable indexPathForCell:cell];

   
    if ([self.delegate respondsToSelector:NSSelectorFromString(@"deleteCellForindex:")]) {
        [self.delegate deleteCellForindex:indexPath];
    }

}

#pragma mark - 图片点击事件回调
- (void)showImageViewWithImageViews:(NSArray *)imageViews byClickWhich:(NSInteger)clickTag{
	[[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.tabBarController.tabBar setHidden:YES];
    UIView *maskview = [[UIView alloc] initWithFrame:self.view.bounds];
    maskview.backgroundColor = [UIColor blackColor];
    [self.view addSubview:maskview];
    YMShowImageView *ymImageV = [[YMShowImageView alloc] initWithFrame:self.view.bounds byClick:clickTag appendArray:imageViews];
    [ymImageV show:maskview didFinish:^(){
		[[self navigationController] setNavigationBarHidden:NO animated:YES];
        [[[self tabBarController] tabBar]setHidden:NO];
        [UIView animateWithDuration:0.5f animations:^{
            ymImageV.alpha = 0.0f;
            maskview.alpha = 0.0f;
        } completion:^(BOOL finished) {
            [ymImageV removeFromSuperview];
            [maskview removeFromSuperview];
        }];
        
    }];
}

#pragma mark - 评论说说回调
- (void)YMReplyInputWithReply:(NSString *)replyText appendTag:(NSInteger)inputTag{
    NSLog(@"评论说说回调");
    
    NSString *newString = [NSString stringWithFormat:@"%@:%@",[[self delegate] senderName],replyText];//此处可扩展。已写死，包括内部逻辑也写死 在YMTextData里 自行添加部分
    
    
    YMTableViewCell *cell = (YMTableViewCell*)[self.mainTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:inputTag inSection:0]];
    YMTextData *ymData = [cell getYMTextData];
    [ymData.replyDataSource addObject:newString];
    //清空属性数组。否则会重复添加
    [ymData.completionReplySource removeAllObjects];
    [ymData.attributedData removeAllObjects];
    
    NSString *rangeStr = NSStringFromRange(NSMakeRange(0, [[self delegate] senderName].length));
    NSMutableArray *rangeArr = [[NSMutableArray alloc] init];
    
    
    [rangeArr addObject:rangeStr];
    
    [ymData.defineAttrData addObject:rangeArr];
    ymData.replyHeight = [ymData calculateReplyHeightWithWidth:self.view.frame.size.width];
    [cell setYMViewWith:ymData];
    [self.mainTable reloadData];
    

	if ([self.delegate respondsToSelector:NSSelectorFromString(@"replyForIndex:replyText:")]) {
		[self.delegate replyForIndex:inputTag replyText:replyText];
	}
    
}





- (void)destorySelf{
    //  NSLog(@"dealloc reply");
    [replyView removeFromSuperview];
    replyView = nil;
}


- (void)dealloc{
    
}



@end
