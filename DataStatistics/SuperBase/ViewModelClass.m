//
//  ViewModelClass.m
//  DataStatistics
//
//  Created by Kang on 15/12/9.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "UtilToolsClss.h"
#import "MoreViewController.h"

@implementation ViewModelClass

static ViewModelClass *viewModelC = nil;
static dispatch_once_t viewModelToken;

+ (ViewModelClass *)sharedViewModelClss {
    dispatch_once(&viewModelToken, ^{
        if (viewModelC == nil) {
            viewModelC = [[super allocWithZone:NULL] init];
        }
    });
    return viewModelC;
}


#pragma mark - 获取网络状态
- (void)netWorkStateWithNetConnectBlock:(NetWorkBlock)netConnectBlock WithURlStr:(NSString *)strURL {
    BOOL netState = [NetRequestClss netWorkReachabilityWithURLString:strURL];
    netConnectBlock(netState);
}


#pragma mark - 请求二
+ (void)requestDataWithReturnUrl:(NSString *)url withViewController:(id)vc
                  withParameters:(NSDictionary *)par isMethod:(BOOL)met isListLoaded:(BOOL)loaded {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    if (!loaded) [[UtilToolsClss getUtilTools] addDoLoading];
    __weak typeof(ViewModelClass *) weakSelf = [ViewModelClass sharedViewModelClss];
    
    [NetRequestClss requestWithUrl:url requestWithParameters:par resultSuccess:^(id objs, int status, NSString *mag) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (!loaded) [[UtilToolsClss getUtilTools] removeDoLoading];
        NSLog(@"objs--->%@--->%d-->%@",objs,status,mag);
        
        [weakSelf requestDataWithValueSuccess:objs withViewController:vc judgeApiStrOrApiId:vc returnBlockVlue:status];

    } resultError:^(NSString *err) {
        //错误
    }];
    
    
    
}


#pragma mark - 强大请求
- (BOOL)requestDataWithReturnUrl:(NSString *)url withViewController:(UIViewController *)vc
                  withParameters:(NSDictionary *)par  requestWithHeader:(NSDictionary *)header  requestWithApiID:(id)api isMethod:(BOOL)met isListLoaded:(BOOL)loaded {

        [[NetRequestClss sharedNetRequestClss] requestWithUrl:url requestWithHeader:header requestWithParameters:par requestWithViewController:vc requestWithApiID:api isMethod:met isListLoaded:loaded result:^(id objs, int status, NSString *msg) {
             NSLog(@"objs--->%@--->%d-->%@",objs,status,msg);
        
          [self requestDataWithValueSuccess:objs withViewController:vc judgeApiStrOrApiId:api returnBlockVlue:status];
         }];
    
        return self.returnBlockVlue; // 这可以判断是否运行了这个方法。

}

#pragma mark 成功返回
 - (void)obtainDataWithValueSuccess:(NSDictionary *)returnValue withViewController:(UIViewController *)vc judgeApiStrOrApiId:(id)api {}

#pragma mark - block
- (void)setBlockWithApiBlock:(ApiBlock)apiBlock WithReceiveVlueBlock:(ReceiveVlueBlock)receive {
    _receivevlueblock = receive;
    _apiblock  = apiBlock;
}

// 登陆强制退出
- (void)forceJumpViewController:(UIViewController *)vcont {
    [MoreViewController logout:vcont];
    [MBProgressHUD yty_showErrorWithTitle:nil detailsText:NSLocalizedString(@"aborted", nil) toView:nil];
}



// 登陆强制退出 二
+ (NSString *)forceJumpViewStatus:(int)status forController:(UIViewController *)vcont {
        if (status == -2 || status == -1) {
            [MoreViewController logout:vcont];
            [MBProgressHUD yty_showErrorWithTitle:nil detailsText:NSLocalizedString(@"aborted", nil) toView:nil];
        }
        return @"---";
}


@end
