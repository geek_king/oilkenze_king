//
//  NetRequestClss.m
//  DataStatistics
//
//  Created by Kang on 15/12/9.
//  Copyright © 2015年 YTYangK. All rights reserved.
//
//  请求格式
//
/**
 要使用常规的AFN网络访问
 
 1. AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
 
 所有的网络请求,均有manager发起
 
 2. 需要注意的是,默认提交请求的数据是二进制的,返回格式是JSON
 
 1> 如果提交数据是JSON的,需要将请求格式设置为AFJSONRequestSerializer
 2> 如果返回格式不是JSON的,
 
 3. 请求格式
 
 AFHTTPRequestSerializer            二进制格式
 AFJSONRequestSerializer            JSON
 AFPropertyListRequestSerializer    PList(是一种特殊的XML,解析起来相对容易)
 
 4. 返回格式
 
 AFHTTPResponseSerializer           二进制格式
 AFJSONResponseSerializer           JSON
 AFXMLParserResponseSerializer      XML,只能返回XMLParser,还需要自己通过代理方法解析
 AFXMLDocumentResponseSerializer (Mac OS X)
 AFPropertyListResponseSerializer   PList
 AFImageResponseSerializer          Image
 AFCompoundResponseSerializer       组合
 */

#import "NetRequestClss.h"
#import "AFNetworking.h"
#import "JSONKit.h"
#import "AFHTTPSessionManager.h"
//#import "AFNetworkActivityIndicatorManager.h"
#import "UtilToolsClss.h"


@interface NetRequestClss ()
@end

@implementation NetRequestClss

static NetRequestClss  *instance = nil;
static dispatch_once_t onceToken;

/** 网络状态
 AFNetworkReachabilityStatusUnknown          = -1,  // 未知
 AFNetworkReachabilityStatusNotReachable     = 0,   // 无连接
 AFNetworkReachabilityStatusReachableViaWWAN = 1,   // 3G 花钱
 AFNetworkReachabilityStatusReachableViaWiFi = 2,   // 局域网络,不花钱
 */
static int netWorkState;

+ (NetRequestClss *)sharedNetRequestClss {
    dispatch_once(&onceToken, ^{
        if (instance == nil) {
            instance = [[super allocWithZone:NULL] init];
        }
    });
    return instance;
}

#pragma mark - 检测网络连接
// 如果要检测网络状态的变化,必须用检测管理器的单例的startMonitoring
+ (void)reach {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    // 检测网络连接的单例,网络变化时的回调方法
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        netWorkState = status;
    }];
}

/// 外调 监控网络
+ (void)netWorkReachabilityWithRequestBlock: (ReturnValueBlock)block {
    __block id number = @201600;
    // 创建网络监听者管理者对象
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
// 设置监听
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                number = @201601;
                NSLog(@"未识别的网络");
                [MBProgressHUD showError:NSLocalizedString(@"NetRClss1", nil)];
                break;
            case AFNetworkReachabilityStatusNotReachable:
                number = @201602;
                NSLog(@"不可达的网络(未连接)");
                [MBProgressHUD showError:NSLocalizedString(@"NetRClss1", nil)];
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                number = @201603;
                NSLog(@"2G,3G,4G...的网络");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                number = @201604;
                 NSLog(@"wifi的网络");
                break;
            default:
                number = @201605;
                NSLog(@"特殊情况");
                [MBProgressHUD showError:NSLocalizedString(@"NetRClss2", nil)];
                break;
        }
          block(number);
    }];
    // 开始监听
    [manager startMonitoring];
}
//+ (void)showNetworkActivityIndicatior {
//    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
//}


/// 外调-监控url
+ (BOOL)netWorkReachabilityWithURLString:(NSString *)strUrl {
    __block BOOL netState = NO;
    NSString *urlStr = [[NSString alloc] initWithFormat:@"%@%@",IPHEAD,strUrl];
    NSURL *baseURL = [NSURL URLWithString:urlStr];  // 接收链接
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    NSOperationQueue *operationQueue = manager.operationQueue; //纳秒运算队列
    
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                [operationQueue setSuspended:NO];
                netState = YES;
                break;
            //case AFNetworkReachabilityStatusUnknown:       //未知的网络
            case AFNetworkReachabilityStatusNotReachable:  //没有网络
                netState = NO;
               
                break;
            default:
                [operationQueue setSuspended:YES]; //暂停的
                break;
        }
       
    }];
    
    [manager.reachabilityManager startMonitoring];
     return netState;
   
}



/// 内调-监控网络
+ (BOOL)beforeExecute:(AFHTTPRequestOperationManager *)manager {
    NSOperationQueue *operationQueue = manager.operationQueue;
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
                [operationQueue setSuspended:NO];  //开启请求
                break;
            case AFNetworkReachabilityStatusNotReachable:
            default:
                [operationQueue setSuspended:YES]; //暂停请求
                break;
        }
    }];
    [manager.reachabilityManager startMonitoring];
    return true;
}

/// 监控提示
+ (NSString *)afterExecute:(NSError *)error addViewC:(UIViewController *)vc {
//    if (error == nil) {
//        return @"---";//true;
//    }
    
    if ([error code] == NSURLErrorNotConnectedToInternet) {
        
        // 网络不给力
       //@"网络不给力"
       // [MBProgressHUD showError:NSLocalizedString(@"NetRClss3701", nil)];
        
        return NSLocalizedString(@"NetRClss3701", nil);//false;
    }else if([error code] == NSURLErrorTimedOut) {
        
        // @"请求超时,请你稍后尝试。"
       //  [MBProgressHUD showError:NSLocalizedString(@"NetRClss3705", nil)];
        return NSLocalizedString(@"NetRClss3705", nil);
    }else if([error code] == NSURLErrorBadServerResponse) {
        
        // @"服务器异常,该网页不存在"
       //  [MBProgressHUD showError:NSLocalizedString(@"NetRClss3704", nil)];
        return NSLocalizedString(@"NetRClss3704", nil);
    }else {
        return NSLocalizedString(@"NetRClss3703", @"服务器维护，请稍后再试");
    }
    
    // 服务器故障，请稍后再试
    //@"服务器维护，请稍后再试"
    //[MBProgressHUD showError:NSLocalizedString(@"NetRClss3703", nil)];
    //return true;
}


#pragma mark - 复杂请求方法一
- (void)requestWithUrl:(NSString *)url requestWithHeader:(NSDictionary *)header requestWithParameters:(NSDictionary *)parameter requestWithViewController:(UIViewController *)vc requestWithApiID:(id)api isMethod:(BOOL)ismet isListLoaded:(BOOL)islist result:(void (^)(id objs, int status, NSString *msg))result {
    
    if (!islist)
    [[UtilToolsClss getUtilTools] addDoLoading];
    
    // 请求返回的信息
   __block NSString *requestStr = @"";
    // 可变 URL
    NSMutableURLRequest *requestUrl = [NetRequestClss ANetRequestWithURLString:url WithHeader:nil WithParameter:parameter WithHTTPMethod:@"GET"];
    
    // AF 管理者
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",requestUrl]]];
    // 监控网络
    if (![NetRequestClss beforeExecute:manager]) {
        return;
    }
   
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 3.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    // 请求操作
    AFHTTPRequestOperation *requstOperation = [[AFHTTPRequestOperation alloc] initWithRequest:requestUrl];      // 发送请求
    requstOperation.responseSerializer = [AFHTTPResponseSerializer serializer];                                 // 设置返回数据的解析方式
 
    [requstOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {

        // 转成Json字符串
        NSString  *jsonStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *jsonDic = [jsonStr objectFromJSONString];
        NSLog(@"获取json字符串：原型---->%@  Dic---%@",jsonStr,jsonDic);
        
        
        if (!islist) //不是列表
            [[UtilToolsClss getUtilTools] removeDoLoading];
        
        
       __weak NSString *response = jsonStr; // 获取页面内容
        
        if (jsonDic == nil) {
            self.obtainStatus = NetObtainDataStatusFail;
            requestStr = @"返回到了登陆页面";
            result(nil,self.obtainStatus,requestStr);  // -1
        }else {
            
        if([response rangeOfString:@"oilklenze后台登录界面"].location != NSNotFound || [response rangeOfString:@"oilklenze测试服务器后台登录界面"].location != NSNotFound ) {
            NSLog(@"----》包含");
            self.obtainStatus = NetObtainDataStatusFail;
            requestStr = @"返回到了登陆页面";
            result(nil,self.obtainStatus,requestStr);  // -1
        }else if([response rangeOfString:@"您的账号已在异地登录，是否重新登录"].location != NSNotFound) {
            NSLog(@"----》包含");
            self.obtainStatus = NetObtainDataStatusUnknownAlsoFail;
            requestStr = @"账号异地登陆";
            result(nil,self.obtainStatus,requestStr); // -2
            
        }else {
            NSLog(@"----》不包含");
            
            if (jsonDic != nil) {
                self.obtainStatus = NetObtainDataStatusSuccess;
                requestStr = @"请求成功!";
                result(jsonDic,self.obtainStatus,requestStr); // 1
            }else {
                self.obtainStatus = NetObtainDataStatusSuccessAlsoNotData;
                requestStr = @"请求成功!，获取不到任何数据";
                result(nil,self.obtainStatus,requestStr); //2
            }

        }
        }
        
        
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        if (!islist) //不是列表
            [[UtilToolsClss getUtilTools] removeDoLoading];
        
        NSLog(@"\n获取到错误，API错误，或服务器连接失败-->error is %@",error);
        self.obtainStatus = NetObtainDataStatusError;
        [NetRequestClss afterExecute:error addViewC:vc];
        requestStr = @"api异常！请检查您的服务器！";
        result(nil,self.obtainStatus,requestStr); // 0
    }];
    
    [requstOperation start];
}


#pragma mark - 请求方法二
+ (void)requestWithUrl:(NSString *)url requestWithParameters:(NSDictionary *)parameter resultSuccess:(void(^)(id objs, int status, NSString * mag))success resultError:(void(^)(NSString *err))error {
    
    __weak typeof (self)weakSelf = self;
    // 请求返回的信息
    __block NSString *requestStr = @"";
    // 可变 URL
    NSMutableURLRequest *requestUrl = [weakSelf ANetRequestWithURLString:url WithHeader:nil WithParameter:parameter WithHTTPMethod:@"GET"];
    
    // AF 管理者
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",requestUrl]]];
    
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 3.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    // 请求操作
    AFHTTPRequestOperation *requstOperation = [[AFHTTPRequestOperation alloc] initWithRequest:requestUrl];      // 发送请求
    requstOperation.responseSerializer = [AFHTTPResponseSerializer serializer];                                 // 设置返回数据的解析方式
    
    [requstOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSError *jsonError = nil;
        // 转成Json字符串
        NSString  *jsonStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&jsonError];
        NSLog(@"获取json字符串：原型---->%@  Dic---%@",jsonStr,jsonDic);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (jsonDic == nil) {
            requestStr = @"返回到了登陆页面";
            success(nil,NetObtainDataStatusFail,requestStr);  // -1
        }else {
            requestStr = jsonStr;
            if([requestStr rangeOfString:@"oilklenze后台登录界面"].location != NSNotFound || [requestStr rangeOfString:@"oilklenze测试服务器后台登录界面"].location != NSNotFound ) {
                requestStr = @"返回到了登陆页面";
                success(nil,NetObtainDataStatusFail,requestStr);  // -1
            }else if([requestStr rangeOfString:@"您的账号已在异地登录，是否重新登录"].location != NSNotFound) {
                requestStr = @"账号异地登陆";
                success(nil,NetObtainDataStatusUnknownAlsoFail,requestStr); // -2
                
            }else {
                //  NSLog(@"----》不包含");
                
                if (jsonDic != nil) {
                    requestStr = @"请求成功!";
                    success(jsonDic,NetObtainDataStatusSuccess,requestStr); // 1
                }else {
                    requestStr = @"获取不到任何数据";
                    success(nil,NetObtainDataStatusSuccessAlsoNotData,requestStr); //2
                }
                
            }
        }
        
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"\n获取到错误，API错误，或服务器连接失败-->error is %@",error);
        [NetRequestClss afterExecute:error addViewC:nil];
        requestStr = @"api异常！请检查您的服务器！";
        success(nil,NetObtainDataStatusError,requestStr); // 0
    }];
    
    [requstOperation start];
    
}



#pragma mark - 使用这个请求方法，其他不使用。
+ (void)requestWithUrl:(NSString *)url requestWithParameters:(NSDictionary *)par method:(NetMethod)met returnSuccess:(void(^)(id objs, int status, NSString *mag))success returnError:(void(^)(NSString *err))err {
    __weak typeof (self)weakSelf = self;
    //__block   typeof(NetRequestClss *)wearkNetR = [NetRequestClss sharedNetRequestClss];
    NSString *metStr = (met > 0) ? @"POST" : @"GET";
   
    // 可变 URL
    NSMutableURLRequest *requestUrl = [weakSelf ANetRequestWithURLString:url WithHeader:nil WithParameter:par WithHTTPMethod:metStr];
    // AF 管理者
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",requestUrl]]];
    
    if (![NetRequestClss beforeExecute:manager]) {
        return;
    }
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 3.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
     NSComparisonResult getComparison = [metStr caseInsensitiveCompare:@"GET"];
    if (getComparison == NSOrderedSame) {
        [manager GET:[NSString stringWithFormat:@"%@",requestUrl.URL.absoluteString] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
            NSLog(@"----->%@",responseObject);
            [weakSelf requestWithRetrunSuccess:responseObject successBlock:success];
            
        } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
              [weakSelf requestWithReturnError:error errorBlock:err];
        }];
    }
    
    
    NSComparisonResult postComparison = [metStr caseInsensitiveCompare:@"POST"];
    if (postComparison == NSOrderedSame) {
        NSLog(@"url ==================  %@",requestUrl.URL.absoluteString);
        
        [manager POST:[NSString stringWithFormat:@"%@",requestUrl.URL.absoluteString] parameters:par success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
            [weakSelf requestWithRetrunSuccess:responseObject successBlock:success];
        } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
            [weakSelf requestWithReturnError:error errorBlock:err];
        }];
    
    }

}
/**
 *  请求成功
 */
+ (void)requestWithRetrunSuccess:(id _Nonnull)responseObject successBlock:(void(^)(id,int,NSString *))block{
    // 请求返回的信息
    __block NSString *requestStr = @"";
    NSError *jsonError = nil;
    // 转成Json字符串
    NSString  *jsonStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&jsonError];
    NSLog(@"获取json字符串：原型---->%@  Dic---%@",jsonStr,jsonDic);
    
    if (jsonDic == nil) {
        requestStr = @"返回到了登陆页面";
        block(nil,NetObtainDataStatusFail,requestStr);  // -1
    }else {
        requestStr = jsonStr;
        if([requestStr rangeOfString:@"oilklenze后台登录界面"].location != NSNotFound || [requestStr rangeOfString:@"oilklenze测试服务器后台登录界面"].location != NSNotFound ) {
            requestStr = @"返回到了登陆页面";
            block(nil,NetObtainDataStatusFail,requestStr);  // -1
        }else if([requestStr rangeOfString:@"您的账号已在异地登录，是否重新登录"].location != NSNotFound) {
            requestStr = @"账号异地登陆";
            block(nil,NetObtainDataStatusUnknownAlsoFail,requestStr); // -2
            
        }else {
            //  NSLog(@"----》不包含");
            
            if (jsonDic != nil) {
                requestStr = @"请求成功!";
                block(jsonDic,NetObtainDataStatusSuccess,requestStr); // 1
            }else {
                requestStr = @"获取不到任何数据";
                block(nil,NetObtainDataStatusSuccessAlsoNotData,requestStr); //2
            }
        }
    }
}
/**
 *  请求失败
 */
+ (void)requestWithReturnError:(NSError *_Nonnull)error errorBlock:(void(^)(NSString *err))block {
    NSString *requestStr = @"";
    [UIApplication sharedApplication].networkActivityIndicatorVisible  = NO;
     NSLog(@"\n获取到错误，API错误，或服务器连接失败-->error is %@",error);
    //block(nil,NetObtainDataStatusError,requestStr); // 0
    requestStr = [NetRequestClss afterExecute:error addViewC:nil]; //@"api异常！请检查您的服务器！";
    block(requestStr);
}



#pragma mark - Url拼接 - 添加请求头部内容 -单件
+ (NSMutableURLRequest *)ANetRequestWithURLString:(NSString *)requestUrlString WithHeader:(NSDictionary *)header WithParameter:(NSDictionary *)parameter WithHTTPMethod:(NSString *)httpMethod {
    
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",IPHEAD,requestUrlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strUrl] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:25]; //请求时间10s
    
    //添加请求头
    if (header != nil) {
        for (NSString *key in header.allKeys) {
            [request addValue:header[key] forHTTPHeaderField:key];
            // [request setValue:header[key] forHTTPHeaderField:key];
        }
    }
    
    // get 请求
    NSComparisonResult GETComparison = [httpMethod caseInsensitiveCompare:@"GET"];
    if (GETComparison == NSOrderedSame) {
        [request setHTTPMethod:@"GET"];
        
        if (parameter != nil) {
            //添加参数，将参数拼接在url后面
            NSMutableString *paramsStr = [NSMutableString string];
            NSArray *allKeyAry = [parameter allKeys];
            for(NSString *key in allKeyAry) {
                NSString *vale = [parameter objectForKey:key];
                [paramsStr appendFormat:@"&%@=%@",key,vale];
            }
            
            
            if (paramsStr.length > 0) {
                [paramsStr replaceCharactersInRange:NSMakeRange(0, 1) withString:@"?"];
                //重新设置url
                NSLog(@"\n请求路径:GET-->requestUrl is %@",request);
                NSString *newStrUrl = [strUrl stringByAppendingString:paramsStr];
                [request setURL:[NSURL URLWithString:newStrUrl]];
                
                NSLog(@"\n请求路径:GET-->requestUrl is %@",request);
                
            }
        }
    }
    
    // post 请求
    NSComparisonResult POSTComparison = [httpMethod caseInsensitiveCompare:@"POST"];
    if (POSTComparison == NSOrderedSame) {
        [request setHTTPMethod:@"POST"];
        
        // 暂时不处理
//        for (NSString *key in parameter) {
//            [request setHTTPBody:parameter[key]];
//        }
        NSLog(@"\n请求路径:POST-->requestUrl is %@",request);
        
    }
    return request;
}



@end
