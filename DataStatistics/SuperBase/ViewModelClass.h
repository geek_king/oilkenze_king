//
//  ViewModelClass.h
//  DataStatistics
//
//  Created by Kang on 15/12/9.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface ViewModelClass : NSObject


@property (strong, nonatomic) ReturnValueBlock returnblock;
@property (strong, nonatomic) ErrorCodeBlock errorblock;
@property (strong, nonatomic) FailureBlock failureblock;
//新加
@property (strong, nonatomic) ReceiveVlueBlock receivevlueblock;
@property (strong, nonatomic) ApiBlock apiblock;

@property (copy, nonatomic) NSString  *returnVlueStr;
@property (copy, nonatomic) NSString  *errorVlueStr;
@property (copy, nonatomic) NSString  *failureVlueStr;

/// 请求时返回的 的结果 ，yes - 有值 ，no - 没有
@property (assign, nonatomic ,getter=isReturnBlockVlue) __block BOOL returnBlockVlue;


+ (ViewModelClass *)sharedViewModelClss;

/** 获取网络链接状态 */
- (void)netWorkStateWithNetConnectBlock:(NetWorkBlock)netConnectBlock WithURlStr:(NSString *)strURL;


/** 传入交互的Block */
- (void)setBlockWithReturnBlock:(ReturnValueBlock)returnBlock
                 WithErrorBlock:(ErrorCodeBlock)errorBlock
               WithFailureBlock:(FailureBlock)failureBlock;







/*** 父类VMC-成功获取到数据 子类需要重写*/
- (void)obtainDataWithValueSuccess:(NSDictionary *)returnValue withViewController:(UIViewController *)vc judgeApiStrOrApiId:(id)api ;


/** 父 － 请求*/
- (BOOL)requestDataWithReturnUrl:(NSString *)url withViewController:(UIViewController *)vc
                  withParameters:(NSDictionary *)par  requestWithHeader:(NSDictionary *)header  requestWithApiID:(id)api isMethod:(BOOL)met isListLoaded:(BOOL)loaded;

+ (void)requestDataWithReturnUrl:(NSString *)url withViewController:(id)vc
                  withParameters:(NSDictionary *)par isMethod:(BOOL)met isListLoaded:(BOOL)loaded;

/** 父 － 获取*/
- (void)requestDataWithValueSuccess:(NSDictionary *)returnValue withViewController:(UIViewController *)vc judgeApiStrOrApiId:(id)api returnBlockVlue:(int)returnStatus;

/** 父 － 回调*/
- (void)setBlockWithApiBlock:(ApiBlock)apiBlock WithReceiveVlueBlock:(ReceiveVlueBlock)receive;

// 提示框
- (void)addViewController:(UIViewController *)cont  withTitleStr:(NSString *)str withMessage:(NSString *)mess;

//强制跳转登录
- (void)forceJumpViewController:(UIViewController *)vcont;
// 登陆强制退出 二
+ (NSString *)forceJumpViewStatus:(int)status forController:(UIViewController *)vcont;
@end
