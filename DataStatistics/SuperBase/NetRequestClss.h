//
//  NetRequestClss.h
//  DataStatistics
//
//  Created by Kang on 15/12/9.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>


/** 获取到数据的情况 */
typedef NS_ENUM(NSInteger, NetObtainDataStatus) {
    NetObtainDataStatusUnknownAlsoFail      = -2,  // 返回了登陆 -失败
    NetObtainDataStatusFail                 = -1,  // 失败
    NetObtainDataStatusError                = 0,   // error
    NetObtainDataStatusSuccess              = 1,   // 成功
    NetObtainDataStatusSuccessAlsoNotData   = 2,   // 成功 -无数据
};

typedef NS_ENUM(NSInteger, NetMethod){
      NetMethodGET  =  -1,
      NetMethodPOST =  1,
};


/** 请求类 - 负责 Get请求\ POST 请求\ 监测网络连接情况  */
@interface NetRequestClss : NSObject

@property  (assign, nonatomic) NetObtainDataStatus obtainStatus;
@property  (assign, nonatomic) NetMethod obtainMethod;

// 自构建共享对象
+ (NetRequestClss *)sharedNetRequestClss;

#pragma mark - 监测网络连接情况
/**
 * @brief  监控网络状态
 * @param  block - 返回 String 网络状态
 *
 * @return
 *    none
 */
+ (void)netWorkReachabilityWithRequestBlock: (ReturnValueBlock)block;
+ (void)showNetworkActivityIndicatior;



/**
 * @brief  监控接口
 * @param strUrl -  需要监控的url
 *
 * @return
 *    BOOL - YES = 1 , NO = 0
 */
+ (BOOL)netWorkReachabilityWithURLString:(NSString *)strUrl;


///<(￣︶￣)↗[GO!] 强大的网络监控方法
+ (void)reach;

#pragma mark - <(￣︶￣)↗[GO!] 强大的请求方法

/**
 * @brief 请求方法 默认为Get方法  （未完善）
 * @param url - 请求的URL
 * @param header - 请求头 - NSDictionary
 * @param parameter - 请求参数 - NSDictionary
 * @param vc - 请求对象 - UIViewController
 * @param api - 请求标记 （区分url）- id
 * @param ismet -  yes ->get, no -> post
 * @param islist - yes ->列表, no -> view
 * @param  result  ^()(id objs,   -> 返回的数据
                       int status,  -> 返回标示码： -1 - 登陆界面  0 - error； 1 - 成功 ； 2 - 无数据 失败
                       NSString *msg -> 提示 )
 *
 * @return
 *    none
 */
- (void)requestWithUrl:(NSString *)url requestWithHeader: (NSDictionary *)header requestWithParameters:(NSDictionary *)parameter requestWithViewController:(UIViewController *)vc requestWithApiID:(id)api isMethod:(BOOL)ismet isListLoaded:(BOOL)islist result:(void (^)(id objs, int status, NSString *msg))result;

/**
 *  请求数据
 *
 *  @param url       请求URL
 *  @param parameter 参数
 *  @param result    返回数据
 */
+ (void)requestWithUrl:(NSString *)url requestWithParameters:(NSDictionary *)parameter resultSuccess:(void(^)(id objs, int status, NSString * mag))success resultError:(void(^)(NSString *err))error ;


+ (void)requestWithUrl:(NSString *)url requestWithParameters:(NSDictionary *)par method:(NetMethod)met returnSuccess:(void(^)(id objs, int status, NSString *mag))success returnError:(void(^)(NSString *err))err;



@end
