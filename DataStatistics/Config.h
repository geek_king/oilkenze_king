//
//  Config.h
//  DataStatistics
//
//  Created by Kang on 15/12/9.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#ifndef Config_h
#define Config_h

/// 定义返回请求成功数据的block类型
typedef void(^ReturnValueBlock)(id returnValue);
/// 定义返回错误数据的block类型
typedef void(^ErrorCodeBlock)(id errorCode);
/// 定义 失败数据的block类型
typedef void(^FailureBlock)();
/// 定义 需要返回的失败数据的block类型
typedef void(^FailureCodeBlock)(id failureCode);
/// 定义 返回状态成功数据的block类型
typedef void(^NetWorkBlock)(BOOL netConnetState);

/// 定义 一个可以相互传值block 类型
typedef id (^ReceiveVlueBlock)(id receiveValue);
/// 定义 返回多个参数 api block类型
typedef void(^ApiBlock)(id returnValue ,UIViewController *vc,NSString *msg,id api);

/// 宏定义 匿名 block
#define  REQUEST_CALLBACK void (^)(NetResponse * response)

#import "BPush.h" // 百度
#import "BPush+Convenience.h"
#import <ALBBSDK/ALBBSDK.h> //阿里百川推送
#import <ALBBPush/CloudPushSDK.h>
#import "AFNetworking.h"  // AFN
#import "NetRequestClss.h" 
#import "ViewModelClass.h"
#import "MBProgressHUD.h"
#import "MoreViewController.h"


#import "UIImage+Extension.h"
#import "UIView+Extension.h"
#import "UIBarButtonItem+Extension.h"
#import "NSString+Extension.h"
#import "MJExtension.h"
#import "MJRefresh.h"
#import <pop/Pop.h>

#define PicOriginalDidMoreNotication @"StatusOriginalDidMoreNotication"

// 富文本链接属性标志
#define PicLinkAttr @"PicLinkAttr"
// 富文本链接通知
#define PicLinkDidSelectedNotification @"PicLinkDidSelectedNotification"
//// 转发微博的字体颜色
//#define PicStatusRetweededTextColor DSColor(111, 111, 111)
// 普通文本通知
#define PicStatusNormalTextDidSelectedNotification @"PicStatusNormalTextDidSelectedNotification"




#endif /* Config_h */
