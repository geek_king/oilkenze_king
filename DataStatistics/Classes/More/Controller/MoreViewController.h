//
//  MoreViewController.h
//  DataStatistics
//
//  Created by Kang on 15/8/20.
//  Copyright (c) 2015年 YTYangK. All rights reserved.


#import <UIKit/UIKit.h>

@interface MoreViewController : UIViewController

/**
 *  退出登陆接口
 */
+ (void)logoutOnline;
+ (void)logout:(UIViewController *)VC;


@end
