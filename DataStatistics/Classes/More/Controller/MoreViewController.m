//
//  MoreViewController.m
//  DataStatistics
//
//  Created by Kang on 15/8/20.
//  Copyright (c) 2015年 YTYangK. All rights reserved.
//

#import "MoreViewController.h"

#import "Webview.h"
#import "Masonry.h"
#import "isPhoneNumber.h"
#import "LoginVC.h"
#import "MBProgressHUD.h"
#import "SearchVC.h"
#import "UserModel.h"
#import "UserViewModel.h"

@interface MoreViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    float W_and_H;

}
//页面布局全局变量
@property (strong, nonatomic) IBOutlet UIButton *logoutBtm;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *privacyH;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *logoutBottom;
@property (strong, nonatomic) IBOutlet UILabel *userName;
@property (strong, nonatomic) IBOutlet UIButton *iconBtm;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *privacyAndPasswordBtm;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *iconWH;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *iconBottom;
@property (strong,nonatomic) NSMutableDictionary *parameter;
@property (strong, nonatomic) IBOutlet UIImageView *trafficImage;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *iconTopLC;
@property (strong, nonatomic) IBOutlet UILabel *totalFlow;
@property (strong, nonatomic) IBOutlet UILabel *surplusFlow;
@property (strong, nonatomic) IBOutlet UILabel *deveiceAmount;

@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    W_and_H=self.iconBtm.size.height;
    self.title = NSLocalizedString(@"User", nil);
    self.logoutBtm.layer.cornerRadius=3;
    self.logoutBtm.layer.masksToBounds=YES;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (HEAD_IMAGE) {
        
        [self.iconBtm  setBackgroundImage:HEAD_IMAGE forState:UIControlStateNormal];
     }
    [self.navigationController setNavigationBarHidden:YES];
    
    if (USERNAME) {
        
        NSLog(@"USERNAME ==========  %@",USERNAME);
        
        self.userName.text=USERNAME;
    }else{
        self.userName.text =@"Administartor";
        
        NSLog(@"USERNAME ==========  %@",USERNAME);

    }
  
    float scale = 190.0/667.0; float icon_H = 70; float useName_H = 25; float useName_Top = 8;
    float iconTopLc = (SCREEN_HEIGHT * scale-icon_H - useName_H - useName_Top) / 2;
    
    self.iconTopLC.constant =iconTopLc;
    [self dataRequest];

}
-(void)updateViewConstraints {
    
    [super updateViewConstraints];
    if (SCREEN_HEIGHT==480) {
    }
    self.iconBtm.layer.cornerRadius  = W_and_H/2;
    self.iconBtm.layer.borderColor   = RGB(72, 87, 97, 1).CGColor;
    self.iconBtm.layer.borderWidth   = 2;
    self.iconBtm.layer.masksToBounds = YES;
}

- (void)dataRequest {
    [UserViewModel requestWithUrlForUser:self andParam:@{@"user_id":USERCODE} success:^(UserModel *model) {
        self.totalFlow.text = [NSString stringWithFormat:@"%.2fL", model.total_flow];
        self.surplusFlow.text =[NSString stringWithFormat:@"%.2fL", model.remain_flow];
        self.deveiceAmount.text = model.d_count;
    } failure:^(NSString *err) {
        [MBProgressHUD yty_showErrorWithTitle:nil detailsText:err toView:nil];
    }];

}

/* 退出登录 */
- (IBAction)Logout:(UIButton *)sender {
    
        NSString *message = [NSString stringWithFormat:@"%@",NSLocalizedString(@"Logout will not delete any data. You can still log in with this account.", nil)];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                 message:message
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        NSString *cancelButtonTitle = [NSString stringWithFormat:@"%@", NSLocalizedString(@"Cancel", nil)];
        NSString *otherButtonTitle = [NSString stringWithFormat:@"%@",NSLocalizedString(@"Log out", nil)];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelButtonTitle
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction *action) {  }];
        UIAlertAction *otherAction = [UIAlertAction actionWithTitle:otherButtonTitle
                                                              style:UIAlertActionStyleDestructive
                                                            handler:^(UIAlertAction *action) {
            [[SearchVC sharedSearchVC] setSearchModelAry:nil];
            [[SearchVC sharedSearchVC].searchTableView reloadData];
            [MoreViewController logout:self];
        }];
        [alertController addAction:cancelAction];
        [alertController addAction:otherAction];
    [self.navigationController presentViewController:alertController animated:YES completion:nil];

}
+ (void)logout:(UIViewController *)VC {
   //  [BPush YTYangK_DelAllListTags];
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"accessToken"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"notFirstOpen"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"headImage"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USER_INFO_Defaults];
   // [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"userCode"];
  //  [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userCode"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"passwordText"];
    
    [[NSNotificationCenter defaultCenter] removeObserver:BADGE_Home];
    [[NSNotificationCenter defaultCenter] removeObserver:BADGE_Num];
    [[NSNotificationCenter defaultCenter] removeObserver:UIApplicationDidBecomeActiveNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:@"setBadge"];
    [[NSNotificationCenter defaultCenter] removeObserver:@"SearchJump"];
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications]; // 清除全部通知。
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0]; //角标清0
   
    [MoreViewController logoutOnline];

   
}
+ (void)logoutOnline {
    [UserViewModel requestWithUrlForlogoutOnline:self andParam:nil success:^(id model) {
        if (isLogin != 1) {
            /// 删除绑定的全部标签
            NSString *bindStr;
            if (![IP isEqualToString:@"http://112.74.100.200"]) {
                bindStr = [NSString stringWithFormat:@"test_%@",USERCODE];
            }else {
                bindStr = [NSString stringWithFormat:@"formal_%@",USERCODE];
            }
            
            [CloudPushSDK unbindAccount:bindStr withCallback:^(BOOL success) {
                if (success) {
                    //成功
                    NSLog(@"删除标签成功=> %@",bindStr);
                }else {
                    NSLog(@"删除标签失败=> ");
                }
            }];
            
            UIStoryboard * storyboard =[UIStoryboard storyboardWithName:@"Login" bundle:NULL];
            LoginVC *log              = [storyboard instantiateInitialViewController];
            UIWindow *window          = [UIApplication sharedApplication].keyWindow;
            window.rootViewController = log;
            
        }
    } failure:^(NSString *err) {
        [MBProgressHUD yty_showErrorWithTitle:nil detailsText:err toView:nil];
    }];
}


@end
