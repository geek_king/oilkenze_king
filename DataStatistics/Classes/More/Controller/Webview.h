//
//  Webview.h
//  DataStatistics
//
//  Created by Kang on 15/8/15.
//  Copyright (c) 2015年 YTYangK. All rights reserved.
//



@interface Webview : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *Web;
@property (strong, nonatomic) NSString  *identificationStr;
@end
