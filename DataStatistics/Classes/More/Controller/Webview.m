//
//  Webview.m
//  DataStatistics
//
//  Created by Kang on 15/8/15.
//  Copyright (c) 2015年 YTYangK. All rights reserved.
//

#import "Webview.h"

@interface Webview ()<UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;

@end

@implementation Webview

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBarController.tabBar.hidden =YES;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
}
- (void)viewDidAppear:(BOOL)animate {
    [super viewDidAppear:animate];
    [self loadString];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ....
- (void)loadString {
    /*http://www.oilklenze.com.cn/privacy_policy.html
     
      http://www.oilklenze.com.cn/privacy_policy_cn.html
    */
    NSString *nsLang = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    NSURLRequest *request;
    //NSString * nslang2 = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if ([nsLang isEqualToString:@"cn"]) {
        if ([_identificationStr isEqualToString:@"Privacy Policy"]) {
            request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.oilklenze.com.cn/privacy_policy_cn.html"]];
        }else {
            request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.oilklenze.com.cn/terms_of_use_cn.html"]];
        }
    }else {
        if ([_identificationStr isEqualToString:@"Privacy Policy"]) {
            request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.oilklenze.com.cn/privacy_policy.html"]];
        }else {
            request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.oilklenze.com.cn/terms_of_use.html"]];
        }
    }
    _Web.dataDetectorTypes = UIDataDetectorTypeAll;
    [self.activity startAnimating];
    dispatch_async(dispatch_queue_create("dd", NULL), ^{
        [self.Web loadRequest:request];
    });
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.activity stopAnimating];
    });
}

@end
