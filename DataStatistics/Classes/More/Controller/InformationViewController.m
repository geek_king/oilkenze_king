//
//  InformationViewController.m
//  DataStatistics
//
//  Created by oilklenze on 16/1/12.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "InformationViewController.h"
#import "MoreViewController.h"
#import "ChangeUserNameViewModel.h"

@interface InformationViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView        * imageicon;
@property (strong, nonatomic) IBOutlet UILabel            * userName;
@property (strong, nonatomic) MoreViewController          * more;
@property (weak, nonatomic  ) IBOutlet UILabel            * account;

@end

@implementation InformationViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.imageicon.layer.cornerRadius = 42 / 2;
    self.imageicon.layer.borderColor = RGB(72, 87, 97, 1).CGColor;
    self.imageicon.layer.borderWidth = 2;
    self.imageicon.layer.masksToBounds = YES;
    if (HEAD_IMAGE) {
        self.imageicon.image = HEAD_IMAGE;
    }
    if (USERNAME) {
        self.userName.text = USERNAME;
    }
    else {
        self.userName.text = @"Administartor";
    }
    NSNotificationCenter* center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(defaultsChanged:)
                   name:NSUserDefaultsDidChangeNotification
                 object:nil];
    self.account.text = USERCODE;
}

- (void)defaultsChanged:(NSNotification*)notification
{
    if (HEAD_IMAGE) {
        self.imageicon.image = HEAD_IMAGE;
    }
    if (USERNAME) {
        self.userName.text = USERNAME;
    }
    else {
        self.userName.text = @"Administartor";
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (IBAction)changeIcon:(UIButton*)sender {
    
    UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing = YES;

    UIAlertController* sheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Choose photos", nil)
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    
    UIAlertAction* camera = [UIAlertAction actionWithTitle:NSLocalizedString(@"Take Photo", nil)
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction* _Nonnull action) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];
    
    UIAlertAction* PhotoLibrary = [UIAlertAction actionWithTitle:NSLocalizedString(@"Choose from Photos", nil)
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction* _Nonnull action) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }];
    sheet.title = nil;

    [sheet addAction:cancel];
    [sheet addAction:camera];
    [sheet addAction:PhotoLibrary];
    [self presentViewController:sheet animated:YES completion:nil];
}
//修改头像
- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString*, id>*)info {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage* image = [info valueForKey:@"UIImagePickerControllerEditedImage"];
    NSData* imageData = UIImageJPEGRepresentation(image, 0.5);
    NSString* stringbes64 = [imageData base64EncodedStringWithOptions:0];
    stringbes64 = [@"data:image/png;base64," stringByAppendingString:stringbes64];
    NSDictionary* par = @{ @"headImage" : stringbes64 };
    self.imageicon.image = image;
    
    [ChangeUserNameViewModel requestWithUrlForChangeUserProfilePhoto:self andParam:par success:^(id model) {
    NSLog(@"*----->%@",model);
        
        [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"headImage"];
        NSData* data         = [[NSUserDefaults standardUserDefaults] objectForKey:@"headImage"];
        UIImage* image1      = [[UIImage alloc] initWithData:data];
        self.imageicon.image = image1;
        
    } failure:^(NSString *error) {
        [MBProgressHUD yty_showErrorWithTitle:nil detailsText:error toView:nil];
    }];
    
    

}

- (MoreViewController*)more {
    if (!_more) {
        _more = [[MoreViewController alloc] init];
    }
    return _more;
}

@end
