//
//  ChangeUserNameViewModel.m
//  DataStatistics
//
//  Created by 123456 on 16/6/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ChangeUserNameViewModel.h"
#import "ChangeUserNameModel.h"

@implementation ChangeUserNameViewModel



#pragma mark - 修改名字
+ (void)requestWithUrlForChangeUserName:(id)obj  andParam:(NSDictionary *)param success:(void(^)(ChangeUserNameModel * model))success failure:(void(^)(NSString * error))failure {
    
    
    [[UtilToolsClss getUtilTools] addDoLoading];
    [NetRequestClss requestWithUrl:@"account/userInfo.asp" requestWithParameters:param method:NetMethodPOST returnSuccess:^(id objs, int status, NSString *mag) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
       
      //  NSNumber *body = objs[@"body"][@"result"];
        if (objs) {
            ChangeUserNameModel *list = [ChangeUserNameModel mj_objectWithKeyValues:objs[@"body"]];
            success(list);
            return;
        }else {
            failure(@"---");
        }
        
        
    } returnError:^(NSString *err) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        failure(err);
        
    }];

    
}
#pragma mark - 修改头像
+ (void)requestWithUrlForChangeUserProfilePhoto:(id)obj  andParam:(NSDictionary *)param success:(void(^)(id model))success failure:(void(^)(NSString * error))failure {
    [[UtilToolsClss getUtilTools] addDoLoading];
    [NetRequestClss requestWithUrl:@"account/userInfo.asp" requestWithParameters:param method:NetMethodPOST returnSuccess:^(id objs, int status, NSString *mag) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
    
        NSString *resultStr = [objs valueForKeyPath:@"body.result"];
        NSString *resultErr = [objs valueForKeyPath:@"body.error"];
        if (!resultErr) {
            if ([resultStr intValue] == 1) {
                success(resultStr);
                return;
            }else {
                failure(NSLocalizedString(@"Failure", nil));
            }
        }
        failure(resultErr);
    } returnError:^(NSString *err) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        failure(err);
    }];

    
}


@end
