//
//  UserModel.h
//  DataStatistics
//
//  Created by 123456 on 16/6/15.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "JSONModel.h"

@interface UserModel : JSONModel

singleton_for_interface(UserModel)

//总流量
@property (assign, nonatomic) float total_flow;
//剩余流量
@property (assign, nonatomic) float remain_flow;
//设备数量
@property (copy, nonatomic) NSString* d_count;


@end
