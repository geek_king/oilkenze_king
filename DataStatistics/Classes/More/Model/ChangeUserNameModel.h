//
//  ChangeUserNameModel.h
//  DataStatistics
//
//  Created by 123456 on 16/6/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "JSONModel.h"

@interface ChangeUserNameModel : JSONModel
singleton_for_interface(ChangeUserNameModel)


@property(nonatomic,copy)NSString *result;


@end
