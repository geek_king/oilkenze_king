//
//  ChangePasswordViewModel.m
//  DataStatistics
//
//  Created by 123456 on 16/6/20.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ChangePasswordViewModel.h"
#import "ChangePasswordModel.h"

@implementation ChangePasswordViewModel

#pragma mark - 修改密码
+ (void)requestWithUrlForChangePassword:(id)obj  andParam:(NSDictionary *)param success:(void(^)(ChangePasswordModel * model))success failure:(void(^)(NSString * error))failure {
    
    
    [[UtilToolsClss getUtilTools] addDoLoading];
    [NetRequestClss requestWithUrl:@"account/updatePwd.asp" requestWithParameters:param method:NetMethodPOST returnSuccess:^(id objs, int status, NSString *mag) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        
        if (objs) {
            ChangePasswordModel *list = [ChangePasswordModel mj_objectWithKeyValues:objs[@"body"]];
            success(list);
            return;
        }else {
            failure(@"---");
        }
        
        
    } returnError:^(NSString *err) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        failure(err);
        
    }];
    
    
}


@end
