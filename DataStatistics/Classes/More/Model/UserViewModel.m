//
//  UserViewModel.m
//  DataStatistics
//
//  Created by 123456 on 16/6/15.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "UserViewModel.h"
#import "UserModel.h"
@implementation UserViewModel

    

#pragma mark - 个人中心
+ (void)requestWithUrlForUser:(id)user andParam:(NSDictionary *)param success:(void (^)(UserModel *model))success failure:(void (^)(NSString *err))failure {
     [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [NetRequestClss requestWithUrl:@"account/account.asp" requestWithParameters:param method:NetMethodPOST returnSuccess:^(id objs, int status, NSString *mag) {
          [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

        if (status != 1) {
            NSString *tilasStr = [ViewModelClass forceJumpViewStatus:status forController:user];
            failure(tilasStr); return;
        }
        
        if (objs != nil) {
            UserModel *list  = [UserModel mj_objectWithKeyValues:objs[@"body"]];
            success(list); return;
        }
        
    } returnError:^(NSString *err) {
          [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        failure(err);
    }];
}


#pragma mark - 退出登录
+ (void)requestWithUrlForlogoutOnline:(id)obj andParam:(NSDictionary *)param success:(void (^)(id model))success failure:(void (^)(NSString *err))failure {
    [[UtilToolsClss getUtilTools] addDoLoading];
    [NetRequestClss requestWithUrl:@"login/logout.asp" requestWithParameters:param method:NetMethodPOST returnSuccess:^(id objs, int status, NSString *mag) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        NSLog(@"----->%@",objs);
        NSString *  resultStr = [[objs objectForKey:@"body"] objectForKey:@"result"];
        if ([resultStr intValue]  == 1) {
            success(resultStr);
        }else {
            failure(@"退出失败了");
        }
    } returnError:^(NSString *err) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        failure(err);
    }];
}

@end
