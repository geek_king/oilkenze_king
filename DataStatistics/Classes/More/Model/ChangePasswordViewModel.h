//
//  ChangePasswordViewModel.h
//  DataStatistics
//
//  Created by 123456 on 16/6/20.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "ChangePasswordModel.h"

@interface ChangePasswordViewModel : ViewModelClass


/**
 *  修改密码API
 */
+ (void)requestWithUrlForChangePassword:(id)obj  andParam:(NSDictionary *)param success:(void(^)(ChangePasswordModel * model))success failure:(void(^)(NSString * error))failure;


@end
