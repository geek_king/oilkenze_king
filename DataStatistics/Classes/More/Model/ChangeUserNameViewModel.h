//
//  ChangeUserNameViewModel.h
//  DataStatistics
//
//  Created by 123456 on 16/6/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "ChangeUserNameModel.h"

@interface ChangeUserNameViewModel : ViewModelClass

/**
 *  修改名字 API
 */
+ (void)requestWithUrlForChangeUserName:(id)obj  andParam:(NSDictionary *)param success:(void(^)(ChangeUserNameModel * model))success failure:(void(^)(NSString * error))failure;

/**
 *  修改头像API
 */
+ (void)requestWithUrlForChangeUserProfilePhoto:(id)obj  andParam:(NSDictionary *)param success:(void(^)(id model))success failure:(void(^)(NSString * error))failure;

@end
