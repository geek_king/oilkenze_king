//
//  UserViewModel.h
//  DataStatistics
//
//  Created by 123456 on 16/6/15.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "UserModel.h"
@interface UserViewModel : ViewModelClass

/**
 *  个人中心API
 */
+ (void)requestWithUrlForUser:(id)user andParam:(NSDictionary *)param success:(void (^)(UserModel *model))success failure:(void (^)(NSString *err))failure;

/**
 *  退出登录API
 */
+ (void)requestWithUrlForlogoutOnline:(id)obj andParam:(NSDictionary *)param success:(void (^)(id model))success failure:(void (^)(NSString *err))failure;

@end
