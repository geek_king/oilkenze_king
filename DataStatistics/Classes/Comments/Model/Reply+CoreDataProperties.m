//
//  Reply+CoreDataProperties.m
//  DataStatistics
//
//  Created by oilklenze on 16/3/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Reply+CoreDataProperties.h"

@implementation Reply (CoreDataProperties)

@dynamic commentPK;
@dynamic commentContent;
@dynamic user_name;
@dynamic user_image;
@dynamic commentTime;
@dynamic message;

@end
