//
//  Images+CoreDataProperties.m
//  DataStatistics
//
//  Created by oilklenze on 16/3/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Images+CoreDataProperties.h"

@implementation Images (CoreDataProperties)

@dynamic imageURL;
@dynamic message;

@end
