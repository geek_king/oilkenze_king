//
//  Reply+CoreDataProperties.h
//  DataStatistics
//
//  Created by oilklenze on 16/3/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Reply.h"

NS_ASSUME_NONNULL_BEGIN

@interface Reply (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *commentPK;
@property (nullable, nonatomic, retain) NSString *commentContent;
@property (nullable, nonatomic, retain) NSString *user_name;
@property (nullable, nonatomic, retain) NSString *user_image;
@property (nullable, nonatomic, retain) NSDate *commentTime;
@property (nullable, nonatomic, retain) MainTalkAbout *message;

@end

NS_ASSUME_NONNULL_END
