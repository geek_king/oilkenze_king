//
//  MainTalkAbout+CoreDataProperties.h
//  DataStatistics
//
//  Created by oilklenze on 16/3/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MainTalkAbout.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainTalkAbout (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *pk;
@property (nullable, nonatomic, retain) NSString *content;
@property (nullable, nonatomic, retain) NSString *user_name;
@property (nullable, nonatomic, retain) NSString *user_image;
@property (nullable, nonatomic, retain) NSString *deviceNo;
@property (nullable, nonatomic, retain) NSDate *publishTime;
@property (nullable, nonatomic, retain) NSNumber *page;
@property (nullable, nonatomic, retain) NSSet<Reply *> *reply;
@property (nullable, nonatomic, retain) NSSet<Images *> *images;

@end

@interface MainTalkAbout (CoreDataGeneratedAccessors)

- (void)addReplyObject:(Reply *)value;
- (void)removeReplyObject:(Reply *)value;
- (void)addReply:(NSSet<Reply *> *)values;
- (void)removeReply:(NSSet<Reply *> *)values;

- (void)addImagesObject:(Images *)value;
- (void)removeImagesObject:(Images *)value;
- (void)addImages:(NSSet<Images *> *)values;
- (void)removeImages:(NSSet<Images *> *)values;

@end

NS_ASSUME_NONNULL_END
