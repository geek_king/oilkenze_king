//
//  Images.h
//  DataStatistics
//
//  Created by oilklenze on 16/3/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Messages.h"

@class MainTalkAbout;

NS_ASSUME_NONNULL_BEGIN

@interface Images : NSManagedObject

+ (Images *)imagesWithImageURL:(NSString *)imageURL inManagedObjectContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Images+CoreDataProperties.h"
