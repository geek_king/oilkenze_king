//
//  Images.m
//  DataStatistics
//
//  Created by oilklenze on 16/3/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "Images.h"
#import "MainTalkAbout.h"

@implementation Images

+ (Images *)imagesWithImageURL:(NSString *)imageURL inManagedObjectContext:(NSManagedObjectContext *)context;
{
    Images *image = nil;
    if (imageURL) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Images"];
        NSError *error;
        NSArray *matches = [context executeFetchRequest:request error:&error];
        if (!matches || matches.count > 1) {
            //出错；
        }else if (!matches.count){
            image = [NSEntityDescription insertNewObjectForEntityForName:@"Images" inManagedObjectContext:context];
            NSString *imagesURL = [GET_COMMENTS_IMAGE_IP stringByAppendingString:imageURL];
            image.imageURL = imagesURL;
        } else {
            image = [matches lastObject];
        }
    }
    return image;
}
@end
