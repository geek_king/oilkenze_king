//
//  MainTalkAboutViewModel.h
//  DataStatistics
//
//  Created by Kang on 16/7/4.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "Messages.h"
#import "MainTalkAbout.h"


@interface MainTalkAboutViewModel : ViewModelClass
+(void)requestWithUrlForMainTalk:(id)obj andParam:(NSDictionary *)param success:(void(^)(NSDictionary *model))success failure:(void(^)(NSString *error))failure;
@end
