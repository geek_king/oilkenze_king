//
//  Messages.h
//  DataStatistics
//
//  Created by oilklenze on 16/2/17.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "JSONModel.h"

@protocol Messages
@end
@protocol Comments
@end

@interface Comments : JSONModel
@property (nonatomic)int commentPK; //评论主键
@property (nonatomic,strong)NSString *commentContent; //评论内容
@property (nonatomic,strong)NSString *user_name; //评论用户的用户名
@property (nonatomic,strong)NSString *commentTime; //评论时间
@property (nonatomic,strong)NSString *user_nameAndComemtenContet; //合并评论着和内容

@end

@interface Messages : JSONModel
@property (nonatomic)int pk; //信息主键
@property (nonatomic,strong)NSString *content; //content
@property (nonatomic,strong)NSArray *images; //信息图片列表
@property (nonatomic,strong)NSString *user_name; //发布人的用户
@property (nonatomic,strong)NSString *user_image; //发布人的图像
@property (nonatomic,strong)NSString *deviceNo; //设备编号 可选
@property (nonatomic,strong)NSString *publishTime; //发表时间

@property (nonatomic,strong)NSMutableArray <Comments,ConvertOnDemand> * comments; //评论列表
@end

@interface Body : JSONModel
@property (nonatomic,strong) NSArray <Messages,ConvertOnDemand> * messages;
@end