//
//  Reply.h
//  DataStatistics
//
//  Created by oilklenze on 16/3/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Messages.h"

@class MainTalkAbout;

NS_ASSUME_NONNULL_BEGIN

@interface Reply : NSManagedObject

+ (Reply *)replyWithcomment:(Comments *)comment inManageObjectContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "Reply+CoreDataProperties.h"
