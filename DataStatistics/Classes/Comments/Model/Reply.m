//
//  Reply.m
//  DataStatistics
//
//  Created by oilklenze on 16/3/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "Reply.h"
#import "MainTalkAbout.h"


@implementation Reply


+ (Reply *)replyWithcomment:(Comments *)comment inManageObjectContext:(NSManagedObjectContext *)context
{
    Reply *reply = nil;
    NSNumber *commetPK = [[NSNumber alloc]initWithInt:comment.commentPK];

    if (commetPK) {
        NSFetchRequest *requset = [NSFetchRequest fetchRequestWithEntityName:@"Reply"];
        requset.predicate = [NSPredicate predicateWithFormat:@"commentPK = %@",commetPK];
        NSError *error;
        NSArray *matches = [context executeFetchRequest:requset error:&error];
        if (!matches || ([matches count] > 1)) {
            //出错
        }else if (![matches count]){
            reply = [NSEntityDescription insertNewObjectForEntityForName:@"Reply" inManagedObjectContext:context];
            reply.commentPK = commetPK;
            reply.commentContent = comment.commentContent;
            reply.user_name = comment.user_name;
            NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *date = [formatter  dateFromString:comment.commentTime];
            reply.commentTime =date;
        }else {
            reply = [matches lastObject];
        }
    }
    NSLog(@"------>>%@",reply);
    return reply;
}
@end
