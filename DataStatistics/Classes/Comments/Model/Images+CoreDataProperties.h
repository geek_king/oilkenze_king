//
//  Images+CoreDataProperties.h
//  DataStatistics
//
//  Created by oilklenze on 16/3/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Images.h"

NS_ASSUME_NONNULL_BEGIN

@interface Images (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *imageURL;
@property (nullable, nonatomic, retain) MainTalkAbout *message;

@end

NS_ASSUME_NONNULL_END
