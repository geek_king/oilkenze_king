//
//  MainTalkAbout+CoreDataProperties.m
//  DataStatistics
//
//  Created by oilklenze on 16/3/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MainTalkAbout+CoreDataProperties.h"

@implementation MainTalkAbout (CoreDataProperties)

@dynamic pk;
@dynamic content;
@dynamic user_name;
@dynamic user_image;
@dynamic deviceNo;
@dynamic publishTime;
@dynamic page;
@dynamic reply;
@dynamic images;

@end
