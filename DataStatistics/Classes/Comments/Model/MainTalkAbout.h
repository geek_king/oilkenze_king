//
//  MainTalkAbout.h
//  DataStatistics
//
//  Created by oilklenze on 16/3/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Messages.h"

@class Images, Reply;

NS_ASSUME_NONNULL_BEGIN

@interface MainTalkAbout : NSManagedObject

+ (MainTalkAbout *)mainTalkAboutWithMessages:(Messages *)messages
                      inManagedObjectContext:(NSManagedObjectContext *)context;

@end

NS_ASSUME_NONNULL_END

#import "MainTalkAbout+CoreDataProperties.h"
