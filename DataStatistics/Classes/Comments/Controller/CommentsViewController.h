//
//  CommentsViewController.h
//  DataStatistics
//
//  Created by oilklenze on 16/2/16.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDRichTextViewController.h"
#import "NetRequestClss.h" // 在交流 圈没有修改前暂时使用的请求工具
#import "MainTalkAboutViewModel.h"

@interface CommentsViewController : DDRichTextViewController<NSFetchedResultsControllerDelegate>

@property (strong, nonatomic)NSString *commentsDeviceCode;

@property (strong,nonatomic) NSMutableArray             *ymDataArray;//数据模型
@property (nonatomic,strong) NSManagedObjectContext     *manageobjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic       ) BOOL                       debug;
@end
