//
//  CoverView.m
//  DataStatistics
//
//  Created by 123456 on 16/7/1.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "CoverView.h"


@interface CoverView ()

@end


@implementation CoverView






-(void)setCoverObjectForKey:(NSString *)key;
{
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:key]) {
        
        [self.DevicesView removeFromSuperview];
        
    }else{
        
        self.DevicesView = [[NSBundle mainBundle]loadNibNamed:@"DevicesWelcome" owner:self options:nil][0];
        self.DevicesView.frame = [UIScreen mainScreen].bounds;
        
        UIWindow *keyv=[[UIApplication sharedApplication] keyWindow];
        [keyv addSubview:self.DevicesView];

    }
}





@end
