//
//  CustomLineView.h
//  DataStatistics
//
//  Created by Kang on 16/4/21.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomLineView : UIView

@property (assign, nonatomic) float lineWidth;

@property (assign, nonatomic) CGPoint start_linePoint;
@property (assign, nonatomic) CGPoint end_linePoint;
+ (instancetype)initLineViewWithRGBStrokeColor:(float)r greenVlue:(float)g blueVlue:(float)b moveToPoint:(CGPoint)startPoint addLineToPoint:(CGPoint)endPoint;

@end
