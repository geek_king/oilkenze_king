//
//  TabBar.m
//  DataStatistics
//
//  Created by Kang on 15/12/28.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "TabBar.h"

@interface TabBar()

@property (weak, nonatomic) UIButton *plusButton;
@end

@implementation TabBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        if (!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            self.backgroundImage = [UIImage imageWithName:@"tabbar_background"];
        }
        self.backgroundColor = [UIColor redColor];
        self.selectionIndicatorImage = [UIImage imageWithName:@"statusdetail_toolbar_button_background"];
        
        [self setupPlusButton];
    }
    return self;
}


- (void)setupPlusButton {
    UIButton *plusButton = [[UIButton alloc] init];
    //设置背景
    [plusButton setBackgroundImage:[UIImage imageWithName:@"tabbar_compose_button"] forState:UIControlStateNormal];
    [plusButton setBackgroundImage:[UIImage imageWithName:@"tabbar_compose_button_highlighted"] forState:UIControlStateHighlighted];
    // 设置图标
    [plusButton setImage:[UIImage imageWithName:@"tabbar_compose_icon_add"] forState:UIControlStateNormal];
    [plusButton setImage:[UIImage imageWithName:@"tabbar_compose_icon_add_highlighted"] forState:UIControlStateHighlighted];
    [plusButton addTarget:self action:@selector(plusClick:) forControlEvents:UIControlEventTouchUpInside];
    // 添加
    [self addSubview:plusButton];
     self.plusButton = plusButton;
}

- (void)plusClick:(UIButton *)btn {
   // if ([self.delegate respondsToSelector:@selector(tabBarDidClickedPlusButton:)]) {
     //   [self.delegate tabBarDidClickedPlusButton:self];
 //   }
}

//布局子控件
- (void)layoutSubviews {
    [super layoutSubviews];
    [self setupPlusButtonFrame];
    [self setupAllTabBarButtonsFrame];
    
}

- (void)setupPlusButtonFrame {
    self.plusButton.size = self.plusButton.currentBackgroundImage.size;
    self.plusButton.center = CGPointMake(self.width, self.height); //CGPointMake(self.width * 0.5, self.height *0.5);
}

- (void)setupAllTabBarButtonsFrame {
    int index = 0;
    for (UIView *tabBtn in self.subviews) {
        NSLog(@"----->%@",tabBtn);
       /* if (index >=5) {
            [tabBtn removeFromSuperview];
        }else {
        if (![tabBtn isKindOfClass:NSClassFromString(@"UITabBarButton")])continue; //不等于系统的UITabBarButton
        [self setupTabBarButtonFrom:tabBtn atIndex:index];
         index++;
       }*/
        if (![tabBtn isKindOfClass:NSClassFromString(@"UITabBarButton")])continue; //不等于系统的UITabBarButton
        [self setupTabBarButtonFrom:tabBtn atIndex:index];
        index++;

    }
}

- (void)setupTabBarButtonFrom:(UIView *)tabBarButton atIndex:(int)index {

    //计算Btn 尺寸
    CGFloat buttonW = self.width / (self.items.count);
    CGFloat buttonH = self.height;

    tabBarButton.width  = buttonW;
    tabBarButton.height = buttonH;
   // if (index >= 4) {
    //   tabBarButton.x  =  buttonW * (index + 1);
        
  //  }else {
   //     tabBarButton.x =  buttonW * (index);
  //  }
    tabBarButton.y = 0;

}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
