//
//  TabBarC.m
//  DataStatistics
//
//  Created by Kang on 15/12/25.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "TabBarC.h"
#import "TabBar.h"


//#import "HomeVC.h"
//#import "EquipmenVC.h"
//#import "MoreViewController.h"
//#import "UserVC.h"

#import "SearchVC.h"


@interface TabBarC ()
@property (copy, nonatomic) NSString *NavigationItemTitle;
//@property (weak, nonatomic) HomeVC *homevc;
//@property (weak, nonatomic) EquipmenVC *equipmenvc;
//@property (weak, nonatomic) UserVC  * uservc;
@property (weak, nonatomic) MoreViewController * morevc;

@property (weak, nonatomic) UIButton *selectedBtn;
@end

@implementation TabBarC


+ (void)initialize {
    /*UITabBarItem *Item  = [UITabBarItem appearance];
    [Item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:RGB(42, 192, 228, 1),NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];*/
    
    NSMutableDictionary *dir1 = [NSMutableDictionary dictionary];
    dir1[NSForegroundColorAttributeName] = RGB(42, 192, 228, 1);
    
    [[UITabBar appearance] setSelectedImageTintColor:RGB(42, 192, 228, 1)];
    [[UITabBarItem appearance].image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [[UITabBarItem appearance].selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [[UITabBar appearance] setBarTintColor:RGB(25, 25, 25, 1)];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSMutableDictionary dictionaryWithObject:RGB(171, 171, 171, 1) forKey:NSForegroundColorAttributeName] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:dir1 forState:UIControlStateSelected];
}
- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *bindStr= @"";
    if (![IP isEqualToString:@"http://112.74.100.200"]) {
        bindStr = [NSString stringWithFormat:@"test_%@",USERCODE];
    }else {
        bindStr = [NSString stringWithFormat:@"formal_%@",USERCODE];
    }


    [CloudPushSDK unbindAccount:bindStr withCallback:^(BOOL success) {
        if (success) {
            //成功
            NSLog(@"删除标签成功=> %@",bindStr);
        }else {
             NSLog(@"删除标签失败=> ");
        }
    }];

    //if (![[NSUserDefaults standardUserDefaults] boolForKey:@"notFirstOpen"])
//   [TabBarC baiduBindChannelWithTonke];
    [TabBarC alBBBindChannelWithTonke];
   // NSLog(@"%s",__func__);
   // NSLog(@"%@",self.view.subviews);//能打印出所有子视图,和其frame

  //  [self addAllChildVCs];
  //  [self addCustomTabBar];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpSearch:) name:BADGE_Home object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homeBadgeNumber:) name:BADGE_Num object:nil]; // 前台运行时
//  /  [[NSNotificationCenter  defaultCenter] addObserver:self selector:@selector(homeBadgeNumber:) name:UIApplicationDidBecomeActiveNotification object:nil]; //激活时候
}


// 创建自定义的tabbar
- (void)addCustomTabBar {
    
    TabBar *customTabBar = [[TabBar alloc] init];
   // customTabBar.delegate = self;
    //更换系统自带的tabbar
   [self setValue:customTabBar forKey:@"tabBar"];
}

- (void)homeBadgeNumber:(NSNotification *)text {
    [[NSNotificationCenter defaultCenter] removeObserver:BADGE_Num];
    [[NSNotificationCenter defaultCenter] removeObserver:UIApplicationDidBecomeActiveNotification];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setBadge" object:self userInfo:[text userInfo]];
}

//- (void)jumpSearch:(NSNotification *)text {
//    [[NSNotificationCenter defaultCenter] removeObserver:BADGE_Home];
//    NSDictionary *dic = [text userInfo];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"SearchJump" object:self userInfo:dic];
//}


+ (void)baiduBindChannelWithTonke {
    /** 百度代码 */
      NSLog(@"百度推送绑定");
    
    NSString *bindStr= @"";
    if (![IP isEqualToString:@"http://112.74.100.200"]) {
        bindStr = [NSString stringWithFormat:@"test_%@",USERCODE];
    }else {
        bindStr = [NSString stringWithFormat:@"formal_%@",USERCODE];
    }
    
     [BPush bindChannelWithCompleteHandler:^(id result, NSError *error) {
       NSLog(@"进行标签绑定");
         if (error) {  // 网络错误
             return;
         }
         
         if (result) {
             if ([result[@"error_code"]intValue] != 0) {
                 return;
             }
            // NSString *channeID = [BPush getChannelId];
           //  NSLog(@"channel_id --->%@",[BPush getChannelId]);
                       // 需要在绑定成功后进行 settag listtag deletetag unbind 操作否则会失败
    

    NSLog(@"-*---->%@",[BPush getUserId]);
                 if (USERCODE != nil && ![USERCODE isEqualToString:@" "]) {
                    [BPush setTag:bindStr withCompleteHandler:^(id result, NSError *error) {
                         NSLog(@"%@,",[NSString stringWithFormat:@"剩余绑定次数:%ld",(long)result]);

                         if (result) {
                             NSLog(@"管理员标签绑定成功,添加成功！");
                         }else {
                             NSLog(@"管理员标签绑定成功,添加失败！");
                         }
                         
                     }];
                 }else {
                     NSLog(@"账号：%@",USERCODE);
                 }
             

         }
    }];

   // [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"notFirstOpen"]; //不是第一次打开标记
}

+ (void)alBBBindChannelWithTonke {
    /** 阿里代码 */
    NSString *bindStr= @"";
    if (![IP isEqualToString:@"http://112.74.100.200"]) {
        bindStr = [NSString stringWithFormat:@"test_%@",USERCODE];
    }else {
        bindStr = [NSString stringWithFormat:@"formal_%@",USERCODE];
    }
    
     if (USERCODE != nil && ![USERCODE isEqualToString:@" "]) {
         [CloudPushSDK bindAccount:bindStr withCallback:^(BOOL success) {
              if (success) {
                  //成功
                 NSLog(@"绑定标签成功=> %@",bindStr);
              }else {
                   NSLog(@"绑定标签失败=> ");
              }
         }];
     }else {
         NSLog(@"账号：%@",USERCODE);
     }
    
    
    
}

- (void)addAllChildVCs {
    
   /* HomeVC *home = [[HomeVC alloc] init];
    [self addOneChildVC:home title:@"Home" imageName:@"tabHome_off" selectedImageName:@"tabHome"];
    _homevc = home;
    EquipmenVC *devices = [[EquipmenVC alloc] init];
    [self addOneChildVC:devices title:@"Devices" imageName:@"tabDevices_off" selectedImageName:@"tabDevices"];
    _equipmenvc = devices;
    UserVC *user = [[UserVC alloc] init];
    [self addOneChildVC:user title:@"Center" imageName:@"tabCenter_off" selectedImageName:@"tabCenter"];
    _uservc  = user;
    MoreViewController *more = [[MoreViewController alloc] init];
    [self addOneChildVC:more title:@"User" imageName:@"tabUser_off" selectedImageName:@"tabUser"];
    _morevc = more; */
    
}
/*- (void)addOneChildVC:(UIViewController *)childVc title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageNmae {
    //设置标题
    childVc.title = title;
    if ([childVc class] == [HomeVC class]) {
        childVc.navigationItem.title = @"拜3"; //self.NavigationItemTitle;
    }
    
    
    //设置图标
    childVc.tabBarItem.image = [UIImage imageNamed:imageName];
    //设置选中图标
    UIImage *selectedImage = [UIImage imageNamed:selectedImageNmae];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        selectedImageNmae = [NSString stringWithFormat:@"%@",[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    }
    childVc.tabBarItem.selectedImage = selectedImage;
    
    //添加导航控制器
    NavigationC *nav = [[NavigationC alloc] initWithRootViewController:childVc];
    [self addChildViewController:nav];
} */


@end
