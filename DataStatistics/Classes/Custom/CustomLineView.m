//
//  CustomLineView.m
//  DataStatistics
//
//  Created by Kang on 16/4/21.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "CustomLineView.h"

@interface CustomLineView()
@property (nonatomic) float redFloat;
@property (nonatomic) float greenFloat;
@property (nonatomic) float blueFloat;
@property (nonatomic) float alphaFloat;

//启点
@property (nonatomic) float line_X;
@property (nonatomic) float line_Y;
//终点
@property (nonatomic) float line_x;
@property (nonatomic) float line_y;

@end

@implementation CustomLineView
@synthesize start_linePoint = _start_linePoint;
@synthesize end_linePoint  = _end_linePoint;

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, self.lineWidth);
    CGContextSetAllowsAntialiasing(context, true);
    CGContextSetRGBStrokeColor(context, self.redFloat, self.greenFloat, self.blueFloat, self.alphaFloat);
    CGContextBeginPath(context);
    
    CGContextMoveToPoint(context, self.line_X, self.line_Y);
    CGContextAddLineToPoint(context, self.line_x, self.line_y);
    CGContextStrokePath(context);

}
+ (instancetype)initLineViewWithRGBStrokeColor:(float)r greenVlue:(float)g blueVlue:(float)b moveToPoint:(CGPoint)startPoint addLineToPoint:(CGPoint)endPoint {
    CustomLineView *line = [[CustomLineView alloc] init];
    
    CGPoint sp = startPoint;
    CGPoint ep = endPoint;
    line.frame = CGRectMake(sp.x, sp.y, ep.x, ep.y);
    line.backgroundColor = [UIColor whiteColor];
    line.lineWidth = 1.5f;
    line.start_linePoint = startPoint;
    line.end_linePoint = endPoint;
    [line lineRGBStrokeColorWihtRed:r greenVlue:g blueVlue:b alphaVlue:1.0];
    return line;
}



- (void)lineRGBStrokeColorWihtRed:(float)red greenVlue:(float)green blueVlue:(float)blue alphaVlue:(float)alpha {
    
    self.redFloat =  red/255.0;
    self.greenFloat = green / 255.0;
    self.blueFloat = blue /255.0;
    self.alphaFloat = alpha;
    
}

- (float)lineWidth {
    if (!_lineWidth) {
        _lineWidth = 3;
    }
    return _lineWidth;
}

- (void)setStart_linePoint:(CGPoint)start_linePoint {
    if (_start_linePoint.x != start_linePoint.x) {
        self.line_X  = start_linePoint.x;
    }
    
    if (_start_linePoint.y != start_linePoint.y) {
        self.line_Y = start_linePoint.y;
    }
    
}

- (void)setEnd_linePoint:(CGPoint)end_linePoint {
    if (_end_linePoint.x !=  end_linePoint.x) {
        self.line_x = end_linePoint.x;
    }
    
    if (_end_linePoint.y != end_linePoint.y) {
        self.line_y = end_linePoint.y;
    }
}



@end
