//
//  WelcomeViewController.m
//  DataStatistics
//
//  Created by 123456 on 16/6/30.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "WelcomeViewController.h"
#import "SDCycleScrollView.h"
#import "ControllerTool.h"
#import "CoverView.h"


@interface WelcomeViewController ()<SDCycleScrollViewDelegate>

@property (weak, nonatomic) IBOutlet SDCycleScrollView *WelcomeScroll;

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self welcome];

    
}



-(void)welcome
{

    NSArray *arr = @[@"IMG_6496.PNG",@"IMG_6491.PNG",@"IMG_6511.PNG",@"IMG_6512.PNG",@"IMG_6514.PNG"];
    
    self.WelcomeScroll.imageURLStringsGroup =arr;
    self.WelcomeScroll.delegate = self;
    self.WelcomeScroll.autoScroll = NO;//自动循环滚动
    self.WelcomeScroll.showPageControl =YES;//page点
    self.WelcomeScroll.infiniteLoop = NO;//实现循环轮播;

    
    
    
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index{

    NSLog(@"%ld",index);
    
    if (index == 4) {
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        
        //在获取storyboard中的视图控制器
        UITabBarController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"LoginVC"];
        
        vc.modalPresentationStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:vc animated:YES completion:nil];
        
        //设置已经访问的标志位
        [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"isHaveVisit"];
    }
    
}


@end
