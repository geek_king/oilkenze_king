//
//  CoustomerDetailsController.m
//  DataStatistics
//
//  Created by Kang on 16/4/21.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "CoustomerDetailsController.h"
#import "CoustomerDetailsViewModel.h"
#import "CustomLineView.h"


@interface CoustomerDetailsController ()
{

    IBOutlet NSLayoutConstraint *lineView_H;
    IBOutlet NSLayoutConstraint *lineConteact_H;
    IBOutlet NSLayoutConstraint *lineTex_H;
    IBOutlet NSLayoutConstraint *lineFax_H;
    IBOutlet NSLayoutConstraint *lineEail_H;
    IBOutlet NSLayoutConstraint *lineview_H;
    
    
     IBOutlet NSLayoutConstraint *address_to;
    IBOutlet NSLayoutConstraint *email_bottom;
    __weak IBOutlet NSLayoutConstraint *companyName_H;
    IBOutlet NSLayoutConstraint *companyName_bottom;
    IBOutlet NSLayoutConstraint *companyName_to;

   
    IBOutlet NSLayoutConstraint *contact_W;
}


@property (strong, nonatomic) IBOutlet UILabel *companyName;

@property (strong, nonatomic) IBOutlet UILabel *contactTitle;
@property (strong, nonatomic) IBOutlet UILabel *contactVlue;
@property (strong, nonatomic) IBOutlet UILabel *telTitle;
@property (strong, nonatomic) IBOutlet UILabel *telVlue;
@property (strong, nonatomic) IBOutlet UILabel *fexTitle;
@property (strong, nonatomic) IBOutlet UILabel *fexVlue;
@property (strong, nonatomic) IBOutlet UILabel *emailTitle;
@property (strong, nonatomic) IBOutlet UILabel *emailVlue;
@property (strong, nonatomic) IBOutlet UILabel *addressTitle;
@property (strong, nonatomic) IBOutlet UILabel *addressVlue;


@property (strong, nonatomic) CoustomerListModel *coustomerDataList;
@property (strong, nonatomic) NSArray  *coustomerTitleAry;



@property (strong, nonatomic) IBOutlet NSLayoutConstraint *addressVlue_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *eMailVlue_H;


@end

@implementation CoustomerDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateUI];
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    lineview_H.constant = lineConteact_H.constant = lineTex_H.constant = 0.5;
    lineView_H.constant = lineEail_H.constant        =  lineFax_H.constant = 0.5;
    
    email_bottom.constant = 20;
    if (SCREEN_HEIGHT == 568) {
        contact_W.constant = (278 * 0.72);
        address_to.constant = 15;
        companyName_bottom.constant = 30;
    }else if(SCREEN_HEIGHT == 480) {
        contact_W.constant = (278 * 0.72);
        companyName_bottom.constant = 15;
       // address_to.constant = 15;
    }else if(SCREEN_HEIGHT == 667) {
       // companyName_to.constant = 25;
//        address_to.constant = 20;
//        tableView_H.constant = 138;
//        email_top.constant = 5;
        //email_bottom.constant = 13;
    }
    
    if (SYSTEM_VERSION_GREATER_THAN_W_orH(SCREEN_HEIGHT,@"667.0")) {
//        tableView_H.constant = 240;
//        email_top.constant = 20;
       // companyName_to.constant = 25;
    }
    
   
}

#pragma mark - Other
- (void)updateUI {
 
    self.title = NSLocalizedString(@"CDVCtitle", nil);
    
    self.contactTitle.text        = self.coustomerTitleAry[0];
    self.telTitle.text               = self.coustomerTitleAry[1];
    self.fexTitle.text               = self.coustomerTitleAry[2];
    self.emailTitle.text           = self.coustomerTitleAry[3];
    self.addressTitle.text       = self.coustomerTitleAry[4];

    NSDictionary *dic = @{@"deviceCode":self.coustomerDeviceCode};
    [CoustomerDetailsViewModel requestWithUrlForCoustomerDetails:self andParam:dic success:^(CoustomerDetailsModel *model) {
        
        if (model.CoustomerListModel.count > 0) {
            self.coustomerDataList = model.CoustomerListModel[0];
            self.companyName.text = [self.coustomerDataList c_name];
            
            self.contactVlue.attributedText = [self filterLabelWithString:[self.coustomerDataList c_contact]];
            self.telVlue.attributedText         = [self filterLabelWithString:[self.coustomerDataList c_tel]];
            self.fexVlue.attributedText         = [self filterLabelWithString:[self.coustomerDataList c_fax]];
            
            [self calculateLableHightWithString:[self.coustomerDataList c_email]  filterLabel:self.emailVlue layoutConstraint:self.eMailVlue_H];
           
            // 语言代码  1 -中文   2 -英文
            if ( [UtilToolsClss judgeLocalLanguage] == 1)  {
                [self calculateLableHightWithString:[self.coustomerDataList c_addr] filterLabel:self.addressVlue layoutConstraint:self.addressVlue_H];
            }else {
                [self calculateLableHightWithString:[self.coustomerDataList c_addrEn] filterLabel:self.addressVlue layoutConstraint:self.addressVlue_H];
            }
            
            
        }

    } failure:^(NSString *error) {
    
    }];
}




#pragma  mark - 私有方法
// 过滤
- (NSMutableAttributedString *)filterLabelWithString:(NSString  *)stringVlue {
    NSString * str;
    NSMutableAttributedString *mutStr;
    if (stringVlue == nil || [stringVlue isEqualToString:@""] || [stringVlue isEqualToString:@"(null)"]) {
        str = @"---";
    }else {
        str = stringVlue;
    }
    mutStr = [[NSMutableAttributedString alloc] initWithString:str];
    return mutStr;
}
// 设置高度
- (void)calculateLableHightWithString:(NSString *)vlue  filterLabel:(UILabel *)label  layoutConstraint:(NSLayoutConstraint *)layout {
    CGRect frame;
    float  label_H = 0;
    float  default_H = (SCREEN_HEIGHT - 64) *0.03;
    
    label.attributedText  = [self filterLabelWithString:vlue];
    label.textAlignment = NSTextAlignmentLeft;
    if (!label.text || [label.text isEqualToString:@""] || [label.text isEqualToString:@"(null)"]) {
        layout.constant =  default_H; // 当值没有时，显示默认高度
        return;
    }
      frame = [label.text boundingRectWithSize:CGSizeMake(label.width ,label.height *4) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading  attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14.f]} context:nil];
      label_H = frame.size.height; // 获取根据字体大小与最大值高度、宽度 得出的合理大小。
    
     if (label_H < default_H) {
         label_H = default_H;
     }else {
         layout.constant = label_H;
     }
}

#pragma mark - get \ set
- (NSArray *)coustomerTitleAry {
    if (!_coustomerTitleAry) {
        _coustomerTitleAry = [[NSArray alloc] initWithObjects:
                              NSLocalizedString(@"CDVCcont", nil),
                              NSLocalizedString(@"CDVCtel", nil),
                              NSLocalizedString(@"CDVCtax", nil),
                              NSLocalizedString(@"CDVCmail", nil),
                              NSLocalizedString(@"CDVCaddr", @"地址"),
                              nil];
    }
    return _coustomerTitleAry;
}
- (void)setCompanyName:(UILabel *)companyName {
    if (_companyName != companyName) {
        _companyName = companyName;
    }
    
    CGRect frame = [companyName.text boundingRectWithSize:CGSizeMake(SCREEN_WIDTH - 40, 62) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:25.f]} context:nil];
    float company_h = frame.size.height;
    companyName_H.constant = company_h;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
