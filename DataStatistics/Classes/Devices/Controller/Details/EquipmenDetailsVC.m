//
//  EquipmenDetailsVC.m
//  DataStatistics
//
//  Created by Kang on 15/12/30.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "EquipmenDetailsVC.h"
#import "DeviceDetailsModel.h"
#import "EquipmenDetailsViewModel.h"
//#import "CoustomerDetailsVC.h"

@interface EquipmenDetailsVC ()
{
    IBOutlet NSLayoutConstraint *V_greenBG_H;
    IBOutlet NSLayoutConstraint *V_redBG_H;
    IBOutlet NSLayoutConstraint *H_redBG_W;
    IBOutlet NSLayoutConstraint *H_greenBG_W;
    
    
    
    IBOutlet UILabel *EDVC_priceTitle;
    IBOutlet UILabel *EDVC_depositTitle;
    IBOutlet UILabel *EDVC_prepaymentTitle;
    IBOutlet UILabel *EDVC_minimumUsageTitle;
    IBOutlet UILabel *EDVC_contractTermTitle;
    IBOutlet UILabel *EDVC_validPeriodTitle;
    IBOutlet UILabel *EDVC_simCardNoTitle;
    IBOutlet UILabel *EDVC_carrierTitle;
    IBOutlet UILabel *EDVC_roamingTitle;
    IBOutlet UILabel *EDVC_activatedDateTitle;
}
/** 存放获取的数据源 */
@property (strong, nonatomic) __block NSArray *publicModelAry;
@property (weak, nonatomic) IBOutlet UILabel *totalFlow;
@property (weak, nonatomic) IBOutlet UILabel *surplusFlow;
@property (weak, nonatomic) IBOutlet UILabel *subtotalFlow;
@property (weak, nonatomic) IBOutlet UIButton *cilentInfo;

@property (weak, nonatomic) IBOutlet UILabel  *surplusFlowVlue;
@property (weak, nonatomic) IBOutlet UILabel  *totalFlowVlue;
@property (weak, nonatomic) IBOutlet UILabel  *subtotlFlowVlue;
@property (weak, nonatomic) IBOutlet UILabel  *priceVlue;
@property (weak, nonatomic) IBOutlet UILabel  *depositVlue;
@property (weak, nonatomic) IBOutlet UILabel  *prepayFlowVlue;
@property (weak, nonatomic) IBOutlet UILabel  *minUsedFlowVlue;
@property (weak, nonatomic) IBOutlet UILabel  *contractTermVlue;
@property (weak, nonatomic) IBOutlet UILabel  *signedAtVlue;
@property (weak, nonatomic) IBOutlet UILabel  *cardIDVlue;
@property (weak, nonatomic) IBOutlet UILabel  *cardBrandVlue;
@property (weak, nonatomic) IBOutlet UILabel  *roamingTypeVlue;
@property (weak, nonatomic) IBOutlet UILabel  *activationDateVlue;

@end

@implementation EquipmenDetailsVC


- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateUI];
    if (SYSTEM_VERSION_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"480.0")) {
        V_greenBG_H.constant   += 14;
        H_greenBG_W.constant  -= 8 ;
       // V_redBG_H.constant = V_redBG_H.constant *0.70;
        H_redBG_W.constant  -=8;
        V_redBG_H.constant   +=14;
    }
    __block EquipmenDetailsVC * blockSelf = self;
[EquipmenDetailsViewModel requestWithUrlForDeviceDetails:self andParam:@{@"deviceCode":self.devicecode} success:^(DeviceDetailsModel *model) {
    if (model != nil) {
        _publicModelAry = [NSMutableArray arrayWithObjects:model, nil];
         if (_publicModelAry.count > 0) {
            float totflo = model.total_flow;
            float addflo = model.addup_flow;
            float remflo = [_publicModelAry[0] remain_flow];
            blockSelf.totalFlowVlue.text     = [NSString stringWithFormat:@" %.2fL",totflo];
            blockSelf.subtotlFlowVlue.text   = [NSString stringWithFormat:@" %.2fL",addflo];
            blockSelf.surplusFlowVlue.text   = [NSString stringWithFormat:@" %.2fL",remflo];
            
            if ([ROLEDODE isEqualToString:@"001"] || [ROLEDODE isEqualToString:@"005"]) {
                blockSelf.priceVlue.text         = [NSString stringWithFormat:@" %.2f",model.price];
                blockSelf.depositVlue.text       = [NSString stringWithFormat:@" %.2f",model.deposit];
                blockSelf.prepayFlowVlue.text    = [NSString stringWithFormat:@" %.2f",model.payment_flow];
                blockSelf.minUsedFlowVlue.text   = [NSString stringWithFormat:@" %.2f", model.use_flow];
            }else {
                blockSelf.priceVlue.text         = [NSString stringWithFormat:@" ***"];
                blockSelf.depositVlue.text       = [NSString stringWithFormat:@" ***"];
                blockSelf.prepayFlowVlue.text    = [NSString stringWithFormat:@" ***"];
                blockSelf.minUsedFlowVlue.text   = [NSString stringWithFormat:@" ***"];
            }
            if (![[NSString stringWithFormat:@"%d", model.contract] isEqualToString:@"(null)"]) {
                blockSelf.contractTermVlue.text      = [NSString stringWithFormat:NSLocalizedString(@" %d months", nil) ,model.contract];
            }
            
            if (![[NSString stringWithFormat:@"%@",model.begin] isEqualToString:@"(null)"]) {
               // NSDate *abegin; NSDate *aend; NSString *abeginTime; NSString *aendTime;
                blockSelf.signedAtVlue.text          = [NSString stringWithFormat:@" %@-%@",model.begin,model.end];
               // abegin = [UtilToolsClss dateFromString:model.begin withDateFormat:@"yyyy-MM-dd"];
                //aend = [UtilToolsClss dateFromString:model.end withDateFormat:@"yyyy-MM-dd"];
                //abeginTime = [UtilToolsClss stringFromDate:abegin withDateFormat:@"MM/dd/yyyy"];
              //  aendTime = [UtilToolsClss stringFromDate:aend withDateFormat:@"MM/dd/yyyy"];
               // blockSelf.signedAtVlue.text          = [NSString stringWithFormat:@" %@-%@",abeginTime,aendTime];
                
            }
            
            if (![[NSString stringWithFormat:@"%@",model.id_num] isEqualToString:@"(null)"]) {
                blockSelf.cardIDVlue.text            = [NSString stringWithFormat:@" %@",model.id_num];
            }
            
            if (![[NSString stringWithFormat:@"%@",model.brand] isEqualToString:@"(null)"]) {
                NSString *  string = [NSString stringWithFormat:@"%@",model.brand];
                blockSelf.cardBrandVlue.text         = NSLocalizedString(string, nil);
            }
            
            if (![[NSString stringWithFormat:@"%@",model.method] isEqualToString:@"(null)"]) {
                NSString *string = [NSString stringWithFormat:@"%@",model.method];
                blockSelf.roamingTypeVlue.text       = NSLocalizedString(string, nil);;
            }
            
            if (![[NSString stringWithFormat:@"%@",model.active_date] isEqualToString:@"(null)"]) {
                blockSelf.activationDateVlue.text    = [NSString stringWithFormat:@" %@",model.active_date];
                NSDate * activeDate = [UtilToolsClss dateFromString:model.active_date withDateFormat:@"yyyy-MM-dd"];
                NSString *active = [UtilToolsClss stringFromDate:activeDate withDateFormat:@"MM/dd/yyyy"];
                blockSelf.activationDateVlue.text    = [NSString stringWithFormat:@" %@",active];
            }
        }
    }
    
    
} failure:^(NSString *error) {
    
}];

}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)updateViewConstraints {
    [super updateViewConstraints];
    
    if (SCREEN_WIDTH == 320.0) {
        
        self.totalFlowVlue.font = self.subtotlFlowVlue.font = self.surplusFlowVlue.font = [UIFont systemFontOfSize:14.0];
        self.signedAtVlue.font = [UIFont systemFontOfSize:15.0];
        self.surplusFlow.font = self.totalFlow.font = self.subtotalFlow.font= [UIFont systemFontOfSize:13];
        
    }
}
- (void)updateUI {
    self.title                      = NSLocalizedString(@"DetailsTitle", nil);
    EDVC_priceTitle.text            = NSLocalizedString(@"EDVC_priceTitle", nil);
    EDVC_depositTitle.text          = NSLocalizedString(@"EDVC_depositTitle", nil);
    EDVC_prepaymentTitle.text       = NSLocalizedString(@"EDVC_prepaymentTitle", nil);
    EDVC_minimumUsageTitle.text     = NSLocalizedString(@"EDVC_minimumUsageTitle", nil);
    EDVC_contractTermTitle.text     = NSLocalizedString(@"EDVC_contractTermTitle", nil);
    EDVC_validPeriodTitle.text      = NSLocalizedString(@"EDVC_validPeriodTitle", nil);
    EDVC_simCardNoTitle.text        = NSLocalizedString(@"EDVC_simCardNoTitle", nil);
    EDVC_carrierTitle.text          = NSLocalizedString(@"EDVC_carrierTitle", nil);
    EDVC_roamingTitle.text          = NSLocalizedString(@"EDVC_roamingTitle", nil);
    EDVC_activatedDateTitle.text    = NSLocalizedString(@"EDVC_activatedDateTitle", nil);
    
    self.totalFlow.text             = NSLocalizedString(@"EDVC_totalflowTitle", nil);
    self.surplusFlow.text           = NSLocalizedString(@"EDVC_surplusFlowTitle", nil);
    self.subtotalFlow.text          = NSLocalizedString(@"EDVC_subtotalFlowTitle", nil);
}



- (IBAction)cilentInfoClick:(id)sender {
//    CoustomerDetailsVC *details = [[CoustomerDetailsVC alloc] init];
//    details.Cdevicecode = self.devicecode;
//    details.hidesBottomBarWhenPushed  = YES;
//    [self.navigationController pushViewController:details animated:YES];
}


@end
