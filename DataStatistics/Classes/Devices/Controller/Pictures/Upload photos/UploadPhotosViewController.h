//
//  UploadPhotosViewController.h
//  DataStatistics
//
//  Created by oilklenze on 16/2/2.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface UploadPhotosViewController : UIViewController
@property(strong, nonatomic)NSString  *uploadPhotosDeviceCode;
@property (nonatomic,strong) void(^showimage)(NSMutableArray *,NSString*);//回调照片显示
@property (nonatomic,strong) void(^updateImageSuccess)();//回调照片已经上传完成


@end
