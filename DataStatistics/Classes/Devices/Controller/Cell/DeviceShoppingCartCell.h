//
//  DeviceShoppingCartCell.h
//  DataStatistics
//
//  Created by Kang on 16/1/11.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviceShoppingCartCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *purchaseBtn;
- (IBAction)purchaseAction:(UIButton *)sender;

@end
