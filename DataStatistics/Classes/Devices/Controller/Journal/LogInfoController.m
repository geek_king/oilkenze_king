//
//  LogInfoController.m
//  DataStatistics
//
//  Created by Kang on 16/4/20.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "LogInfoController.h"
#import "NSString+Extension.h"
#import "JournalDetailsViewModel.h"

@interface LogInfoController ()

@property (strong, nonatomic) NSArray *HeaderArray;
@property (strong, nonatomic) JournalDetailsList *lists;
@property (strong, nonatomic) IBOutlet UILabel *flowUsedTitle;
@property (strong, nonatomic) IBOutlet UILabel *flowUsedVlue;

@property (strong, nonatomic) IBOutlet UILabel *green_avgL_min;
@property (strong, nonatomic) IBOutlet UILabel *green_avgVlue;
@property (strong, nonatomic) IBOutlet UILabel *green_maxVlue;

@property (strong, nonatomic) IBOutlet UILabel *blue_avgBar;
@property (strong, nonatomic) IBOutlet UILabel *blue_avgVlue;
@property (strong, nonatomic) IBOutlet UILabel *blue_maxVlue;

@property (strong, nonatomic) IBOutlet UITableView *vlueTableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *vlueTableViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *flowVlue_greeView_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *greenView_H;
@end

@implementation LogInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateUI];
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (SYSTEM_VERSION_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"480.0") ) {
        self.flowVlue_greeView_H.constant = 6;
        self.greenView_H.constant  = 119 *0.173;
    }else if (SYSTEM_VERSION_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"736.0")) {
        self.vlueTableViewHeight.constant = 361;
    }
}

#pragma mark - TableView Data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.HeaderArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cellHeight = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    float  heightFloat = CGRectGetHeight(cellHeight.frame);
    if (SYSTEM_VERSION_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"480.0")) {
        heightFloat = 35;
    }else if (SYSTEM_VERSION_EQUAL_TO_W_orH(SCREEN_HEIGHT,@"667.0")) {
        heightFloat = 50;
    }else if (SYSTEM_VERSION_EQUAL_TO_W_orH(SCREEN_HEIGHT,@"736.0")) {
        heightFloat = 60;
    }
    
    return heightFloat;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idenfitier = @"logInfoList";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idenfitier];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont systemFontOfSize:13.0f];
    cell.textLabel.text = self.HeaderArray[indexPath.row];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:13.0f];
    cell.detailTextLabel.text = [self aryValue:self.lists indexPath:indexPath];
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    return cell;
}

#pragma mark - Other
// 更新数据，布局UI
- (void)updateUI {
    
    _vlueTableView.userInteractionEnabled = NO;
    _vlueTableView.separatorColor = [UIColor grayColor];
    _vlueTableView.backgroundColor = [UIColor clearColor];
    
    self.title = self.logInfoDeviceCode;
    self.flowUsedTitle.text = NSLocalizedString(@"LVCfil", @"flow used(L)");
    NSNumber * num = [NSNumber numberWithInt:self.logInfoID];
    NSDictionary *parameterDic = @{@"log_id" : num};
    
    // __weak LogInfoController *wearkSelf = self;
    __weak typeof(self)wearkSelf = self;
    [JournalDetailsViewModel requestWithUrlForJournalDetails:self andParam:parameterDic success:^(JournalDetailsModel *model) {
        
        if (model.JournalDetailsList == nil) {
        }else {
            wearkSelf.lists = model.JournalDetailsList[0];
            float usedvlue = [[wearkSelf.lists flow] floatValue];
            wearkSelf.flowUsedVlue.text = [NSString stringWithFormat:@"%@",[UtilToolsClss aryFloatValue:usedvlue]];
            float avgvlue = [wearkSelf.lists avg_flowspeed];
            wearkSelf.green_avgVlue.text = [NSString stringWithFormat:@"%@",[UtilToolsClss aryFloatValue:avgvlue]];
            wearkSelf.blue_avgVlue.text  = [NSString stringWithFormat:@"%@",[UtilToolsClss aryFloatValue:[wearkSelf.lists avg_pressure]]];
            wearkSelf.green_maxVlue.text = [NSString stringWithFormat:@"MAX %@",[UtilToolsClss aryFloatValue:[wearkSelf.lists max_flowspeed]]];
            wearkSelf.blue_maxVlue.text = [NSString stringWithFormat:@"MAX %@",[UtilToolsClss aryFloatValue:[wearkSelf.lists max_pressure]]];
            [wearkSelf.vlueTableView reloadData];
           
        }
    } failure:^(NSString *error) {}];
  
}
- (NSString *)aryValue:(JournalDetailsList *)list indexPath:(NSIndexPath *)inde {
    
    NSString *str = nil;
    NSDate *date;
    NSString *formart = @"MM/dd/yyyy HH:mm:ss";
    if (inde.row == 0) {
        str   = [NSString stringWithFormat:@"%@",[list work_id]];
    }else if (inde.row == 1) {
        str =  [NSString intervalFromLastDate:[list work_begin] toTheDate:[list work_end]];
    }else if (inde.row == 2) {
        str = [NSString stringWithFormat:@"%@L",[UtilToolsClss aryFloatValue:[list remain_flow]]];
    }else if (inde.row == 3) {
        str = [NSString stringWithFormat:@"%@L",[UtilToolsClss aryFloatValue:[list total_flow]]];
    }else if (inde.row == 4) {
      date  = [UtilToolsClss dateFromString:[list work_begin] withDateFormat:formart];
       str =  [UtilToolsClss stringFromDate:date withDateFormat:formart];
    }else if (inde.row  == 5) {
        date  = [UtilToolsClss dateFromString:[list work_end] withDateFormat:formart];
         str =  [UtilToolsClss stringFromDate:date withDateFormat:formart];
    }
    
    if ([str isEqualToString:@"(null)"] || str == nil || str.length <=0) {
        str = @"---";
    }
    return str;
}


#pragma  mark - get / set
- (NSArray *)HeaderArray {
    if (!_HeaderArray) {
        _HeaderArray = [[NSArray alloc] initWithObjects: NSLocalizedString(@"LVCser", nil),
                        NSLocalizedString(@"LVCtime", nil),
                        NSLocalizedString(@"LVCres", nil),
                         NSLocalizedString(@"LVCtot", nil),
                        NSLocalizedString(@"LVCbeg", nil),
                        NSLocalizedString(@"LVCend", nil),nil];
    }
    return _HeaderArray;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
