//
//  JournalListController.h
//  DataStatistics
//
//  Created by Kang on 16/4/14.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JournalListController : UITableViewController
@property (retain, nonatomic) NSString *journalDeviceCode;
@property (retain, nonatomic) NSString *journalWork_id;
@property (strong, nonatomic) IBOutlet UITableView *journalTableView;

@end
