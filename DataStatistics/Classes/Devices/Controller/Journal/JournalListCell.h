//
//  JournalListCell.h
//  DataStatistics
//
//  Created by Kang on 16/4/14.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JournalViewModel.h"

@interface JournalListCell : UITableViewCell

/**
 *   Cell数据更新
 */
- (void)vlueWithList:(JouList *)list;
@end
