//
//  LogInfoController.h
//  DataStatistics
//
//  Created by Kang on 16/4/20.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogInfoController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) NSString *logInfoDeviceCode;
@property (assign, nonatomic) int logInfoID;

@end
