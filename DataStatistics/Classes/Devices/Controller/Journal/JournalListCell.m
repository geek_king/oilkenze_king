//
//  JournalListCell.m
//  DataStatistics
//
//  Created by Kang on 16/4/14.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "JournalListCell.h"


@interface JournalListCell ()
@property (strong, nonatomic) IBOutlet UILabel *JLCell_workId;
@property (strong, nonatomic) IBOutlet UILabel *JLCell_workDate;
@property (strong, nonatomic) IBOutlet UILabel *JLCell_workFlow;

@end

@implementation JournalListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - 设置Cell UI及数据更新
- (void)vlueWithList:(JouList *)list {
    self.JLCell_workId.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"JCellid", @"流水号"),list.work_id];
    NSString *timeString;
    if ([list.recv_ymd isEqualToString:@"(null)"] || [list.recv_hm isEqualToString:@"(null)"]) {
        timeString = @"---";
    }else {
        timeString = [NSString stringWithFormat:@"%@ %@:00",list.recv_ymd,list.recv_hm];
    }
    
    NSDate * timeDate = [UtilToolsClss dateFromString:timeString withDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    timeString = [UtilToolsClss stringFromDate:timeDate withDateFormat:@"MM/dd/yyyy HH:mm"];
    
    self.JLCell_workDate.text   =  [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"JCellrecv", nil),timeString];
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]initWithString:NSLocalizedString(@"Flow used:", nil) attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10],NSForegroundColorAttributeName :RGB(220, 220, 220, 0.8),NSBaselineOffsetAttributeName:@(2)}];
    NSMutableAttributedString  *string1 =[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@L",[UtilToolsClss aryFloatValue:list.flow]]];
     [string insertAttributedString:string1 atIndex:string.length];
    self.JLCell_workFlow.attributedText = string;
}

@end
