//
//  EquipmenCell.h
//  DataStatistics
//
//  Created by Kang on 15/12/10.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EquipmenModel.h"


@protocol EquipmenCellDelegate<NSObject>
//-(void)judgeVlue:(UIButton *)btn BtnVlue:(int)btnVlue isDetermineClick:(BOOL)click;
@end


@interface EquipmenCell : UITableViewCell
@property (weak, nonatomic)id<EquipmenCellDelegate>delegate;
/** 红色小标 */
@property (strong, nonatomic) IBOutlet UIButton *btnVlue;

/** 设置list数据  */
- (void)setvalueWithList:(EListModel *)List;
- (void)judgeBtnVlue:(int)btnVlue isDetermineClick:(BOOL)click;
@end
