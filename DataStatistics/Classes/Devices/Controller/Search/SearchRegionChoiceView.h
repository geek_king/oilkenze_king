//
//  SearchRegionChoiceView.h
//  DataStatistics
//
//  Created by Kang on 16/4/18.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchCityModel.h"

typedef NS_ENUM(NSInteger, RegionStageState) {
    /**
     *   国家
     */
    regionStage_Country,
    /**
     *  省
     */
    regionStage_Province,
    /**
     *  城市
     */
    regionStage_City
};

@class SearchRegionChoiceView;
@protocol SearchRegionChoiceDelegate <NSObject>
- (void)pickViewSetHigh:(float)high;
- (void)pickerViewWillHide:(SearchRegionChoiceView *)view;
- (void)refreshSearchData:(SearchRegionChoiceView *)view;

@end


@interface SearchRegionChoiceView : UIView<UITableViewDelegate,UITableViewDataSource>
@property (assign, nonatomic) id<SearchRegionChoiceDelegate>delegate;

/**
 *  提示语
 */
@property (strong, nonatomic) IBOutlet UILabel   *SRCView_tipsStr;
/**
 *  子-头部容器
 */
@property (strong, nonatomic) IBOutlet UIView    *SRCView_headTipsView;
/**
 *   子-中部容器
 */
@property (strong, nonatomic) IBOutlet UIView    *SRCView_centralSectionView;
/**
 *  子-尾部容器
 */
@property (strong, nonatomic) IBOutlet UIView    *SRCView_tailView;
/**
 *  提交按钮
 */
@property (strong, nonatomic) IBOutlet UIButton *SRCView_doneBtn;
/**
 *   选中的值 -Btn
 */
@property (strong, nonatomic) IBOutlet UIButton *SRCView_pitchOnVlue;
/**
 *  cityTable
 */
@property (strong, nonatomic) IBOutlet UITableView *cityTableView;


// 搜索值
@property (strong, nonatomic) NSString  * SRCView_searchValueStr;
/**
 *  城市层级状态
 */
@property (assign, nonatomic) RegionStageState stageState;
/**
 *  国家数组
 */
@property (strong, nonatomic) NSMutableArray *SRCView_countryAry;
/**
 *  城市数组
 */
@property (strong, nonatomic) NSMutableArray *SRCView_provinceAry;
/**
 *  pickView高度
 */
@property (assign, nonatomic) float SRCView_pickViewHigh;
/**
 *  开启
 */
@property (assign, nonatomic) BOOL isOpenAreaValue;

- (IBAction)submitAndRequest:(UIButton *)sender;
- (IBAction)clearSelectedValue:(UIButton *)sender;



//+ (instancetype)initSearchRegionChoiceViewWithFrame:(CGRect)frame;

@end
