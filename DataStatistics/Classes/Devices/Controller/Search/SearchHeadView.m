//
//  SearchHeadView.m
//  DataStatistics
//
//  Created by Kang on 16/4/18.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "SearchHeadView.h"

@interface SearchHeadView()




@property (assign, nonatomic) int balanceMarkers;
@property (assign, nonatomic) int usedMarkers;
@property (assign, nonatomic) int timeMarkers;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SHView_balance_x;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SHView_used_x;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SHView_recents_x;

@end


@implementation SearchHeadView

@synthesize screeningState = _screeningState;
@synthesize sortMethodStr      = _sortMethodStr;


#pragma mark - event response
+ (void)initialize {
    NSLog(@"XXX");
    
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
    if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO_W_orH(SCREEN_WIDTH, @"320.0")) {
        self.SHView_balance_x.constant = 4;
        self.SHView_used_x.constant  = 4;
        self.SHView_recents_x.constant  = 4;
    }else {}
    self.balanceMarkers = 0;
    self.usedMarkers = 0;
    self.timeMarkers = 0;
    self.SHView_timeLabel.textColor = [UIColor grayColor];
    self.SHView_balanceLabel.textColor = [UIColor grayColor];
    self.SHView_usedLabel.textColor = [UIColor grayColor];
    [self SHView_balanceView];
    [self SHView_usedView];
    [self SHView_timeView];
   
}


#pragma mark - getters and setters
- (UIView *)SHView_balanceView {
    if (!_SHView_balanceView) {
        
    }
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchState:)];
    [_SHView_balanceView addGestureRecognizer:tapGestureRecognizer];
    
    return _SHView_balanceView;
}

- (UIView *)SHView_usedView {
    if (!_SHView_usedView) {
        
    }
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchState:)];
    [_SHView_usedView addGestureRecognizer:tapGestureRecognizer];
    return _SHView_usedView;
}

- (UIView *)SHView_timeView {
   
    if (!_SHView_timeView) {
        
    }
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchState:)];
    [_SHView_timeView addGestureRecognizer:tapGestureRecognizer];
    return _SHView_timeView;
}

- (NSString *)sortMethodStr {
    if (!_sortMethodStr) {
        _sortMethodStr =  @"unreadCount";//unreadCount
    }
    return _sortMethodStr;
}
- (void)setSortMethodStr:(NSString *)sortMethodStr {
    if (_sortMethodStr != sortMethodStr) {
        _sortMethodStr = sortMethodStr;
    }
}

- (ScreeningState)screeningState {
    if (!_screeningState) {
        _screeningState = screening_Descending;
    }
    return _screeningState;
}
- (void)setScreeningState:(ScreeningState)screeningState  {
    if (_screeningState != screeningState) {
        _screeningState = screeningState;
    }
}

#pragma mark - private methods
- (void)searchState:(UITapGestureRecognizer*)tap
{
    switch ([tap.view tag]) {
        case 1031:
            self.patternState = patternState_balance;
            break;
        case 1032:
            self.patternState = patternState_used;
            break;
        case 1033:
            self.patternState = patternState_time;
            break;
        default:
            self.patternState = patternState_ordinary;
            break;
    }
     if(self.patternState != patternState_ordinary) [self isRedDotSort:NO];
    
    [self sitchChoose:tap.view withConfigs:@[@"降序",@"升序"] textVlueAry:@[ NSLocalizedString(@"balance",@"剩余流量"),NSLocalizedString(@"used",@"总剩余流量吧？"), NSLocalizedString(@"time",@"最近使用时间")]  withPattemState:self.patternState];
}

- (void)sitchChoose:(UIView*)judgmentView withConfigs:(NSArray*)imgAry textVlueAry:(NSArray*)vlueAry withPattemState:(PatternState )state
{
    _SHView_timeView.userInteractionEnabled = NO;
    _SHView_usedView.userInteractionEnabled = NO;
    _SHView_balanceView.userInteractionEnabled = NO;
    
    if (judgmentView.tag == 1031) {
        self.sortMethodStr = @"remain";
        if ([self.SHView_balanceLabel.textColor isEqual:[UIColor grayColor]]) { //Label 颜色
            self.SHView_balanceLabel.textColor = [UIColor whiteColor];
        }else {
            if (self.balanceMarkers == 1) {  // 排序符号
                self.balanceMarkers = 0;
            }
            else  {
                self.balanceMarkers++;
            }
            self.SHView_balanceLabel.text = [NSString stringWithFormat:@"%@",[vlueAry objectAtIndex:state -1]];
        }
        
        [self prohibitOptionWithState:state withAry:imgAry];
    }
    else if(judgmentView.tag == 1032) {
        self.sortMethodStr = @"total";
        if ([self.SHView_usedLabel.textColor isEqual:[UIColor grayColor]]) {
            self.SHView_usedLabel.textColor = [UIColor whiteColor];
           
        }else {
            if (self.usedMarkers == 1) {
                self.usedMarkers = 0;
            }
            else {
                self.usedMarkers ++;
            }
            self.SHView_usedLabel.text = [NSString stringWithFormat:@"%@",[vlueAry objectAtIndex:state - 1]];
        }
        [self prohibitOptionWithState:state withAry:imgAry];
    }
    else if (judgmentView.tag == 1033) {
        self.sortMethodStr = @"latestUse";
        if ([self.SHView_timeLabel.textColor isEqual:[UIColor grayColor]]) {
            self.SHView_timeLabel .textColor = [UIColor whiteColor];
        }else {
            if (self.timeMarkers == 1) {
                self.timeMarkers = 0;
            }
            else {
                self.timeMarkers++;
            }
            self.SHView_timeLabel.text = [NSString stringWithFormat:@"%@",[vlueAry objectAtIndex:state -1]];
        }
        [self prohibitOptionWithState:state withAry:imgAry];
    }else {
        self.sortMethodStr = @"unreadCount";
        if ([self.SHView_balanceLabel.textColor isEqual:[UIColor whiteColor]]) {
            self.SHView_balanceLabel.textColor = [UIColor grayColor];
        }
        
        if ([self.SHView_usedLabel.textColor isEqual:[UIColor whiteColor]]) {
            self.SHView_usedLabel.textColor = [UIColor grayColor];
        }
        
        if ([self.SHView_timeLabel.textColor isEqual:[UIColor whiteColor]]) {
            self.SHView_timeLabel.textColor = [UIColor grayColor];
        }

        [self prohibitOptionWithState:state withAry:imgAry];
    }
  // 3
    if (self.searchHeadDelegate != nil && [self.searchHeadDelegate respondsToSelector:@selector(searchHeadViewWithRefreshData)]) {
        [self.searchHeadDelegate searchHeadViewWithRefreshData];
    }
}

// 切换第二步
- (void)prohibitOptionWithState:(PatternState)patState withAry:(NSArray *)imgAry
{
   
    if (patState == patternState_balance) {
        self.SHView_usedLabel.textColor        = [UIColor grayColor];
        self.SHView_timeLabel.textColor    = [UIColor  grayColor];
        
        self.SHView_balanceImage.image  = [UIImage imageNamed:[imgAry objectAtIndex:self.balanceMarkers]];
        self.SHView_usedImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-off",[imgAry objectAtIndex:self.usedMarkers]]];
        self.SHView_timeImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-off",[imgAry objectAtIndex:self.timeMarkers]]];
        self.screeningState =  self.balanceMarkers ? screening_Ascending : screening_Descending; //升  ：降
    }
    else if (patState == patternState_used) {
        self.SHView_balanceLabel.textColor   = [UIColor grayColor];
        self.SHView_timeLabel.textColor    = [UIColor  grayColor];
        
        self.SHView_balanceImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-off",[imgAry objectAtIndex:self.balanceMarkers]]];
        self.SHView_usedImage.image = [UIImage imageNamed:[imgAry objectAtIndex:self.usedMarkers]];
        self.SHView_timeImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-off",[imgAry objectAtIndex:self.timeMarkers]]];
        self.screeningState =  self.usedMarkers ? screening_Ascending : screening_Descending; //升  ：降
    }
    else if (patState == patternState_time) {
        self.SHView_balanceLabel.textColor   = [UIColor grayColor];
        self.SHView_usedLabel.textColor        = [UIColor grayColor];
        
        self.SHView_balanceImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-off",[imgAry objectAtIndex:self.balanceMarkers]]];
        self.SHView_usedImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-off",[imgAry objectAtIndex:self.usedMarkers]]];
        self.SHView_timeImage.image = [UIImage imageNamed:[imgAry objectAtIndex:self.timeMarkers]];
        self.screeningState =  self.timeMarkers ? screening_Ascending : screening_Descending; //升  ：降
    }
    else {
        self.SHView_balanceImage.image  = [UIImage imageNamed:[NSString stringWithFormat:@"%@-off",[imgAry objectAtIndex:self.balanceMarkers]]];
        self.SHView_usedImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-off",[imgAry objectAtIndex:self.usedMarkers]]];
        self.SHView_timeImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@-off",[imgAry objectAtIndex:self.timeMarkers]]];
         _screeningState =  screening_Descending; // 降
    }
}

- (IBAction)searchHeadViewWithRegionSelection:(UIButton *)sender {
    if (self.searchHeadDelegate != nil && [self.searchHeadDelegate respondsToSelector:@selector(hairPromotionControlMethod)]) {
        [self.searchHeadDelegate hairPromotionControlMethod];
    }
}

- (void)resetSearchHeadWithView:(UIView *)view {

    NSLog(@"%@",view.gestureRecognizers[0]);
    if (self.patternState != patternState_ordinary) {
        [self searchState:nil];
    }
}

@end
