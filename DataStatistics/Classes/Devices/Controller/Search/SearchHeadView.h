//
//  SearchHeadView.h
//  DataStatistics
//
//  Created by Kang on 16/4/18.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  按钮类型
 */
typedef NS_ENUM(NSInteger, PatternState) {
    /**
     *  默认
     */
    patternState_ordinary   = 0,
    /**
     * 第1个按钮
     */
    patternState_balance    = 1,
    /**
     *  第2个按钮
     */
    patternState_used       = 2,
    /**
     *  第3个按钮
     */
    patternState_time       = 3
};

/**
 *  排序状态
 */
typedef NS_ENUM(NSInteger, ScreeningState) {
    /**
     *  默認 - 禁止状态
     */
    screeningState_Prohibit = 0,
    /**
     *   升序
     */
    screening_Ascending     = 1,
    /**
     *  降序
     */
    screening_Descending    = 2
};

@class SearchHeadView;
@protocol SearchHeadViewDelegate <NSObject>

- (void)searchHeadViewWithRefreshData;

- (void)hairPromotionControlMethod;

@end


@interface SearchHeadView : UIView
@property (strong, nonatomic) IBOutlet UIView *SHView_balanceView;
@property (strong, nonatomic) IBOutlet UIView *SHView_usedView;
@property (strong, nonatomic) IBOutlet UIView *SHView_timeView;
@property (strong, nonatomic) IBOutlet UIButton *SHView_regionSelectionBtn;

/**
 *  剩余
 */
@property (strong, nonatomic) IBOutlet UILabel *SHView_balanceLabel;
@property (strong, nonatomic) IBOutlet UIImageView *SHView_balanceImage;
/**
 *  总过滤
 */
@property (strong, nonatomic) IBOutlet UILabel *SHView_usedLabel;
@property (strong, nonatomic) IBOutlet UIImageView *SHView_usedImage;
/**
 *  最近时间
 */
@property (strong, nonatomic) IBOutlet UILabel *SHView_timeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *SHView_timeImage;


#pragma  mark -Custom
/**
 *  保存请求参数 - 类型
 */
@property (strong, nonatomic) NSString *sortMethodStr;
@property (assign, nonatomic) PatternState patternState;
@property (assign, nonatomic) ScreeningState screeningState;
@property (setter=isRedDotSort:, nonatomic) BOOL redDotSort; //默认是 红点排序
@property (weak, nonatomic) id<SearchHeadViewDelegate> searchHeadDelegate;



- (IBAction)searchHeadViewWithRegionSelection:(UIButton *)sender;
/**
 *  重置 -回到默认状态
 */
- (void)resetSearchHeadWithView:(UIView *)view;
@end




