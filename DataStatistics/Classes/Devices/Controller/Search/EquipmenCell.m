//
//  EquipmenCell.m
//  DataStatistics
//
//  Created by Kang on 15/12/10.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "EquipmenCell.h"

@interface EquipmenCell ()
/** 颜色块 */
@property (strong, nonatomic) IBOutlet UIView *colorView;
/** 设备型号 */
@property (strong, nonatomic) IBOutlet UILabel *equipmen;
/** 地区 */
@property (strong, nonatomic) IBOutlet UILabel  *area;
/** 未使用天数 */
@property (strong, nonatomic) IBOutlet UILabel *uselessDays;
/** 剩余流量  */
@property (strong, nonatomic) IBOutlet UILabel *flow;
/** 过期背景图片 */
@property (strong, nonatomic) IBOutlet UIImageView *Viewimage;
/** 过期中间提示 */
@property (strong, nonatomic) IBOutlet UILabel *deeleteAfterLabel;
/** 国旗 */
@property (strong, nonatomic) IBOutlet UIImageView *imageCountry;
/** 过滤流量标题  */
@property (strong, nonatomic) IBOutlet UILabel *usedTile;
/** 剩余流量标题 */
@property (strong, nonatomic) IBOutlet UILabel *balanceTile;
/** 过滤流量 */
@property (strong, nonatomic) IBOutlet UILabel *totalVlue;

@end

@implementation EquipmenCell

- (void)awakeFromNib {
    // Initialization code
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark -设置Cell数据
- (void)setvalueWithList:(EListModel *)List {
    if (List != nil) {
    
        self.equipmen.text  = List.deviceCode;
        self.area.text           = List.c_area;
        self.flow.text              = [NSString stringWithFormat:@"%@L",[UtilToolsClss aryFloatValue:List.remain]];
        self.totalVlue.text = [NSString stringWithFormat:@"%@L",[UtilToolsClss aryFloatValue:List.total_flow]];
        NSDate *date = [UtilToolsClss   dateFromString:List.latestUse withDateFormat:@"yyyy/MM/dd HH:mm:ss"]; //现在是正确的时区
        NSString *dateStringFormatter = [UtilToolsClss timeTransFormStringFormatForLess24h:date];
        self.uselessDays.text =dateStringFormatter;
        
        if (List.c_country != nil) {
        self.imageCountry.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",List.c_country]];
        }
        self.Viewimage.hidden = [self getDevicesState:List.state];
        self.deeleteAfterLabel.hidden =  [self getDevicesState:List.state];
        //[self judgeBtnVlue:List.unreadCount];
    
    }
}
/// 判断小标签是否有值
- (void)judgeBtnVlue:(int)btnVlue isDetermineClick:(BOOL)click {
   // [self.btnVlue addObserver:self forKeyPath:@"hidden" options:NSKeyValueObservingOptionNew context:@"xxx"];
   // [self.delegate judgeVlue:self.btnVlue BtnVlue:btnVlue isDetermineClick:click];
    if (click) {
        
          self.btnVlue.hidden = click;
        [self.btnVlue setTitle:[NSString stringWithFormat:@"0"] forState:UIControlStateNormal];
    }else {
        if (btnVlue != 0) {
            self.btnVlue.hidden = NO;
            [self.btnVlue setTitle:[NSString stringWithFormat:@"%d",btnVlue] forState:UIControlStateNormal];
        }else {
            self.btnVlue.hidden = YES;
        }
    }
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
 //   UITextView *mTextView = object;
    NSLog(@"the textView content is change!!");
}


/// 来判断状态
- (BOOL)getDevicesState:(int)IntState {
    BOOL isState;
    if (IntState != 0) {
        switch (IntState) {
            case 1:
                 self.colorView.backgroundColor = RGB(0, 255, 0, 1);
                isState = YES;
                break;
            case 2:
                isState = YES;
                break;
            case 3:
                isState = NO;
                break;
            default:
                isState = NO; //如果是其他的数字就显示 过期。
                break;
        }

    }else {
        NSLog(@"\n-->IntState is %d",IntState);
    }
    
    return isState;
}







@end
