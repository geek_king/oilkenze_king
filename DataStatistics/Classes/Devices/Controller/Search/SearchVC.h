//
//  SearchVC.h
//  DataStatistics
//
//  Created by Kang on 15/10/13.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "SearchHeadView.h"
#import "SearchRegionChoiceView.h"
#import "DTKDropdownMenuView.h"


/**
 *  地区选择级数
 */
typedef NS_ENUM(NSInteger, stageState) {
    /**
     *   国家
     */
    stageState_Country,
    /**
     *  省
     */
    stageState_Province,
    /**
     *  城市
     */
    stageState_City
};


typedef void(^searchReturnVlue)(id returnValue);

@class SearchVC;
@protocol SearchDelegate <NSObject>
- (void)SearchNum:(SearchVC *)search;
@end
@interface SearchVC : UIViewController <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,NSFetchedResultsControllerDelegate>

singleton_for_interface(SearchVC)
@property (assign, nonatomic) id <SearchDelegate>searchDelegate;

/**
 *  模型管理者
 */
@property (strong, nonatomic) NSManagedObjectContext     *manageobjectContext;
@property (strong, nonatomic) NSFetchedResultsController *SVC_fetchedRessults;
/** 存放获取到的数据 */
@property (strong, nonatomic) NSMutableArray* searchModelAry;
/**
 * 列表页数
 */
@property (assign, nonatomic) int                        page;
/**
 *  判断列表状态 （YES - 刷新状态， NO - 加载状态）
 */
@property (assign, nonatomic) BOOL                       isToSearchUpdate;

/**
 *  通知来时是否刷新界面
 */
@property (assign, nonatomic)  BOOL isSearchJumpLiet;

/**
 *  搜索输入框
 */
@property (retain, nonatomic) UITextField *searchTextField;
/**
 *  主要的TableView
 */
@property (weak, nonatomic  ) IBOutlet UITableView                *searchTableView;
/**
 *  地区背景View
 */
@property (strong, nonatomic) IBOutlet SearchRegionChoiceView *pickerView;
/**
 *  判断点击后，是否完成了请求
 */
@property (getter=isClickFinished, nonatomic) BOOL  clickFinished;



/**
 *  提供给Tab 跳转页面
 *
 *  @param vlue 接收任意值 - id
 */
- (void)JumpWithVlue:(id)vlue;
- (void)setupUnreadCount;
// 计算 - 减
- (void)subtractUnreadCount:(int)count;
// 计算 - 加
- (void)addUnreadCount:(id)vlue;

@end
