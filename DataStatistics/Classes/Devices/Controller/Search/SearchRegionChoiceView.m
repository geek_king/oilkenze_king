//
//  SearchRegionChoiceView.m
//  DataStatistics
//
//  Created by Kang on 16/4/18.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "SearchRegionChoiceView.h"

@interface SearchRegionChoiceView ()


/**
 *  头部View高度
 */
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *headTipsViewHigh;
/**
 *  中部View高度
 */
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *centralSectionViewHigh;

/**
 *  记录选择省份的下标
 */
@property (assign, nonatomic) int provinceInt;
/**
 *  记录选择城市的下标
 */
@property (assign, nonatomic) int cityInt;
/**
 *  保存上一级选择的国家
 */
@property (strong, nonatomic) NSString *countriesStr;
/**
 *  保存上一级选择的省份
 */
@property (strong, nonatomic) NSString *provinceStr;
/**
 *  本地语言标识
 */
@property (strong, nonatomic) NSString *localStr;

@end



@implementation SearchRegionChoiceView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 */

#pragma mark - event response

+ (void)initialize {
  
  
}

- (void)drawRect:(CGRect)rect {
    self.cityTableView.delegate = self;
    self.cityTableView.dataSource = self;
}


#pragma mark - UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger row;
    if (self.stageState == regionStage_Country) {
        self.SRCView_pickViewHigh = [self.SRCView_countryAry count]  * 35 + 100;
        row = self.SRCView_countryAry.count;
    }
    else if (self.stageState == regionStage_Province) {
        NSString *state1 = [self.SRCView_provinceAry[self.provinceInt][0] objectForKey:@"name"];
        NSLog(@"%@",state1);
        if ([self.SRCView_provinceAry[self.provinceInt] count]==1 &&[state1 isEqualToString:@""]) {
            self.cityInt = 0;
            self.stageState = regionStage_City;
            [self.cityTableView reloadData];
        }else {
            self.SRCView_pickViewHigh = [self.SRCView_provinceAry[self.provinceInt] count]  * 35 + 100;
            row = [self.SRCView_provinceAry[self.provinceInt] count];
        }
        
    }
    else {
        NSString *city =[[self.SRCView_provinceAry[self.provinceInt] objectAtIndex:self.cityInt] objectForKey:@"citys"][0];
        NSLog(@"%@",city);
        if ([[[self.SRCView_provinceAry[self.provinceInt] objectAtIndex:self.cityInt] objectForKey:@"citys"] count]==1 && [city isEqualToString:@""]) {
            self.stageState = regionStage_Country;
            //[self doneButClick:nil];
            // [self.searchCityTableView reloadData];
            row = self.SRCView_countryAry.count;
            
        }else{
            
            self.SRCView_pickViewHigh  =[[[self.SRCView_provinceAry[self.provinceInt] objectAtIndex:self.cityInt] objectForKey:@"citys"] count]  * 35 + 100 ;
            
            row = [[[self.SRCView_provinceAry[self.provinceInt] objectAtIndex:self.cityInt] objectForKey:@"citys"] count];
        }
    }
    return row;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"568.0")) {
        return 30;
    }
    return 35;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     static NSString *cityID = @"cityID";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cityID];
    cell.backgroundColor = RGB(38, 39, 39, 1);
    cell.textLabel.textColor = RGB(220, 220, 220, 1);
    cell.textLabel.font = [UIFont systemFontOfSize:12];
    if (self.stageState == regionStage_Country) {
        cell.textLabel.text = self.SRCView_countryAry[indexPath.row];
    }else if(self.stageState == regionStage_Province) {
        cell.textLabel.text = [[self.SRCView_provinceAry[self.provinceInt] objectAtIndex:indexPath.row] objectForKey:@"name"];
    }else {
        cell.textLabel.text = [[[self.SRCView_provinceAry[self.provinceInt] objectAtIndex:self.cityInt] objectForKey:@"citys"] objectAtIndex:indexPath.row];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     if (self.stageState == regionStage_Country) { // 国家
     
     self.stageState = regionStage_Province; // 状态
     self.SRCView_searchValueStr = self.SRCView_countryAry[indexPath.row]; //字符串
     [self.SRCView_pitchOnVlue setTitle:self.SRCView_searchValueStr  forState:UIControlStateNormal];
     self.provinceInt = (int)indexPath.row; //保留个数
     [self automatiPickerHeightAdjustmentwithAry:self.SRCView_provinceAry[self.provinceInt]];
     [self.cityTableView reloadData];
      self.countriesStr = self.SRCView_searchValueStr;
     
     }
     else if (self.stageState == regionStage_Province) { // 省
     
     self.stageState = regionStage_City;
     self.SRCView_searchValueStr = [[self.SRCView_provinceAry[self.provinceInt] objectAtIndex:indexPath.row] objectForKey:@"name"];
     [self.SRCView_pitchOnVlue setTitle:[NSString stringWithFormat:@"%@ - %@",self.countriesStr,self.SRCView_searchValueStr]  forState:UIControlStateNormal];
     self.cityInt = (int)indexPath.row; //保留个数
     [self automatiPickerHeightAdjustmentwithAry:[[self.SRCView_provinceAry[self.provinceInt] objectAtIndex:self.cityInt] objectForKey:@"citys"]];
     [self.cityTableView reloadData];
     self.provinceStr = self.SRCView_searchValueStr;
     }
     else { // 市
      // [header beginRefreshing];

     self.SRCView_searchValueStr = [[[self.SRCView_provinceAry[self.provinceInt] objectAtIndex:self.cityInt] objectForKey:@"citys"] objectAtIndex:indexPath.row];
     [self.SRCView_pitchOnVlue setTitle:[NSString stringWithFormat:@"%@ - %@ - %@",self.countriesStr,self.provinceStr,self.SRCView_searchValueStr]  forState:UIControlStateNormal];
        
     [self refreshSearch];
     //  [self.regionSelection setTitle:self.searchStr forState:UIControlStateNormal];
    [self hidePicker:nil];
     }
     
}



#pragma mark - private methods

- (void)hidePicker:(id)picker {
     self.isOpenAreaValue = YES;
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(pickerViewWillHide:)]) {
         [self.delegate pickerViewWillHide:self];
    }
}

- (IBAction)submitAndRequest:(UIButton *)sender {
    if ([self.SRCView_searchValueStr isEqualToString:@""]) {
        [self refreshSearch];
        [self.SRCView_pitchOnVlue setTitle:NSLocalizedString(@"all areas", nil) forState:UIControlStateNormal];
    }else {
        [self refreshSearch];
        [self.SRCView_pitchOnVlue setTitle:self.SRCView_searchValueStr forState:UIControlStateNormal];
    }
    [self hidePicker:sender];
}

- (IBAction)clearSelectedValue:(UIButton *)sender {
    
    if (![self.SRCView_searchValueStr isEqualToString:@""]) {
        self.isOpenAreaValue = NO;
        [self.SRCView_pitchOnVlue setTitle:NSLocalizedString(@"all areas", nil) forState:UIControlStateNormal ];
        self.stageState = regionStage_Country;
        [self.cityTableView reloadData];
        self.SRCView_searchValueStr = @"";
        [self refreshSearch];
    }
}

- (void)automatiPickerHeightAdjustmentwithAry:(NSMutableArray *)mutAry {
    if (SCREEN_HEIGHT == 480) {
        if (mutAry.count <= 0) {
            
        }else if (mutAry.count > 0  && mutAry.count < 3) {
            CGFloat ft = mutAry.count *30;
            self.height = ft + self.headTipsViewHigh.constant + self.centralSectionViewHigh.constant;
        }else {
            self.height = 200;
        }
    }
}

- (void)refreshSearch {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(refreshSearchData:)]) {
        [self.delegate refreshSearchData:self];
    }

}


#pragma mark - getters and setters
- (NSString *)localStr {
    if (!_localStr) {
        _localStr = [NSString stringWithFormat:@"%d",[UtilToolsClss judgeLocalLanguage]];
    }
    return _localStr;
}

- (NSString *)countriesStr {
    if (!_countriesStr) {
        _countriesStr = @"";
    }
    return _countriesStr;
}

- (NSString *)provinceStr {
    if (!_provinceStr) {
        _provinceStr = @"";
    }
    return _provinceStr;
}

- (NSString *)SRCView_searchValueStr {
    if (!_SRCView_searchValueStr) {
        _SRCView_searchValueStr = @"";
    }
    return  _SRCView_searchValueStr;
}

- (UIButton *)SRCView_doneBtn {
    if (!_SRCView_doneBtn) {
        
    }
    [_SRCView_doneBtn setTitle:NSLocalizedString(@"done", @"提交") forState:UIControlStateNormal];
    
    return _SRCView_doneBtn;
}

- (UIButton *)SRCView_pitchOnVlue {
    if (!_SRCView_pitchOnVlue) {
        
    }
    [_SRCView_pitchOnVlue setTitle:NSLocalizedString(@"all areas", @"全部地区") forState:UIControlStateNormal];
    return _SRCView_pitchOnVlue;
}

- (RegionStageState)stageState {
    if (!_stageState) {
        _stageState = regionStage_Country;
    }
    return _stageState;
}

- (BOOL)isOpenAreaValue {
    if (!_isOpenAreaValue) {
        _isOpenAreaValue = NO;
    }
    return _isOpenAreaValue;
}

- (void)setSRCView_pickViewHigh:(float)SRCView_pickViewHigh {
    if (SRCView_pickViewHigh > 304) {
        SRCView_pickViewHigh = 304;
    }
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(pickViewSetHigh:)]) {
        [self.delegate pickViewSetHigh:SRCView_pickViewHigh];
    }
    
    _SRCView_pickViewHigh = SRCView_pickViewHigh;
}


@end
