//
//  CtrlCodeScan.h
//  WisdomSchoolBadge
//
//  Created by Kang on 16/1/12.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


@protocol CtrlCodeScanDelegate <NSObject>
/**
 * info - 获取到扫描结果的返回值
 */
@optional
- (void)didCodeScanOk:(id)info;
@end

@interface CtrlCodeScan : UIViewController <AVCaptureMetadataOutputObjectsDelegate>
/** 整个摄像头背景 */
@property(nonatomic, weak) IBOutlet UIView     *overlayer;
@property (strong, nonatomic) IBOutlet UILabel *Scantitle;

@property(nonatomic, weak) id<CtrlCodeScanDelegate>      delegate;
/** 音视频采集装置 */
@property(strong, nonatomic) AVCaptureDevice            *device;
/** 输入 */
@property(strong, nonatomic) AVCaptureDeviceInput       *input;
/** 数据输出 */
@property(strong, nonatomic) AVCaptureMetadataOutput    *output;
/** 会话 */
@property(strong, nonatomic) AVCaptureSession           *session;
/** 预览 */
@property(strong, nonatomic) AVCaptureVideoPreviewLayer *preview;
/** 扫描框 */
@property(nonatomic, strong) UIView                     *scanFrame;
/** 线条 */
@property(nonatomic, strong) UIView                     *line;
@property(nonatomic, assign) BOOL                        isLightOn;

- (IBAction)OnTopBackDown:(UIButton *)sender;

@end
