//
//  SystemInfoController.m
//  DataStatistics
//
//  Created by YTYanK on 16/4/24.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "SystemInfoController.h"
#import "EquipmenDetailsViewModel.h"
#import "DeviceDetailsModel.h"

@interface SystemInfoController () {
    
    IBOutlet NSLayoutConstraint *equipmentImage_H;
    IBOutlet NSLayoutConstraint *equipmentImage_to;
    IBOutlet NSLayoutConstraint *addEquipmentBtn_to;
    IBOutlet NSLayoutConstraint *tableView_H;
}
@property (weak, nonatomic) IBOutlet UINavigationItem *info_navItem;
@property (strong, nonatomic) NSArray *infoAry;
@property (copy, nonatomic) NSString *scanStr;
@end

@implementation SystemInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self upadateUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (SCREEN_HEIGHT == 480) {
        equipmentImage_to.constant = 10;
        equipmentImage_H.constant  = 120;
        tableView_H.constant = 210;
        addEquipmentBtn_to.constant = 20;
    }else  if(SCREEN_HEIGHT == 568){
        equipmentImage_to.constant = 35;
        equipmentImage_H.constant = 120;
        tableView_H.constant = 265;
        addEquipmentBtn_to.constant = 20;
    }else if (SCREEN_HEIGHT == 667) {
        equipmentImage_H.constant = 130;
        equipmentImage_to.constant = 40;
        tableView_H.constant = 300;
        addEquipmentBtn_to.constant = 40;
    }else if (SCREEN_HEIGHT == 736) {
        equipmentImage_to.constant = 40;
        tableView_H.constant = 360;
        addEquipmentBtn_to.constant = 35;
    }

}



- (void)upadateUI {
   if ( [ROLEDODE isEqualToString:@"001"] || [ROLEDODE isEqualToString:@"005"])  {
       self.addSystemBtn.hidden = YES;
   }else {
       self.addSystemBtn.hidden = NO;
       
   }
    
    [self.addSystemBtn setTitle:NSLocalizedString(@"EIaddBtn", nil) forState:UIControlStateNormal];
    self.infoTableView.userInteractionEnabled = NO;
    self.infoTableView.separatorColor = [UIColor grayColor];
    self.infoTableView.backgroundColor = [UIColor clearColor];
    [self initNavigationItem];
}

- (NSString *)updateViewDataAtIndexPath: (NSIndexPath *)indexPath {
    NSDate *beginDate; NSString *beginTime;
    NSArray *ary = self.detailsAry;
    __weak NSString *constStr = @"---";
    __weak NSString *ontainerStr = @"";
    
    switch (indexPath.row) {
        case 0:
            ontainerStr = [NSString stringWithFormat:@"%@",[ary[0] cname]];//cname
            break;
        case 1:
            if (![ary[0] deviceCode]) {
                self.addSystemBtn.hidden = YES;
            }
            ontainerStr = [NSString stringWithFormat:@"%@",[ary[0] deviceCode]];
            break;
        case 2:
            ontainerStr = [NSString stringWithFormat:@"%@",[ary[0] id_num]];
            break;
        case 3:
            ontainerStr = [NSString stringWithFormat:@"%@",[ary[0] software_version]];
            break;
        case 4:
            ontainerStr = [NSString stringWithFormat:@"%@",[ary[0] begin]];
            beginDate = [UtilToolsClss dateFromString:[ary[0] begin] withDateFormat:@"yyyy-MM-dd"];
            beginTime = [UtilToolsClss stringFromDate:beginDate withDateFormat:@"MM/dd/yyyy"];
            ontainerStr = beginTime;
            break;
        case 5:
            ontainerStr =[ary[0] contract]? [NSString stringWithFormat:NSLocalizedString(@" %d months", nil),[ary[0] contract]]:@"---";
            break;
            
        default:
            break;
    }
    
    if ([ontainerStr isEqualToString:@"(null)"] || ontainerStr == nil)
        ontainerStr = constStr;
    
    return ontainerStr;
}

- (void)initNavigationItem {
    self.navigationController.navigationBar.backgroundColor = RGB(40, 255, 255, 1);
    self.info_navItem.title = NSLocalizedString(@"EIVCtitle",nil);
    self.info_navItem.leftBarButtonItem = [UIBarButtonItem BarButtonItemWithImageName:@"返回" highImageName:@"返回" title:NSLocalizedString(@"",nil) target:self action:@selector(ReturnBtn)];
}

- (IBAction)addSystem:(UIButton *)sender {
    NSDictionary *parameters = @{@"deviceQRCode":[self.scanStr substringFromIndex:(self.scanStr.length - 22)]};
       [EquipmenDetailsViewModel requestWithUrlForDeviceBinding:self andParam:parameters success:^(id model) {
           if (self.delegate != nil && [self.delegate respondsToSelector:@selector(dismissViewControllerWithDeviceVlue:)]) {
               [self.delegate dismissViewControllerWithDeviceVlue:model];
           }
           [self dismissViewControllerAnimated:YES completion:^{}];
       } failure:^(NSString *error) {
            [MBProgressHUD yty_showErrorWithTitle:nil detailsText:error toView:nil];
       }];
    
}
- (void)ReturnBtn {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.infoAry.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cellHeight =[self tableView:tableView cellForRowAtIndexPath:indexPath];
    float heightFloat = CGRectGetHeight(cellHeight.frame);
    
    if (SCREEN_HEIGHT == 480) {
        heightFloat = 35;
    }else if (SCREEN_HEIGHT == 667){
        heightFloat = 50;
    }else if (SCREEN_HEIGHT == 736) {
        heightFloat = 60;
    }
    return heightFloat;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *idenfitier = @"coustomerList";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:idenfitier];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont systemFontOfSize:15.f];
    cell.textLabel.textColor = [UIColor grayColor];
    cell.textLabel.text = self.infoAry[indexPath.row];
    cell.detailTextLabel.textColor = [UIColor whiteColor];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:15.f];
    cell.detailTextLabel.text =[self updateViewDataAtIndexPath:indexPath];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (NSString *)scanStr {
    if (!_scanStr) {
        _scanStr = SCANINFO;
    }
    return _scanStr;
}

- (NSArray *)infoAry {
    if (!_infoAry) {
        _infoAry = [[NSArray alloc] initWithObjects:  NSLocalizedString(@"EIcompany", @"公司"),
                     NSLocalizedString(@"EIdevice", @"设备ID"),
                     NSLocalizedString(@"EIcard", @"IcCard"),
                     NSLocalizedString(@"EIversion", nil),
                    NSLocalizedString(@"EIsigned", nil),
                    NSLocalizedString(@"EIcontract", nil),nil];
    }
    return _infoAry;
}

@end
