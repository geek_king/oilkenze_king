//
//  SystemInfoController.h
//  DataStatistics
//
//  Created by YTYanK on 16/4/24.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EquipmenDetailsViewModel;
@class SystemInfoController;
@protocol SystemInfoControllerDelegate <NSObject>
@optional
- (void)dismissViewControllerWithDeviceVlue:(NSString *)Str;
@end

@interface SystemInfoController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) id <SystemInfoControllerDelegate>delegate;
@property (strong, nonatomic) NSArray * detailsAry;

@property (weak, nonatomic) IBOutlet UITableView *infoTableView;
@property (strong, nonatomic) IBOutlet UIButton *addSystemBtn;
- (IBAction)addSystem:(UIButton *)sender;
@end
