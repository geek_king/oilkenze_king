//
//  EquipmentInformation.h
//  DataStatistics
//
//  Created by Kang on 16/1/15.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EquipmentInformationDelegate <NSObject>

@optional
- (void)dismissViewControllerWithDevicesVlue:(NSString *)Str;
@end


@interface EquipmentInformation : UIViewController

@property (weak, nonatomic) id <EquipmentInformationDelegate> einfoDelegate;
@property (strong, nonatomic) NSMutableArray *dataAry;//接收数据
@property (strong, nonatomic) NSString *scanStr;
/** 公司名*/
@property (strong, nonatomic) IBOutlet UILabel *CompanyVlue;
/** 设备ID */
@property (strong, nonatomic) IBOutlet UILabel *deviceIDVlue;
/** 3G 卡 */
@property (strong, nonatomic) IBOutlet UILabel *cardVlue;
/** 版本 */
@property (strong, nonatomic) IBOutlet UILabel *versionVlue;
/** 出厂日期 */
@property (strong, nonatomic) IBOutlet UILabel *factoryTimeVlue;
/** 签署 */
@property (strong, nonatomic) IBOutlet UILabel *signedAtVlue;
/** 合同 */
@property (strong, nonatomic) IBOutlet UILabel *contractTermVlue;



@end
