//
//  EquipmentInformation.m
//  DataStatistics
//
//  Created by Kang on 16/1/15.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "EquipmentInformation.h"
#import "EquipmenDetailsViewModel.h"
//#import "SidebarViewController.h"
#import "EquipmenModel.h"
@interface EquipmentInformation ()
{
    
    IBOutlet UILabel *companyLabel;
    IBOutlet UILabel *deviceIDLabel;
    IBOutlet UILabel *cardLabel;
    IBOutlet UILabel *versionLabel;
    IBOutlet UILabel *factoryLabel;
    IBOutlet UILabel *signedLabel;
    IBOutlet UILabel *contractLabel;
    IBOutlet UIButton *addBtn;
    
    
    
    IBOutlet NSLayoutConstraint *H_toImag;
    IBOutlet NSLayoutConstraint *H_viewAndBtn;
    IBOutlet NSLayoutConstraint *H_imagAndview;
    IBOutlet UINavigationItem *navItem;
}

@property (strong, nonatomic) IBOutlet UINavigationBar *navigBar;
//保存错误code
@property (assign, nonatomic) int returnCode;
// 错误提示
@property (copy, nonatomic) NSString *errorStr;
// 设备编号
@property (copy, nonatomic) NSString *EquiNo;
//@property  (strong, nonatomic) SidebarViewController *side;
@end

@implementation EquipmentInformation

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavigationItem];
    self.scanStr = SCANINFO;
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initView];
    // 获取数据
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initNavigationItem {
    //self.navigBar.backgroundColor = RGB(40, 40, 40, 1);
    self.navigationController.navigationBar.backgroundColor = RGB(40, 255, 255, 1);
    navItem.title = NSLocalizedString(@"EIVCtitle",nil);
    navItem.leftBarButtonItem = [UIBarButtonItem BarButtonItemWithImageName:@"返回" highImageName:@"返回" title:NSLocalizedString(@"",nil) target:self action:@selector(ReturnBtn)];
}
- (void)ReturnBtn {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 初始化view-
- (void)initView {
    companyLabel.text  = NSLocalizedString(@"EIcompany", nil);
    deviceIDLabel.text = NSLocalizedString(@"EIdevice", nil);
    cardLabel.text     = NSLocalizedString(@"EIcard", nil);
    versionLabel.text  = NSLocalizedString(@"EIversion", nil);
    factoryLabel.text  = NSLocalizedString(@"EIfactory", nil);
    signedLabel.text   = NSLocalizedString(@"EIsigned", nil);
    contractLabel.text = NSLocalizedString(@"EIcontract", nil);
    [addBtn setTitle:NSLocalizedString(@"EIaddBtn", nil) forState:UIControlStateNormal];
    
    
    NSArray *ary  =  [EquipmenDetailsViewModel sharedEquipmenDetailsViewModel].SidebarListDatas;
    NSLog(@"%@",[ary[0] factory_date]);
    
    __weak NSString *ontainerStr = @"";
    
    ontainerStr = [NSString stringWithFormat:@"%@",[ary[0] cname]];
    if (![ontainerStr isEqualToString:@"(null)"] && ![[ary[0] cname] isEqual:[NSNull null]])
        self.CompanyVlue.text  = [NSString stringWithFormat:@" %@",ontainerStr];
   
    ontainerStr = [NSString stringWithFormat:@"%@",[ary[0] deviceCode]];
    if (![ontainerStr isEqualToString:@"(null)"] && ![[ary[0] deviceCode] isEqual:[NSNull null]])
        self.deviceIDVlue.text = [NSString stringWithFormat:@" %@",ontainerStr];

     ontainerStr = [NSString stringWithFormat:@"%@",[ary[0] id_num]];
    if (![ontainerStr isEqualToString:@"(null)"] && ![[ary[0] id_num] isEqual:[NSNull null]])
        self.cardVlue.text     = [NSString stringWithFormat:@" %@",ontainerStr];
   
    ontainerStr =  [NSString stringWithFormat:@"%@",[ary[0] software_version]];
    if (![ontainerStr isEqualToString:@"(null)"] && ![[ary[0] software_version] isEqual:[NSNull null]])
        self.versionVlue.text  = [NSString stringWithFormat:@" %@",ontainerStr];
    
    ontainerStr = [NSString stringWithFormat:@"%@",[ary[0] factory_date]];
    if (![ontainerStr isEqualToString:@"(null)"] && ![[ary[0] factory_date] isEqual:[NSNull null]])
        self.factoryTimeVlue.text = [NSString stringWithFormat:@" %@",ontainerStr];
    
    ontainerStr = [NSString stringWithFormat:@"%@",[ary[0] begin]];
    if (![ontainerStr isEqualToString:@"(null)"] && ![[ary[0] begin] isEqual:[NSNull null]])
        self.signedAtVlue.text     = [NSString stringWithFormat:@" %@",ontainerStr];
    
    ontainerStr = [NSString stringWithFormat:@"%d",[ary[0] contract]];
    if (![ontainerStr isEqualToString:@"(null)"] )
        self.contractTermVlue.text  = [NSString stringWithFormat:@" %@ months",ontainerStr];
}

// 添加设备（绑定）
- (IBAction)AddEquipment:(UIButton *)sender {
    NSDictionary *parameters = @{@"deviceQRCode":[_scanStr substringFromIndex:(_scanStr.length - 22)]};
     EquipmenDetailsViewModel *viewmodel= [EquipmenDetailsViewModel sharedEquipmenDetailsViewModel];
    
    [viewmodel requestDataWithReturnUrl:@"deviceList/deviceBinding.asp" withViewController:self withParameters:parameters requestWithHeader:nil requestWithApiID:@"deviceBinding.asp" isMethod:true isListLoaded:false];
    
    [viewmodel setBlockWithReturnBlock:^(id returnValue) {
        DeviceBindingList *Bindinglist= returnValue[0];
         self.returnCode = Bindinglist.result;
         self.errorStr   = Bindinglist.error;
         self.EquiNo     = Bindinglist.deviceNo;
        
        //提示
        NSString *TipsStr = @"";
        if (self.returnCode != 1) {
            if (self.returnCode == 0) {
                TipsStr = self.errorStr;
            }else if (self.returnCode == 2){
                TipsStr = NSLocalizedString(@"EIP1", @"已绑定");
            }else if (self.returnCode == 3) {
                TipsStr = NSLocalizedString(@"EIP2", @"空间不足");
            }else if (self.returnCode == 4) {
                TipsStr = NSLocalizedString(@"EIP3", @"无法绑定");
            }else {
                TipsStr = NSLocalizedString(@"EIP4", @"绑定成功");
            }
            
        }else {
                
                if (_einfoDelegate != nil && [_einfoDelegate respondsToSelector:@selector(dismissViewControllerWithDevicesVlue:)]) {
                    [self.einfoDelegate dismissViewControllerWithDevicesVlue:self.EquiNo];
                }
                [self dismissViewControllerAnimated:YES completion:^{}];
        }
        if (![TipsStr isEqualToString:@""]) {
             [MBProgressHUD showError:TipsStr];
        }
    } WithErrorBlock:^(id errorCode) {
    } WithFailureBlock:^{
    }];
    
}
- (void)updateViewConstraints {
    [super updateViewConstraints];
    
    if (SCREEN_WIDTH != 414) {
        if (SCREEN_WIDTH == 320) {
            if (SCREEN_HEIGHT < 568) {
                H_toImag.constant = H_toImag.constant - 25;
                H_viewAndBtn.constant = H_viewAndBtn.constant  - 15;
                H_imagAndview.constant = H_imagAndview.constant - 10;
            }else {
                H_toImag.constant = H_toImag.constant -15;
                H_viewAndBtn.constant = H_viewAndBtn.constant  - 12;
            }
        }else {
           H_toImag.constant = H_toImag.constant - 10;
           H_viewAndBtn.constant = H_viewAndBtn.constant - 5;
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
