//
//  EquipmenModel.h
//  DataStatistics
//
//  Created by Kang on 15/12/9.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "JSONModel.h"

@protocol EListModel <NSObject>

@end

@interface EListModel : JSONModel
/** 设备ID */
@property (assign, nonatomic) NSInteger  deviceID;
/** 设备编号 */
@property (copy, nonatomic) NSString *deviceCode;
/** 客户名称 */
@property (copy, nonatomic) NSString *customer;
/** 剩余流量 */
@property (assign, nonatomic) float  remain;
/** 最近使用日期 */
@property (copy, nonatomic) NSString *latestUse;
/** 公司编号 */
@property (copy, nonatomic) NSString *c_no;
/** 地区名称 */
@property (copy, nonatomic) NSString *c_area;
/** 股东名称 */
@property (copy, nonatomic) NSString *c_contact;
/** 余额 */
@property (assign, nonatomic) double ic_money;
/** 单价 */
@property (assign, nonatomic) float price;
/** 运行状态  1、在线。2、离线。3、过期。4、默认 */
@property (assign, nonatomic) int  running_state;
/** 未读数量 */
@property (assign, nonatomic) int  unreadCount;
/** 状态标记 
 1：在线
 2：离线
 3：已过期（对应的量过期的设备）
 4：默认（查询全部设备）
*/
@property (assign, nonatomic) int  state;
/** 合约期 */
@property (assign, nonatomic) int contract;
/** 签约日期 */
@property (copy, nonatomic) NSString *begin;
/** 解约日期 */
@property (copy, nonatomic) NSString *end;
/** 上传时间 */
@property (copy, nonatomic) NSString *last_upload;
/** 总流量 */
@property (assign, nonatomic) float total_flow;

@property (copy, nonatomic) NSString *c_country;

@end

@interface EquipmenModel : JSONModel
singleton_for_interface(EquipmenModel)
@property (strong, nonatomic) NSMutableArray <EListModel,ConvertOnDemand> *EListModel;
/** 一共有的未读信息*/
@property (copy, nonatomic) NSString  *unreadAllCount;
@end
