//
//  SearchViewModel.h
//  DataStatistics
//
//  Created by Kang on 16/6/14.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"

@class EquipmenModel;
@class RemoveBindingModel;

@interface SearchViewModel : ViewModelClass
singleton_for_interface(SearchViewModel)

@property (strong, nonatomic) NSString    *unreadCount;

/**
 *  设备列表API
 *
 */
+(void)requestWithUrlForSearchIn:(id)obj andParam:(NSDictionary *)param success:(void(^)(EquipmenModel *model))success failure:(void(^)(NSString *error))failure;

/**
 *  删除设备API
 *
 */
+ (void)requestWithUrlForDeviceRemoveBinding:(id)obj andParam:(NSDictionary *)param success:(void(^)(RemoveBindingModel *model))success failure:(void(^)(NSString * error))failure;

/**
 *  跳转日志列表
 */
+ (void)jumpListDetailWithListModel:(EquipmenModel *)model forViewController:(id)vc;

@end
