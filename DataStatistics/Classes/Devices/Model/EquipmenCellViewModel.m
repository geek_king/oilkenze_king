//
//  EquipmenCellViewModel.m
//  DataStatistics
//
//  Created by Kang on 15/12/9.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "EquipmenCellViewModel.h"
#import "JSONModel.h"
#import "MapGPSViewController.h"
#import "EquipmenDetailsVC.h"


@implementation EquipmenCellViewModel
static EquipmenCellViewModel *equipmenVM = nil;

+ (EquipmenCellViewModel *)getEquipmenVM {
    if (!equipmenVM) {
        equipmenVM = [[super allocWithZone:NULL] init];
    }
    return equipmenVM;
}
+ (id)allocequipmenZone:(NSZone *)zone {
    return [self getEquipmenVM];
}
- (id)copyequipmenZone:(NSZone *)zone {
    return self;
}
- (id)init {
    if (equipmenVM) {
        return equipmenVM;
    }
    self = [super init];
    return self;
}


- (void)requestDataWithValueSuccess:(NSDictionary *)returnValue withViewController:(UIViewController *)vc judgeApiStrOrApiId:(id)api returnBlockVlue:(int)returnStatus {
 
    // 判断状态
    if (returnStatus != 1 ) {
        if(returnStatus == -2 || returnStatus == -1) {
            [super forceJumpViewController:vc];
        }
       // self.apiblock([NSArray array],vc,[NSString stringWithFormat:@"%d",returnStatus],api); return;
        self.returnblock (nil); return;
    }
    
   
    if ([api isEqualToString:@"deviceRemoveBinding.asp"]) { // 删除绑定API
        self.remBind = [[RemoveBindingModel alloc] initWithDictionary:returnValue error:nil];
      
         self.apiblock(self.remBind.RemoveBindingList,vc,[NSString stringWithFormat:@"%d",returnStatus],api);
    }else if([api isEqualToString:@"querAreas.asp"]) { // 城市列表API
    
        [SearchCityModel mj_setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"provinces":@"myprovinces",
                     };
        }];
        NSArray *searchAry = [SearchCityModel mj_objectArrayWithKeyValuesArray:[[returnValue objectForKey:@"body"] objectForKey:@"countrys"]];

        id comeBackVlue = self.receivevlueblock(searchAry);
        NSLog(@"---->%@",comeBackVlue);
    }else {
        
        // 设备列表API
        self.list = [[EquipmenModel alloc] initWithDictionary:returnValue error:nil];
        NSLog(@"%@",self.list);
        self.unreadCount  = self.list.unreadAllCount;

        if ([self.list.EListModel isEqual:[NSNull null]]) {
            // self.apiblock(@"---",vc,[NSString stringWithFormat:@"%d",returnStatus],api);
            self.returnblock(@"---");
        }else {
           self.returnblock (self.list.EListModel);
           //  self.apiblock(self.list.EListModel,vc,[NSString stringWithFormat:@"%d",returnStatus],api);
        }
    }
}


/// 跳转分页列表
- (void)equipmenListDetailWithListModel:(EListModel *)model adVlue:(id)vlue WithViewController:(SearchVC *)viewController {
    
//    NSDictionary *dic = nil;
//    if (vlue == nil) {
//        dic = @{@"deviceCode":[NSString stringWithFormat:@"%@",model.deviceCode]};
//    }else {
//        dic = @{@"deviceCode":[NSString stringWithFormat:@"%@",vlue]};
//    }
    [viewController performSegueWithIdentifier:@"jumpJournalList" sender:viewController];
    



    
}


@end
