//
//  DevicesListData+CoreDataProperties.m
//  DataStatistics
//
//  Created by Kang on 16/3/17.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DevicesListData+CoreDataProperties.h"

@implementation DevicesListData (CoreDataProperties)

@dynamic begin;
@dynamic c_area;
@dynamic c_contact;
@dynamic co_no;
@dynamic contract;
@dynamic customer;
@dynamic deviceCode;
@dynamic deviceID;
@dynamic end;
@dynamic ic_money;
@dynamic last_upload;
@dynamic latestUse;
@dynamic price;
@dynamic remain;
@dynamic running_state;
@dynamic state;
@dynamic total_flow;
@dynamic unreadCount;

@end
