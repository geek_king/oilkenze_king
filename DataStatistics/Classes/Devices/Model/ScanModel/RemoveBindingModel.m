//
//  RemoveBindingModel.m
//  DataStatistics
//
//  Created by Kang on 16/1/18.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "RemoveBindingModel.h"

@implementation RemoveBindingList
+(BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}
@end

@implementation RemoveBindingModel
+(JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"body.resultList":@"RemoveBindingList"}];
}

@end
