//
//  DeviceBindingModel.m
//  DataStatistics
//
//  Created by Kang on 16/1/18.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "DeviceBindingModel.h"

@implementation DeviceBindingList
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}
@end


@implementation DeviceBindingModel
+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"body.resultList":@"DeviceBindingList"}];
}

@end

