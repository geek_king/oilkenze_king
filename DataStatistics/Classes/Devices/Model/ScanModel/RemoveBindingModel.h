//
//  RemoveBindingModel.h
//  DataStatistics
//
//  Created by Kang on 16/1/18.
//  Copyright © 2016年 YTYangK. All rights reserved.
//


#import "JSONModel.h"

@protocol RemoveBindingList <NSObject>

@end

@interface RemoveBindingList : JSONModel
/** 设备编号 */
@property (copy, nonatomic) NSString  *deviceNo;
/** 解绑结果  0：失败 1：成功 2：设备未绑定 */
@property (assign, nonatomic) int  result;
/** 错误信息 */
@property (copy, nonatomic) NSString *error;

@end

@interface RemoveBindingModel : JSONModel

@property (strong, nonatomic) NSMutableArray  <RemoveBindingList,ConvertOnDemand> *RemoveBindingList;

@end
