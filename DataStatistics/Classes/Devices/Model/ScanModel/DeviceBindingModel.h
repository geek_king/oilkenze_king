//
//  DeviceBindingModel.h
//  DataStatistics
//
//  Created by Kang on 16/1/18.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol DeviceBindingList <NSObject>

@end

@interface DeviceBindingList : JSONModel
/** 设备编号 */
@property (copy, nonatomic) NSString *deviceNo;
/** 绑定结果 */
@property (assign, nonatomic) int result;
/** 错误信息 */
@property (copy, nonatomic) NSString *error;
@end


@interface DeviceBindingModel : JSONModel
@property (strong, nonatomic) NSMutableArray <DeviceBindingList,ConvertOnDemand> *DeviceBindingList;
@end

