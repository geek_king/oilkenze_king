//
//  EquipmenDetailsViewModel.h
//  DataStatistics
//
//  Created by Kang on 15/12/30.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"



@class DeviceDetailsModel;
@interface EquipmenDetailsViewModel : ViewModelClass
singleton_for_interface(EquipmenDetailsViewModel)

///**
// *  暴露给外面的数据
// */
//#warning !!!!!!!!
//@property (strong, nonatomic) NSArray *sidebarListDatas;
@property (weak, nonatomic) id obj;

/**
 *  设备详情API
 *
 */
+ (void)requestWithUrlForDeviceDetails:(id)obj andParam:(NSDictionary *)param
                               success:(void(^)(DeviceDetailsModel *model))success failure:(void(^)(NSString * error))failure;
/**
 *  设备绑定API
 *
 */
+ (void)requestWithUrlForDeviceBinding:(id)obj andParam:(NSDictionary *)param
                               success:(void(^)(id model))success failure:(void(^)(NSString * error))failure;
@end


