//
//  EquipmenDetailsViewModel.m
//  DataStatistics
//
//  Created by Kang on 15/12/30.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "EquipmenDetailsViewModel.h"
#import "DeviceDetailsModel.h"  //设备详情模型
#import "DeviceBindingModel.h"   // 添加设备模型

@interface EquipmenDetailsViewModel()
@end


@implementation EquipmenDetailsViewModel
singleton_for_implementation(EquipmenDetailsViewModel)


#pragma mark - 扫描设备的详情信息
+ (void)requestWithUrlForDeviceDetails:(id)obj andParam:(NSDictionary *)param
                               success:(void(^)(DeviceDetailsModel *model))success failure:(void(^)(NSString * error))failure {
    __weak typeof (ViewModelClass *) weakSuper = [ViewModelClass sharedViewModelClss];
    [[UtilToolsClss getUtilTools] addDoLoading];
    
    [NetRequestClss requestWithUrl:@"deviceList/querDetailList.asp" requestWithParameters:param method:NetMethodGET returnSuccess:^(id objs, int status, NSString *mag) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        
        if (status != 1) {
            if (status == -2 || status == -1) { 
                [weakSuper forceJumpViewController:obj];
            }
            failure(@"---"); return;
        }
        DeviceDetailsModel *listModel = [DeviceDetailsModel mj_objectWithKeyValues:[objs objectForKey:@"body"]];
        NSLog(@"listModel---> %@",listModel);
        if (![listModel isEqual:[NSNull null]]) {
            success(listModel);
            return;
        }
        failure(@"---");
        
    } returnError:^(NSString *err) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        [MBProgressHUD showError:err];
    }];
}

#pragma mark - 添加绑定
+ (void)requestWithUrlForDeviceBinding:(id)obj andParam:(NSDictionary *)param
                               success:(void(^)(id model))success failure:(void(^)(NSString * error))failure {
    [[UtilToolsClss getUtilTools] addDoLoading];
    
    [NetRequestClss requestWithUrl:@"deviceList/deviceBinding.asp" requestWithParameters:param resultSuccess:^(id objs, int status, NSString *mag) {
        [[UtilToolsClss getUtilTools] removeDoLoading];

        NSString *tipsStr = @"";
        if (status != 1) {
             tipsStr = [ViewModelClass forceJumpViewStatus:status forController:obj];
            failure(tipsStr); return;
        }
        
         DeviceBindingModel* binding = [[DeviceBindingModel alloc] initWithDictionary:objs error:nil];
         DeviceBindingList *list = binding.DeviceBindingList[0];
        
        if (list.result != 1) {
            if (list.result == 0) {
                tipsStr = list.error;
            }else if (list.result == 2){
                tipsStr =  NSLocalizedString(@"EIP1", @"已绑定");
            }else if(list.result == 3) {
                tipsStr = NSLocalizedString(@"EIP2", @"空间不足");
            }else if (list.result == 4) {
                tipsStr = NSLocalizedString(@"EIP3", @"无法绑定");
            }else {
                tipsStr = NSLocalizedString(@"EIP4", @"绑定成功");
            }
             failure(tipsStr);
        }else {
            success(list.deviceNo);
        }
        
    } resultError:^(NSString *err) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        [MBProgressHUD showError:err];
    }];
}



@end
