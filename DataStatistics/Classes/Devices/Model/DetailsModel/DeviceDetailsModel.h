//
//  DeviceDetailsModel.h
//  DataStatistics
//
//  Created by Kang on 15/12/31.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "JSONModel.h"

@interface DeviceDetailsModel : NSObject


/** 公司名 */
@property (copy, nonatomic) NSString     *cname;
/** 小计流量 */
@property (assign, nonatomic)double     addup_flow;
/** 总计流量 */
@property (assign, nonatomic)double     total_flow;
/** 剩余流量 */
@property (assign, nonatomic)double     remain_flow;
/** 合同期 */
@property (assign, nonatomic)int        contract;
/** 3G卡号 */
@property (copy, nonatomic) NSString     *id_num;
/** 3卡ICCID */
@property (copy, nonatomic) NSString     *iccid;
/** 3G卡品牌 */
@property (copy, nonatomic )NSString     *brand;
/** 3G卡漫游方式 */
@property (copy, nonatomic) NSString     *method;
/** 3G卡开通时间 */
@property (copy, nonatomic) NSString     *active_date;
/** 余额 */
@property (assign, nonatomic)double     ic_money;
/** 单价 */
@property (assign, nonatomic)float      price;
/** 交付押金 */
@property (assign, nonatomic)double     deposit;
/** 预付流量费 */
@property (assign, nonatomic) double     payment_flow;
/** 最低使用流量 */
@property (assign, nonatomic) double     use_flow;
/** 签约日期 */
@property (copy, nonatomic) NSString    *begin;
/** 解约日期 */
@property (copy, nonatomic) NSString    *end;
/** 机器名称 */
@property (copy, nonatomic) NSString    *m_name;
/** 软件版本 */
@property (copy, nonatomic) NSString    *software_version;
/** 设备版本 */
@property (assign, nonatomic) double    version;
/** 未读日志数量 */
@property (copy, nonatomic) NSString    *unreadCount;
/**  */
@property (copy, nonatomic) NSString    *oil_model;
/** */
@property (copy, nonatomic) NSString    *factory_date;
/** */
@property (copy, nonatomic) NSString    *deviceCode;
@end
