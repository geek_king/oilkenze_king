//
//  CoustomerDetailsModel.h
//  DataStatistics
//
//  Created by Kang on 15/12/30.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "JSONModel.h"

@protocol CoustomerListModel <NSObject>
@end

@interface CoustomerListModel : JSONModel
/** 公司编号 */
@property (assign, nonatomic) NSString *c_no;
/** 公司名称 */
@property (retain, nonatomic) NSString *c_name;
/** 公司地址 - 中文 */
@property (retain, nonatomic) NSString *c_addr;
/** 公司地址 - 英文 */
@property (retain, nonatomic) NSString *c_addrEn;
/** 地区 - 中文*/
@property (retain, nonatomic) NSString *c_area;
/** 地区 - 英文*/
@property (retain, nonatomic) NSString *c_areaEn;
/** 用户 */
@property (retain, nonatomic) NSString *c_contact;
/** 热线电话 */
@property (copy, nonatomic) NSString *c_tel;
/** 移动电话 */
@property (copy, nonatomic) NSString *c_mobile;
/** 固定电话 */
@property (copy, nonatomic) NSString *c_fax;
/** 邮件 */
@property (copy, nonatomic) NSString *c_email;
/** 押金 */
@property (assign, nonatomic) double    deposit;
/** 过滤流量 */
@property (assign, nonatomic) double    payment_flow;
/** 使用流量 */
@property (assign, nonatomic) double    use_flow;
/** 时间 - 结束 */
@property (copy, nonatomic) NSString  *end;
/** 时间 - 开始 */
@property (copy, nonatomic) NSString  *begin;

@end


@interface CoustomerDetailsModel : JSONModel
@property (strong, nonatomic) NSMutableArray <CoustomerListModel,ConvertOnDemand> *CoustomerListModel;
@end
