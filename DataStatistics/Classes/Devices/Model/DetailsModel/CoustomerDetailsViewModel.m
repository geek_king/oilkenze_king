//
//  CoustomerDetailsViewModel.m
//  DataStatistics
//
//  Created by Kang on 15/12/30.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "CoustomerDetailsViewModel.h"
#import "CoustomerDetailsModel.h"

@implementation CoustomerDetailsViewModel

/*- (void)requestDataWithValueSuccess:(NSDictionary *)returnValue withViewController:(UIViewController *)vc judgeApiStrOrApiId:(id)api returnBlockVlue:(int)returnStatus {
    
    if (returnStatus != 1) {
        if (returnStatus == -2 || returnStatus == -1) {
            [super forceJumpViewController:vc];
        }
        self.apiblock(nil,vc,@"---",api); return;
    }
    
    if (returnStatus == 0) {
      //  [MBProgressHUD showError:NSLocalizedString(@"VMdata", nil)];
    }else {
    
    NSString  *msg = [NSString stringWithFormat:@"%lu",[[returnValue objectForKey:@"body"] count]];
    
    NSMutableArray *mutable = [[NSMutableArray alloc] initWithObjects:[returnValue objectForKey:@"body"], nil];
    NSDictionary *dic = @{@"DetailsList":mutable};
    CoustomerDetailsModel *detailsmodel = [[CoustomerDetailsModel alloc] initWithDictionary:dic error:nil];
    
        //self.receivevlueblock([detailsmodel.CoustomerListModel objectAtIndex:0]);
        self.apiblock([detailsmodel.CoustomerListModel objectAtIndex:0],vc,msg,api);
    }
}



/// 跳转 或传数据
- (void)coustomerDetailsWithListModel:(CoustomerListModel *)model WithViewController:(UIViewController *)viewController {
 
} */

#pragma mark - 客户详情信息
+ (void)requestWithUrlForCoustomerDetails:(id)obj andParam:(NSDictionary *)param
                                  success:(void(^)(CoustomerDetailsModel *model))success failure:(void(^)(NSString * error))failure {
    [[UtilToolsClss getUtilTools] addDoLoading];
    
    [NetRequestClss requestWithUrl:@"customerInfo/customerInfo.asp" requestWithParameters:param method:NetMethodGET returnSuccess:^(id objs, int status, NSString *mag) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        
        if (status != 1) {
           NSString *tilasStr = [ViewModelClass forceJumpViewStatus:status forController:obj];
            failure(tilasStr); return;
        }
        
        NSMutableArray *mutable = [[NSMutableArray alloc] initWithObjects:[objs objectForKey:@"body"], nil];
        NSDictionary *dic = @{@"DetailsList":mutable};
        CoustomerDetailsModel *detailsmodel = [[CoustomerDetailsModel alloc] initWithDictionary:dic error:nil];
        if (![detailsmodel.CoustomerListModel isEqual:[NSNull null]]) {
            success(detailsmodel);
            return;
        }
        failure(@"---");
        
    } returnError:^(NSString *err) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        [MBProgressHUD showError:err];
    }];
    
}

@end
