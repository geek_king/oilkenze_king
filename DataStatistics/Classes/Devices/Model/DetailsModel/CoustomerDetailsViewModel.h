//
//  CoustomerDetailsViewModel.h
//  DataStatistics
//
//  Created by Kang on 15/12/30.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "CoustomerDetailsModel.h"

@interface CoustomerDetailsViewModel : ViewModelClass

/** 跳转页面 */
//- (void)coustomerDetailsWithListModel:(CoustomerListModel *)model WithViewController:(UIViewController *)viewController;
//


/**
 *  客户详情API
 *
 */
+ (void)requestWithUrlForCoustomerDetails:(id)obj andParam:(NSDictionary *)param
                                  success:(void(^)(CoustomerDetailsModel *model))success failure:(void(^)(NSString * error))failure;


@end
