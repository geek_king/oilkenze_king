//
//  SearchCityViewModel.h
//  DataStatistics
//
//  Created by Kang on 15/12/18.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "SearchCityModel.h"

@interface SearchCityViewModel : ViewModelClass
+ (SearchCityViewModel *)getCityVM;

/**
 *  地区列表API
 *
 */
+ (void)requestWithUrlForSearchIn:(id)obj andParam:(NSDictionary *)param success:(void(^)(id model))success failure:(void(^)(NSString *error))failure;



@end
