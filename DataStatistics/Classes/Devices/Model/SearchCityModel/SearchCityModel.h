//
//  SearchCityModel.h
//  DataStatistics
//
//  Created by Kang on 15/12/19.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface myProvinces : NSObject
/** 省名称 */
@property (copy, nonatomic) NSString *name;
/** 城市组 */
@property (strong, nonatomic) NSMutableArray * citys;

@end

@interface myCountrys : NSObject
/** 国家名称 */
@property (copy, nonatomic) NSString *name;
/** 省组 */
@property (strong, nonatomic) NSMutableArray *provinces;

@end


@interface SearchCityModel : NSObject

/** 国家名称 */
@property (copy, nonatomic) NSString *name;
/** 省组 */
@property (strong, nonatomic) NSMutableArray *provinces;
@end