//
//  SearchCityViewModel.m
//  DataStatistics
//
//  Created by Kang on 15/12/18.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "SearchCityViewModel.h"
#import "MJExtension.h"

@interface SearchCityViewModel()
@end

@implementation SearchCityViewModel
static SearchCityViewModel *cityVM = nil;

+ (SearchCityViewModel *)getCityVM {
    if (!cityVM) {
        cityVM = [[super allocWithZone:NULL] init];
    }
    return cityVM;
}
+ (id)allocsearchCityZone:(NSZone *)zone {
    return [self getCityVM];
}
- (id)copysearchCityZone:(NSZone *)zone {
    return self;
}
- (id)init {
    if (cityVM) {
        return cityVM;
    }
    self = [super init];
    return self;
}

#pragma  mark -地区列表
+ (void)requestWithUrlForSearchIn:(id)obj andParam:(NSDictionary *)param success:(void(^)(id model))success failure:(void(^)(NSString *error))failure {
    [[UtilToolsClss getUtilTools] addDoLoading];
    
    [NetRequestClss requestWithUrl:@"deviceList/querAreas.asp" requestWithParameters:param method:NetMethodGET returnSuccess:^(id objs, int status, NSString *mag) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        
        if (status != 1) {
            NSString *tilasStr = [ViewModelClass forceJumpViewStatus:status forController:obj];
            failure(tilasStr); return;
        }
        
        
        if (objs != nil) {
            [SearchCityModel mj_setupObjectClassInArray:^NSDictionary *{
                return @{ @"provinces":@"myprovinces"};
            }];
        
            NSArray *searchAry = [SearchCityModel mj_objectArrayWithKeyValuesArray:[[objs objectForKey:@"body"] objectForKey:@"countrys"]];
            success(searchAry);
           
        }
        failure(@"---");
        
    } returnError:^(NSString *err) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        [MBProgressHUD showError:err];
    }];
    
    
}








@end
