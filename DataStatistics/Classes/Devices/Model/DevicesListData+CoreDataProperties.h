//
//  DevicesListData+CoreDataProperties.h
//  DataStatistics
//
//  Created by Kang on 16/3/17.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DevicesListData.h"

NS_ASSUME_NONNULL_BEGIN

@interface DevicesListData (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *begin;
@property (nullable, nonatomic, retain) NSString *c_area;
@property (nullable, nonatomic, retain) NSString *c_contact;
@property (nullable, nonatomic, retain) NSString *co_no;
@property (nullable, nonatomic, retain) NSNumber *contract;
@property (nullable, nonatomic, retain) NSString *customer;
@property (nullable, nonatomic, retain) NSString *deviceCode;
@property (nullable, nonatomic, retain) NSNumber *deviceID;
@property (nullable, nonatomic, retain) NSString *end;
@property (nullable, nonatomic, retain) NSNumber *ic_money;
@property (nullable, nonatomic, retain) NSString *last_upload;
@property (nullable, nonatomic, retain) NSString *latestUse;
@property (nullable, nonatomic, retain) NSNumber *price;
@property (nullable, nonatomic, retain) NSNumber *remain;
@property (nullable, nonatomic, retain) NSNumber *running_state;
@property (nullable, nonatomic, retain) NSNumber *state;
@property (nullable, nonatomic, retain) NSNumber *total_flow;
@property (nullable, nonatomic, retain) NSNumber *unreadCount;

@end

NS_ASSUME_NONNULL_END
