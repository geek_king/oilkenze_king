//
//  EquipmenCellViewModel.h
//  DataStatistics
//
//  Created by Kang on 15/12/9.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "EquipmenModel.h"
//#import "WMPageController.h" //分页
#import "RemoveBindingModel.h" //解绑
#import "SearchCityModel.h" // 城市列表
#import "SearchVC.h"

@interface EquipmenCellViewModel : ViewModelClass

@property (retain, nonatomic) EquipmenModel *list;
@property (retain, nonatomic) RemoveBindingModel *remBind;
@property (retain, nonatomic) SearchCityModel  *city;
@property (strong, nonatomic) NSString    *unreadCount;

+ (EquipmenCellViewModel *)getEquipmenVM;

- (void)equipmenListDetailWithListModel:(EListModel *)model adVlue:(id)vlue WithViewController: (SearchVC *)viewController;

@end
