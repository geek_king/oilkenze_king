//
//  JournalDetailsModel.m
//  DataStatistics
//
//  Created by YTYanK on 16/1/1.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "JournalDetailsModel.h"

@implementation JournalDetailsList
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end



@implementation JournalDetailsModel

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"journalDetails":@"JournalDetailsList"}];
    
}


@end
