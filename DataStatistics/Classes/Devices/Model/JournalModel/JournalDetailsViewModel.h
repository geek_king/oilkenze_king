//
//  JournalDetailsViewModel.h
//  DataStatistics
//
//  Created by YTYanK on 16/1/1.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "JournalDetailsModel.h"

@interface JournalDetailsViewModel : ViewModelClass
singleton_for_interface(JournalDetailsViewModel)

/**
 *  暴露给外面的数据模型--未赋值
 */
//@property (copy, nonatomic)JournalDetailsList *detailsList;

/**
 *  日志详情API
 *
 */

+ (void)requestWithUrlForJournalDetails:(id)obj andParam:(NSDictionary *)param
                                success:(void(^)(JournalDetailsModel *model))success failure:(void(^)(NSString * error))failure;


@end
