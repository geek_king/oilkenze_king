//
//  JournalModel.h
//  DataStatistics
//
//  Created by Kang on 15/12/30.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "JSONModel.h"

@protocol JouList <NSObject>
@end

@interface JouList : JSONModel

/** 日志ID */
@property (assign, nonatomic) NSInteger log_id;
/** 流水号 */
@property (copy, nonatomic) NSString    *work_id;
/** 操作账号 */
@property (copy, nonatomic) NSString    *user;
/** 接收年月日 */
@property (copy, nonatomic) NSString    *recv_ymd;
/** 接收时分 */
@property (copy, nonatomic) NSString    *recv_hm;
/** 未读已读标识 0：未读。1：已读。 */
@property (copy, nonatomic) NSString    *nou;
/** 油液类型 */
@property (copy, nonatomic) NSString    *oil_midel;
/** 机器名称 */
@property (copy, nonatomic) NSString    *m_name;
/** 平均压力 */
@property (assign, nonatomic) float     avg_pressure;
/** 平均流速 */
@property (assign, nonatomic) float     avg_flowspeed;
/** 流量 */
@property (assign, nonatomic) double    flow;
/** 剩余流量 */
@property (assign, nonatomic) double    remain_flow;
/** 总计流量 */
@property (assign, nonatomic) double    total_flow;
/** IC卡编号 */
@property (assign, nonatomic) int       ic_no;
/** 单价 */
@property (assign, nonatomic) float     Price;
/** 余额 */
@property (assign, nonatomic) double    ic_money;


@end


@interface JournalModel : JSONModel
@property (strong, nonatomic) NSMutableArray <JouList,ConvertOnDemand> *JouList;



@end
