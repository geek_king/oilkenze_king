//
//  JournalDetailsModel.h
//  DataStatistics
//
//  Created by YTYanK on 16/1/1.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "JSONModel.h"

@protocol JournalDetailsList <NSObject>

@end

@interface JournalDetailsList : JSONModel
/** 油液类型 */
@property (copy, nonatomic) NSString *oil_midel;
/** 剩余流量 */
@property (assign, nonatomic) double remain_flow;
/** 日志ID */
@property (copy, nonatomic) NSString *log_id;
/** 操作账号 */
@property (copy, nonatomic) NSString *user;
/** 过滤流量 */
@property (copy, nonatomic) NSString *flow;
/** 流水号 */
@property (copy, nonatomic) NSString *work_id;
/** 开始时间 */
@property (copy, nonatomic) NSString *work_begin;
/** 总流量 */
@property (assign, nonatomic) double total_flow;
/** 机器名称 */
@property (copy, nonatomic) NSString *m_name;
/** 用时，结束时间 - 开始时间 */
@property (assign, nonatomic) int    work_use;
/** 耗时，不包含暂停时候的用时 */
@property (assign, nonatomic) int    consume;
/** 最大压力 */
@property (assign, nonatomic) float  max_pressure;
/** 最大流速 */
@property (assign, nonatomic) float  max_flowspeed;
/** 平均压力 */
@property (assign, nonatomic) float  avg_pressure;
/** 平均流速 */
@property (assign, nonatomic) float  avg_flowspeed;
/** 单价 */
@property (assign, nonatomic) float  price;
/** 余额 */
@property (assign, nonatomic) double ic_money;
/** IC卡编号 */
@property (assign, nonatomic) int  ic_no;
/** 结束时间 */
@property (copy, nonatomic) NSString *work_end;



@end

@interface JournalDetailsModel : JSONModel

@property (strong, nonatomic) NSMutableArray <JournalDetailsList,ConvertOnDemand> *JournalDetailsList;


@end
