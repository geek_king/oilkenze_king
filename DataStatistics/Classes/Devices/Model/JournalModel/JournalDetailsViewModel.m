//
//  JournalDetailsViewModel.m
//  DataStatistics
//
//  Created by YTYanK on 16/1/1.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "JournalDetailsViewModel.h"


@implementation JournalDetailsViewModel
singleton_for_implementation(JournalDetailsViewModel)

/*
static JournalDetailsViewModel *jourDetaiVM = nil;

+ (JournalDetailsViewModel *)getJourDetaiVM {
    if (!jourDetaiVM) {
        jourDetaiVM = [[super allocWithZone:NULL]init];
    }
    return jourDetaiVM;
}

+ (id)allocjournalDataZone:(NSZone *)zone {
    return [self getJourDetaiVM];
}
- (id)copyjourDetaiDataZone:(NSZone *)zone {
    return self;
}
- (id)init {
    if(jourDetaiVM) {
        return jourDetaiVM;
    }
    self = [super init];
    return self;
}
*/


#pragma mark - 日志详情
+ (void)requestWithUrlForJournalDetails:(id)obj andParam:(NSDictionary *)param
                                success:(void(^)(JournalDetailsModel *model))success failure:(void(^)(NSString * error))failure {
      [[UtilToolsClss getUtilTools] addDoLoading];
    
      [NetRequestClss requestWithUrl:@"dailylogInfo/dailylogInfo.asp" requestWithParameters:param method:NetMethodGET returnSuccess:^(id objs, int status, NSString *mag) {
          [[UtilToolsClss getUtilTools] removeDoLoading];
          
          if (status != 1) {
              NSString *tilasStr = [ViewModelClass forceJumpViewStatus:status forController:obj];
              failure(tilasStr); return;
          }
          
          NSMutableArray *mutable = [[NSMutableArray alloc] initWithObjects:[objs objectForKey:@"body"], nil];
          NSDictionary *dic = @{@"journalDetails":mutable};
           JournalDetailsModel *jourDetail = [[JournalDetailsModel alloc] initWithDictionary:dic error:nil];
          NSLog(@"jourDetail---> %@",jourDetail);
          if (![jourDetail isEqual:[NSNull null]]) {
              success(jourDetail);
              return;
          }
          failure(@"---");
          
      } returnError:^(NSString *err) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        [MBProgressHUD showError:err];
      }];
    
}

@end
