//
//  JournalViewModel.m
//  DataStatistics
//
//  Created by Kang on 15/12/30.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "JournalViewModel.h"


@interface JournalViewModel()
@end

@implementation JournalViewModel
static JournalViewModel *journalVM = nil;

+ (JournalViewModel *)getJournalVM {
    if (!journalVM) {
        journalVM = [[super allocWithZone:NULL]init];
    }
    return journalVM;
}
+ (id)allocjournalDataZone:(NSZone *)zone {
    return [self getJournalVM];
}
- (id)copyjournalDataZone:(NSZone *)zone {
    return self;
}
- (id)init {
    if(journalVM) {
        return journalVM;
    }
    self = [super init];
    return self;
}

/*- (void)requestDataWithValueSuccess:(NSDictionary *)returnValue withViewController:(UIViewController *)vc judgeApiStrOrApiId:(id)api returnBlockVlue:(int)returnStatus {
    
    // 判断状态
    if (returnStatus != 1 ) {
        if(returnStatus == -2 || returnStatus == -1) {
            [super forceJumpViewController:vc];
        }
        self.apiblock([NSArray array],vc,[NSString stringWithFormat:@"%d",returnStatus],api); return;
    }
    
    
    if ([api isEqualToString:@"dailylogInfoList.asp"]) {
        JournalModel * journ = [[JournalModel alloc] initWithDictionary:returnValue error:nil];
        
        if ([journ.JouList isEqual:[NSNull null]]) {
            self.returnblock(@"---");
        }else {
          // self.returnblock(journ.journalList);
            self.apiblock(journ.JouList,vc,[NSString stringWithFormat:@"%d",returnStatus],api);
        }
    }else if ([api isEqualToString:@"updateReadList.asp"]) {
        self.returnblock(@"XX");
    }
} */


#pragma mrak - 删除日志
+ (void)requestWithUrlForChangePassword:(id)obj  andParam:(NSDictionary *)param success:(void(^)(JournalModel * model))success failure:(void(^)(NSString * error))failure{

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    [NetRequestClss requestWithUrl:@"dailylogInfo/falsedelete.asp?" requestWithParameters:param method:NetMethodPOST returnSuccess:^(id objs, int status, NSString *mag) {

        success(objs[@"body"][@"result"]);
        
    } returnError:^(NSString *err) {
        
        [[UtilToolsClss getUtilTools] removeDoLoading];
        [MBProgressHUD showError:err];
        
    }];

}


#pragma mrak - 日志列表
+ (void)requestWithUrlForJournal:(id)obj andParam:(NSDictionary *)param
                          success:(void(^)(JournalModel *model))success failure:(void(^)(NSString *error))failure {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
 
    [NetRequestClss requestWithUrl:@"dailylogInfo/dailylogInfoList.asp" requestWithParameters:param method:NetMethodGET returnSuccess:^(id objs, int status, NSString *mag) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
       

        if (status != 1) {
            NSString *tilasStr = [ViewModelClass forceJumpViewStatus:status forController:obj];
            failure(tilasStr); return;
        }
        
        JournalModel * journ = [[JournalModel alloc] initWithDictionary:objs error:nil];
        
        NSLog(@"journ---> %@",journ);
        if (![journ.JouList isEqual:[NSNull null]]) {
            success(journ);
            return;
        }
        failure(@"---");
        
    } returnError:^(NSString *err) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        //[MBProgressHUD showError:err];
        failure(@"---");
    }];
    
}

#pragma mrak - 跳转日志详情
+ (void)journalDetailWithListModel:(JournalModel *)model forViewController:(id)vc {
    [vc performSegueWithIdentifier:@"gotoLogInfoC" sender:vc];
}

#pragma mrak - 清除未读信息
+ (void)requestWithUrlForRemoveUnreadNum:(id)obj andParam:(NSDictionary *)param
                                 failure :(void(^)(NSString *mag))failure   {
    [NetRequestClss requestWithUrl:@"deviceList/updateReadList.asp" requestWithParameters:param method:NetMethodPOST returnSuccess:^(id objs, int status, NSString *mag) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (status != 1) {
            NSString *tilasStr = [ViewModelClass forceJumpViewStatus:status forController:obj];
            failure(tilasStr); return;
        }
        
        failure(mag);
    } returnError:^(NSString *err) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [MBProgressHUD showError:err];
    }];
}

@end
