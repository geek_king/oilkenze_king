//
//  JournalViewModel.h
//  DataStatistics
//
//  Created by Kang on 15/12/30.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "JournalModel.h"


@interface JournalViewModel : ViewModelClass
+ (JournalViewModel *)getJournalVM;

/**
 *  暴露给外面的数据模型-未赋值
 */
//@property (copy, nonatomic)JouList *jouList;

/**
 *   日志列表API
 */
+ (void)requestWithUrlForJournal:(id)obj andParam:(NSDictionary *)param
                         success:(void(^)(JournalModel *model))success failure:(void(^)(NSString *error))failure;

/**
 *  跳转页面
 */
+ (void)journalDetailWithListModel:(JournalModel *)model forViewController:(id)vc;

/**
 *  清除未读信息API
 */
+ (void)requestWithUrlForRemoveUnreadNum:(id)obj andParam:(NSDictionary *)param
                                failure :(void(^)(NSString *mag))failure;



/**
 *  删除日志列表
 */
+ (void)requestWithUrlForChangePassword:(id)obj  andParam:(NSDictionary *)param success:(void(^)(JournalModel * model))success failure:(void(^)(NSString * error))failure;


@end
