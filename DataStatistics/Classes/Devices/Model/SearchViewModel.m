//
//  SearchViewModel.m
//  DataStatistics
//
//  Created by Kang on 16/6/14.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "SearchViewModel.h"
#import "EquipmenModel.h"           // 设备模型
#import "SearchCityModel.h"         // 地区模型
#import "RemoveBindingModel.h" // 删除模型


#import "MapGPSViewController.h"
#import "EquipmenDetailsVC.h"

@interface SearchViewModel()

@end

@implementation SearchViewModel
singleton_for_implementation(SearchViewModel)

#pragma mrak - 设备列表
+ (void)requestWithUrlForSearchIn:(id)obj andParam:(NSDictionary *)param
                          success:(void(^)(EquipmenModel *model))success failure:(void(^)(NSString *error))failure {
    
    
    [NetRequestClss requestWithUrl:@"deviceList/deviceList.asp" requestWithParameters:param method:NetMethodGET returnSuccess:^(id objs, int status, NSString *mag) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (status != 1) {
            NSString *tilasStr = [ViewModelClass forceJumpViewStatus:status forController:obj];
            failure(tilasStr);
            return;
        }
        
        EquipmenModel *equipmen = [[EquipmenModel alloc] initWithDictionary:objs error:nil];
        NSLog(@"equipmen--->%@",equipmen);
        
        if (![equipmen.EListModel isEqual:[NSNull null]]) {
            success(equipmen);
        }
        }returnError:^(NSString *err) {
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [MBProgressHUD showError:err];
            failure(@"---");
        }];
}

     
     
#pragma mark - 删除绑定
+ (void)requestWithUrlForDeviceRemoveBinding:(id)obj andParam:(NSDictionary *)param success:(void(^)(RemoveBindingModel *model))success failure:(void(^)(NSString * error))failure {
    [[UtilToolsClss getUtilTools] addDoLoading];
    [NetRequestClss requestWithUrl:@"deviceList/deviceRemoveBinding.asp" requestWithParameters:param method:NetMethodPOST returnSuccess:^(id objs, int status, NSString *mag) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        
        
        if (status != 1) {
            NSString *tilasStr = [ViewModelClass forceJumpViewStatus:status forController:obj];
            failure(tilasStr); return;
        }
        
        RemoveBindingModel *removeBinding = [[RemoveBindingModel alloc] initWithDictionary:objs error:nil];
        
        RemoveBindingList * list = removeBinding.RemoveBindingList[0];
        int returnCode = list.result;
        if (returnCode == 0) {
            failure(NSLocalizedString(@"SVCDelete", nil));
        }
        else if (returnCode == 1) {
            success(removeBinding);
        }
        else {
            // 未绑定
            failure(NSLocalizedString(@"SVCbeen", nil));
        }
        
    } returnError:^(NSString *err) {
        [[UtilToolsClss getUtilTools] removeDoLoading];
        [MBProgressHUD showError:err];
    }];
}



#pragma mark - 跳转日志列表
+ (void)jumpListDetailWithListModel:(EListModel *)model forViewController:(id)vc {
    [vc performSegueWithIdentifier:@"jumpJournalList" sender:vc];
}


@end
