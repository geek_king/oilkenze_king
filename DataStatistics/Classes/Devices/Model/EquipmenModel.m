//
//  EquipmenModel.m
//  DataStatistics
//
//  Created by Kang on 15/12/9.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "EquipmenModel.h"

@implementation EListModel
+ (BOOL)propertyIsOptional:(NSString *)propertyName {
    return YES;
}

@end


@implementation EquipmenModel
singleton_for_implementation(EquipmenModel)

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"body.deviceList":@"EListModel"}];
}



@end
