//
//  spaceCommondityViewModel.h
//  DataStatistics
//
//  Created by Kang on 16/1/27.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "spaceCommondityModel.h"
#import "spaceOrderModel.h"

@interface spaceCommondityViewModel : ViewModelClass
@property (strong, nonatomic) spaceListModel *spaceCommondityList;
@property (strong, nonatomic) spaceCommondityModel *spaceCommondity;
@property (strong, nonatomic) spaceOrderModel    *spaceList;

+ (spaceCommondityViewModel *)getSpaceCommondityVM;
@end
