//
//  spaceOrderModel.h
//  DataStatistics
//
//  Created by Kang on 16/1/27.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface spaceOrderModel : NSObject

/** 订单号 */
@property (copy, nonatomic) NSString  *orderID;
/** 空间容量 */
@property (assign, nonatomic)int spaceCapacity;
/** 空间有效期 */
@property (assign, nonatomic)int validTime;
/** price */
@property (assign, nonatomic)float price;
/**  订单创建时间 */
@property (copy, nonatomic) NSString * createTime;
/** 订单过期时间 */
@property (copy, nonatomic) NSString *expireTime;
/** 支付类型 */
@property (assign, nonatomic) int payType;



@end
