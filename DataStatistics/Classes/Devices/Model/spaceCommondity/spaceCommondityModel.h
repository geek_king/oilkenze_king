//
//  spaceCommondityModel.h
//  DataStatistics
//
//  Created by Kang on 16/1/27.
//  Copyright © 2016年 YTYangK. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface spaceListModel :  NSObject
/** 商品ID */
@property (assign, nonatomic) int commondityID;
/** 空间容量 */
@property (assign, nonatomic) int spaceCapacity;
/** 空间有效期  单位：天 */
@property (assign, nonatomic) int validTime;
/// 原来价格
@property (assign, nonatomic) int realPrice;
/** 空间价格  单位：元，价格为0的时候，显示免费*/
@property (assign, nonatomic) float spacePrice;
@end


@interface spaceCommondityModel : NSObject
//@property (weak, nonatomic) id<spaceListModel,ConvertOnDemand>spaceListModel;
@property (strong, nonatomic) NSMutableArray *commonList;

/** 支付方式 订单支持的支付方式
 1：支付宝
 2：微支付
 3：银联
 注意：
 免费不与后面的方式同时出现。
 目前仅有免费的方式。*/
@property (strong, nonatomic) NSMutableArray *spaceType;
@end
