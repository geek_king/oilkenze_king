//
//  spaceCommondityViewModel.m
//  DataStatistics
//
//  Created by Kang on 16/1/27.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "spaceCommondityViewModel.h"

@implementation spaceCommondityViewModel
static spaceCommondityViewModel *spaComVM;
+ (spaceCommondityViewModel *)getSpaceCommondityVM {
    if (!spaComVM) {
        spaComVM = [[super allocWithZone:NULL] init];
    }
    return spaComVM;
}

+ (id)allocspaceCommondityZone:(NSZone *)zone {
    return [self getSpaceCommondityVM];
}

- (id)spaceCommondityZone:(NSZone *)zone {
    return self;
}
- (id)init {
    if (spaComVM) {
        return spaComVM;
    }
    self = [super init];
    return self;
}



- (void)obtainDataWithValueSuccess:(NSDictionary *)returnValue withViewController:(UIViewController *)vc judgeApiStrOrApiId:(id)api {
  
    if ([api isEqualToString:@"spaceCommondity.asp"]) {
        [spaceCommondityModel mj_setupObjectClassInArray:^NSDictionary *{
            return @{
                     @"commonList":@"spaceListModel",
                     };
        }];
        
        
        NSArray *ary = [spaceListModel mj_objectArrayWithKeyValuesArray:[[returnValue objectForKey:@"body"]objectForKey:@"commondityList"]];
        self.spaceCommondity.spaceType = [[returnValue objectForKey:@"body"] objectForKey:@"payType"];
        NSArray *aryType = [[returnValue objectForKey:@"body"] objectForKey:@"payType"];
        NSArray *ar = @[ary,aryType];
        
        if (ary != nil && aryType != nil) {
            self.returnblock(ar);
        }

    }
    
    if ([api isEqualToString:@"spaceOrder.asp"]) {
        spaceOrderModel *spaceOrder = [spaceOrderModel mj_objectWithKeyValues:[returnValue objectForKey:@"body"]];
        self.spaceList = spaceOrder;
        self.returnblock(self.spaceList);
    }
    
    
}


@end
