//
//  LoginModel.h
//  DataStatistics
//
//  Created by Kang on 15/12/25.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "JSONModel.h"

@interface LoginModel : NSObject
singleton_for_interface(LoginModel)

/**  账号 */
@property (copy, nonatomic) NSString *userCode;
/** 返回结果 */
@property (assign, nonatomic) NSNumber *loginResult;
/** 用户类型 */
@property (copy, nonatomic)  NSString *roleCode;
/** 编号 */
@property (retain, nonatomic) NSString *cNo;
/** 用户头像 */
@property (retain, nonatomic) NSString *headImage;
/** 用户类型名称 */
@property (copy, nonatomic) NSString *userName;

@end


