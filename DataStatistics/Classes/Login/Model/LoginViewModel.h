//
//  LoginViewModel.h
//  DataStatistics
//
//  Created by Kang on 16/6/13.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "LoginModel.h"

@class LoginModel;
@interface LoginViewModel : ViewModelClass

#pragma mark - 登陆请求
+ (void) requestWithUrlForLoginIn:(id)obj  andParam:(NSDictionary *)param success:(void(^)(LoginModel * model))success failure:(void(^)(NSString * error))failure;

#pragma mark - 注册验证
+ (void)requestWithUrlForRegisteredAccountSign:(NSString *)sign andParam:(NSDictionary *)param success:(void (^)(id model))success failure:(void (^)(NSString *err))failure;

#pragma mark - 校验手机短信
+ (void)requestWithUrlForVerification:(id)obj andParam:(NSDictionary *)param success:(void(^)(id model))success failure:(void(^)(NSString *err))failure;

#pragma  mark - 重新设置密码
+ (void)requestWithUrlForForgetPassword:(id)obj andParam:(NSDictionary *)param success:(void(^)(id model))success failure:(void(^)(NSString *err))failure;

#pragma mark - 登陆跳转
+ (void)loginWithViewController:(UIViewController *)viewController;



@end
