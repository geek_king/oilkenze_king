//
//  LoginVC.h
//  DataStatistics
//
//  Created by Kang on 15/12/24.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface LoginVC : UIViewController

//singleton_for_interface(LoginVC)

/** 标记 signStr = Registered 注册页面 ， signStr = Forget 忘记密码页面  - String*/
@property (strong, nonatomic) NSString *signStr;
/** 用户名输入框 */
@property (strong, nonatomic) IBOutlet UITextField *phoneNum;
/** 密码输入框 */
@property (strong, nonatomic) IBOutlet UITextField *password;
/** 注册账号 */
@property (strong, nonatomic) IBOutlet UIButton *reclsTeredBtn;
/** 忘记密码 */
@property (strong, nonatomic) IBOutlet UIButton *forcetPasswordBtn;
/** 背景图片 */
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
/** 登录按钮 */
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
/** 自动登录 */
@property (assign, nonatomic) BOOL isAutomaticLogin;

/// 登陆方法
- (IBAction)loginBtnCiake:(UIButton *)sender;

@end
