//
//  SubmitVC.h
//  DataStatistics
//
//  Created by Kang on 16/3/10.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField+LolitaText.h"

@interface SubmitVC : UIViewController
/** SubmitVC 接收 标识*/
@property (strong, nonatomic) NSString *SVCsignStr;
/** SubmitVC 接收 手机号码*/
@property (strong, nonatomic) NSString *SVC_phoneNumber;
/** SubmitVC 接收 手机区号*/
@property (strong, nonatomic) NSString *SVC_areaNam;
/** SubmitVC 接收 验证码 */
@property (strong, nonatomic) NSString *SVC_SMSCode;

/** 密码提示 */
@property (strong, nonatomic) IBOutlet UILabel *pasText;
/** 昵称提示 */
@property (strong, nonatomic) IBOutlet UILabel *nameText;
/** 密码输入框*/
@property (strong, nonatomic) IBOutlet UITextField *pasField;
@property (strong, nonatomic) IBOutlet UITextField *againPasField;
/** 昵称输入框 */
@property (strong, nonatomic) IBOutlet UITextField *nameField;
/** 确定按钮 */
@property (strong, nonatomic) IBOutlet UIButton *okBtn;
- (IBAction)okMethod:(UIButton *)sender;

@end
