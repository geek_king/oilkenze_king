//
//  RegisteredAccountVC.m
//  DataStatistics
//
//  Created by Kang on 16/1/4.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
//    dispatch_group_t gruop = dispatch_group_create();
//    // 创建队列 （参数一: 优先级 ，参数二： 未来参数）
//    dispatch_queue_t queue = dispatch_get_global_queue(QOS_CLASS_DEFAULT, 0);
//    dispatch_group_async(gruop, queue, ^{

//    });
//    dispatch_group_notify(gruop, dispatch_get_main_queue(), ^{
//    });

#import "RegisteredAccountVC.h"
#import "UIButton+countDown.h"
#import "Webview.h"
#import "YJLocalCountryData.h"
#import "isPhoneNumber.h"
#import <AddressBook/AddressBook.h> //通讯录
#import <SMS_SDK/Extend/SMSSDKCountryAndAreaCode.h>
#import <SMS_SDK/SMSSDK.h>
#import "SectionsViewController.h"
#import "UIView+RGSize.h"
#import <MOBFoundation/MOBFoundation.h>
#import <SMS_SDK/Extend/SMSSDK+ExtexdMethods.h>
#import "UITextField+LolitaText.h"
#import "LoginViewModel.h"


#define LABEL ((MLLinkLabel*)self.Continue)
@interface RegisteredAccountVC () <UITextFieldDelegate, UIAlertViewDelegate,UIGestureRecognizerDelegate,SectionsViewControllerDelegate> {
    // 默认View 的Frame
    CGRect frameDefault;
    // 记录是否点击
    BOOL isClick;
    //
    int second;
    //
    UIAlertController* alert;
    
    //  获取到请求的值
    int regisInt;
    
}
@property (strong, nonatomic) IBOutlet UIView* nextView;
@property (strong, nonatomic) UIButton *eliminateContent;
@property (strong, nonatomic) UIToolbar* topview;  // 容器
@property (strong, nonatomic) UIButton *baritem;   // 键盘上按钮
@property (retain, nonatomic) NSTimer* timer;
@property (assign, nonatomic, setter=isSuccessful:) BOOL whetherSuccess;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint* V_phoneNum;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint* V_netxtView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint* V_nextview_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint* V_viceGuide_top;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint* _H_next_Y;


@end

@implementation RegisteredAccountVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateUI];
    [self SMSLocalArea];
    [self.SMSCodeBtn setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)viewDidDisappear:(BOOL)animated
{
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.iphoneNum.delegate = self;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 设置内容
- (void)updateUI
{
    if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"568.0")) {
        _V_nextview_H.constant = 30;
        //__H_next_Y.constant -= 5;
        if (SCREEN_HEIGHT == 480) {
            _V_phoneNum.constant += 9;
        }
    }

    
    frameDefault = self.view.frame;
    //手势隐藏键盘
    UITapGestureRecognizer* tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    tapGestureRecognizer.delegate = self;
    tapGestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGestureRecognizer];

    [self.view addSubview:[self eliminateContent]];
  
    [self iphoneNum];
    self.SMSCodeBtn.userInteractionEnabled = NO;
    [self.SMSCodeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];

    if ([self.RAVCsignStr isEqualToString:@"Forget"]) {
        self.viceGuideLabel.hidden = YES;
        self.Continue.hidden = YES;
        [self updateForgetUI];
    }
    else if ([self.RAVCsignStr isEqualToString:@"Registered"]) {
        self.viceGuideLabel.height = NO;
        self.Continue.hidden = NO;
        [self updateRegisteredUI];
    }
}
// 更新 注册UI
- (void)updateRegisteredUI
{
    self.title = NSLocalizedString(@"RAVCtitle", nil);
    self.guideLabel.text = NSLocalizedString(@"RAVCguid", nil);
    self.viceGuideLabel.text = NSLocalizedString(@"RAVCviceGui", nil);

    if (!self.Continue.hidden) {
        // 段落样式
        NSMutableParagraphStyle* paragraph = [[NSMutableParagraphStyle alloc] init];
        //指定段落
        paragraph.firstLineHeadIndent = 2;
        //调整文字缩进像素
        paragraph.headIndent = 13;

        // 下划线 + 字体
        NSMutableAttributedString* attrStr = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"RAVCagreement", nil)];
         int  i = [UtilToolsClss judgeLocalLanguage];
        if (i == 1) {
            [attrStr addAttribute:NSLinkAttributeName value:@"http://07568919527.locoso.com" range:NSMakeRange(11, 19)];
            [attrStr addAttribute:NSLinkAttributeName value:@"http://07568919527.locoso.com" range:NSMakeRange((attrStr.length - 5), 4)];
        }else  {
            [attrStr addAttribute:NSLinkAttributeName value:@"http://07568919527.locoso.com" range:NSMakeRange(30, 19)];
            [attrStr addAttribute:NSLinkAttributeName value:@"http://07568919527.locoso.com" range:NSMakeRange((attrStr.length - 15), 14)];
        }
        
        self.Continue.attributedText = attrStr;
        self.Continue.font = [UIFont systemFontOfSize:12.f];
        self.Continue.textAlignment = NSTextAlignmentLeft;
        self.Continue.textInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        self.Continue.lineHeightMultiple = 0.9f;
        self.Continue.beforeAddLinkBlock = nil;
        self.Continue.dataDetectorTypes = MLDataDetectorTypeAll;
        self.Continue.allowLineBreakInsideLinks = YES;
        self.Continue.linkTextAttributes = nil;
        self.Continue.activeLinkTextAttributes = nil;
        __weak typeof(self) weakSelf = self;
        [self.Continue setDidClickLinkBlock:^(MLLink* link, NSString* linkText, MLLinkLabel* label) {
            NSString* tips = [NSString stringWithFormat:@"Click\nlinkType:%ld\nlinkText:%@\nlinkValue:%@", (unsigned long)link.linkType, linkText, link.linkValue];
            NSLog(@"----->%@", tips);
            if ([linkText isEqualToString:@"Privacy Policy"]) {
                [weakSelf gotoWebView:linkText];
            }
            else {
                [weakSelf gotoWebView:linkText];
            }
        }];

        // 小点
        UIView* vv = [[UIView alloc] initWithFrame:CGRectMake(-5, 8, 6, 6)];
        vv.backgroundColor = RGB(14, 118, 199, 1);
        vv.layer.cornerRadius = 4.0f;
        [self.Continue addSubview:vv];
    }
}
// 更新 忘记密码UI
- (void)updateForgetUI
{
    self.title = NSLocalizedString(@"RAVCForTitle", nil);
    self.guideLabel.text = NSLocalizedString(@"RAVForCguid", nil);
    self.guideLabel.font = [UIFont systemFontOfSize:13.f];
    self.V_viceGuide_top.constant = 16;
}

// 设置地区缓存
- (void)SMSLocalArea {
    // 设置本地区号
    [self setTheLocalAreaCode];
    // 保存记录
    NSString* saveTimeString = [[NSUserDefaults standardUserDefaults] objectForKey:@"saveDate"];
    NSDateComponents* dateComponents = nil;
    if (saveTimeString.length != 0) {
        dateComponents = [YJLocalCountryData compareTwoDays:saveTimeString];
    }
    //day = 0 ,代表今天，day = 1  代表昨天  day >= 1 表示至少过了一天  saveTimeString.length == 0表示从未进行过缓存
    if (dateComponents.day >= 1 || saveTimeString.length == 0) {
        __weak RegisteredAccountVC* regViewController = self;
        
        [SMSSDK getCountryZone:^(NSError* error, NSArray* zonesArray) {
            if (!error) {
                NSLog(@"----->get the area code sucessfully");
                //区号数据
                
                regViewController.areaArray = [NSMutableArray arrayWithArray:zonesArray];
                [[MOBFDataService sharedInstance] setCacheData:regViewController.areaArray forKey:@"countryCodeArray" domain:nil];
                
                //设置缓存时间
                NSDate* saveDate = [NSDate date];
                [[NSUserDefaults standardUserDefaults] setObject:[MOBFDate stringByDate:saveDate withFormat:@"yyyy-MM-dd"] forKey:@"saveDate"];
                
                NSLog(@"_areaArray_%@", regViewController.areaArray);
            }
            else {
                NSLog(@"failed to get the area code _%@______error_%@", [error.userInfo objectForKey:@"getZone"], error);
            }
        }];
    }
    else {
        _areaArray = [[MOBFDataService sharedInstance] cacheDataForKey:@"countryCodeArray" domain:nil];
    }

}

// 设置本地区号
- (void)setTheLocalAreaCode
{
    NSLocale* locale = [NSLocale currentLocale];

    NSDictionary* dictCodes = [NSDictionary dictionaryWithObjectsAndKeys:@"972", @"IL",
                                            @"93", @"AF", @"355", @"AL", @"213", @"DZ", @"1", @"AS",
                                            @"376", @"AD", @"244", @"AO", @"1", @"AI", @"1", @"AG",
                                            @"54", @"AR", @"374", @"AM", @"297", @"AW", @"61", @"AU",
                                            @"43", @"AT", @"994", @"AZ", @"1", @"BS", @"973", @"BH",
                                            @"880", @"BD", @"1", @"BB", @"375", @"BY", @"32", @"BE",
                                            @"501", @"BZ", @"229", @"BJ", @"1", @"BM", @"975", @"BT",
                                            @"387", @"BA", @"267", @"BW", @"55", @"BR", @"246", @"IO",
                                            @"359", @"BG", @"226", @"BF", @"257", @"BI", @"855", @"KH",
                                            @"237", @"CM", @"1", @"CA", @"238", @"CV", @"345", @"KY",
                                            @"236", @"CF", @"235", @"TD", @"56", @"CL", @"86", @"CN",
                                            @"61", @"CX", @"57", @"CO", @"269", @"KM", @"242", @"CG",
                                            @"682", @"CK", @"506", @"CR", @"385", @"HR", @"53", @"CU",
                                            @"537", @"CY", @"420", @"CZ", @"45", @"DK", @"253", @"DJ",
                                            @"1", @"DM", @"1", @"DO", @"593", @"EC", @"20", @"EG",
                                            @"503", @"SV", @"240", @"GQ", @"291", @"ER", @"372", @"EE",
                                            @"251", @"ET", @"298", @"FO", @"679", @"FJ", @"358", @"FI",
                                            @"33", @"FR", @"594", @"GF", @"689", @"PF", @"241", @"GA",
                                            @"220", @"GM", @"995", @"GE", @"49", @"DE", @"233", @"GH",
                                            @"350", @"GI", @"30", @"GR", @"299", @"GL", @"1", @"GD",
                                            @"590", @"GP", @"1", @"GU", @"502", @"GT", @"224", @"GN",
                                            @"245", @"GW", @"595", @"GY", @"509", @"HT", @"504", @"HN",
                                            @"36", @"HU", @"354", @"IS", @"91", @"IN", @"62", @"ID",
                                            @"964", @"IQ", @"353", @"IE", @"972", @"IL", @"39", @"IT",
                                            @"1", @"JM", @"81", @"JP", @"962", @"JO", @"77", @"KZ",
                                            @"254", @"KE", @"686", @"KI", @"965", @"KW", @"996", @"KG",
                                            @"371", @"LV", @"961", @"LB", @"266", @"LS", @"231", @"LR",
                                            @"423", @"LI", @"370", @"LT", @"352", @"LU", @"261", @"MG",
                                            @"265", @"MW", @"60", @"MY", @"960", @"MV", @"223", @"ML",
                                            @"356", @"MT", @"692", @"MH", @"596", @"MQ", @"222", @"MR",
                                            @"230", @"MU", @"262", @"YT", @"52", @"MX", @"377", @"MC",
                                            @"976", @"MN", @"382", @"ME", @"1", @"MS", @"212", @"MA",
                                            @"95", @"MM", @"264", @"NA", @"674", @"NR", @"977", @"NP",
                                            @"31", @"NL", @"599", @"AN", @"687", @"NC", @"64", @"NZ",
                                            @"505", @"NI", @"227", @"NE", @"234", @"NG", @"683", @"NU",
                                            @"672", @"NF", @"1", @"MP", @"47", @"NO", @"968", @"OM",
                                            @"92", @"PK", @"680", @"PW", @"507", @"PA", @"675", @"PG",
                                            @"595", @"PY", @"51", @"PE", @"63", @"PH", @"48", @"PL",
                                            @"351", @"PT", @"1", @"PR", @"974", @"QA", @"40", @"RO",
                                            @"250", @"RW", @"685", @"WS", @"378", @"SM", @"966", @"SA",
                                            @"221", @"SN", @"381", @"RS", @"248", @"SC", @"232", @"SL",
                                            @"65", @"SG", @"421", @"SK", @"386", @"SI", @"677", @"SB",
                                            @"27", @"ZA", @"500", @"GS", @"34", @"ES", @"94", @"LK",
                                            @"249", @"SD", @"597", @"SR", @"268", @"SZ", @"46", @"SE",
                                            @"41", @"CH", @"992", @"TJ", @"66", @"TH", @"228", @"TG",
                                            @"690", @"TK", @"676", @"TO", @"1", @"TT", @"216", @"TN",
                                            @"90", @"TR", @"993", @"TM", @"1", @"TC", @"688", @"TV",
                                            @"256", @"UG", @"380", @"UA", @"971", @"AE", @"44", @"GB",
                                            @"1", @"US", @"598", @"UY", @"998", @"UZ", @"678", @"VU",
                                            @"681", @"WF", @"967", @"YE", @"260", @"ZM", @"263", @"ZW",
                                            @"591", @"BO", @"673", @"BN", @"61", @"CC", @"243", @"CD",
                                            @"225", @"CI", @"500", @"FK", @"44", @"GG", @"379", @"VA",
                                            @"852", @"HK", @"98", @"IR", @"44", @"IM", @"44", @"JE",
                                            @"850", @"KP", @"82", @"KR", @"856", @"LA", @"218", @"LY",
                                            @"853", @"MO", @"389", @"MK", @"691", @"FM", @"373", @"MD",
                                            @"258", @"MZ", @"970", @"PS", @"872", @"PN", @"262", @"RE",
                                            @"7", @"RU", @"590", @"BL", @"290", @"SH", @"1", @"KN",
                                            @"1", @"LC", @"590", @"MF", @"508", @"PM", @"1", @"VC",
                                            @"239", @"ST", @"252", @"SO", @"47", @"SJ", @"963", @"SY",
                                            @"886", @"TW", @"255", @"TZ", @"670", @"TL", @"58", @"VE",
                                            @"84", @"VN", @"1", @"VG", @"1", @"VI", nil];

    NSString* tt = [locale objectForKey:NSLocaleCountryCode];
    NSString* defaultCode = [dictCodes objectForKey:tt];

    NSString* defaultCountryName = [locale displayNameForKey:NSLocaleCountryCode value:tt];
    _areaCode.text = [NSString stringWithFormat:@"+%@", defaultCode];
    _areaName.text = defaultCountryName;
}

/** 跳转协议 */
- (void)gotoWebView:(NSString*)keyStr
{
    // 下一页返回的字样
    UIBarButtonItem* backIetm = [[UIBarButtonItem alloc] init];
    backIetm.title = NSLocalizedString(@"back", nil);
    self.navigationItem.backBarButtonItem = backIetm;
    
    Webview* web = [[Webview alloc] init];
    web.title = keyStr;
    web.hidesBottomBarWhenPushed = YES;
    web.identificationStr = keyStr;
    [self.navigationController pushViewController:web animated:YES];
}
// 限制长度
- (void)textLengthMethod:(UITextField*)textField
{

    if (textField.text.length > 0) {
        self.SMSCodeBtn.userInteractionEnabled = YES;
        [self.SMSCodeBtn setTitleColor:RGB(14, 118, 199, 1) forState:UIControlStateNormal];
        self.baritem.userInteractionEnabled = YES;
        [self.baritem setTitleColor:RGB(14, 118, 199, 1) forState:UIControlStateNormal];
    }
    else {
        self.SMSCodeBtn.userInteractionEnabled = NO;
        [self.SMSCodeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        self.baritem.userInteractionEnabled = NO;
        [self.baritem setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }

    if (textField == self.iphoneNum) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }
}

/// 监控键盘 - 显示
- (void)LTkeyboardWillShow:(NSNotification*) not
{
    if (!isClick) {
       isClick = YES;
        [self.iphoneNum becomeFirstResponder];
    }
    if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"568.0")) {
        self.topview.alpha = 0.0;
        CGRect newFrome = self.view.frame;
        if (SCREEN_HEIGHT == 480) {
            newFrome.origin.y = -1 * 36;
        }
        else {
            newFrome.origin.y = 40;
        }
        
        [UIView animateWithDuration:0.24 animations:^{
            self.topview.alpha = 1.0;
            [self.view setFrame:newFrome];
        }
                         completion:^(BOOL finished){
                             
                         }];
    }

    
}

/// 监控键盘 - 隐藏
- (void)LTkeyboardWillHide:(NSNotification*) not
{
    
//    _nextView.alpha = 0.0;
//    [UIView animateWithDuration:0.14 animations:^{
//        _nextView.alpha = 1.0;
//    }
//        completion:^(BOOL finished){
//
//        }];
}

// 键盘－ 按钮
- (void)setkeyboardRingBtn
{
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if (textField == _iphoneNum) {
        // next
    }
    return YES;
}
// 禁止输入框长按复制\拷贝功能
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{

    if (action == @selector(paste:))
        return NO;

    if (action == @selector(copy:))
        return NO;

    if (action == @selector(cut:))
        return NO;

    return [super canPerformAction:action withSender:sender];
}

// 手势方法键盘隐藏
- (void)keyboardHide:(UIGestureRecognizer*)send
{
    NSLog(@"%@",send);
    if (isClick == YES) {
        isClick = NO;
          [self resignRespondersForText];
    }
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIButton class]]) {
        return NO;
    }
    
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UIButton"]) {
        return NO;
    }
    return YES;
}

- (IBAction)clickSMSPinCodeBtn:(id)sender
{
     [self resignRespondersForText];
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction* okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"sure", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction* _Nonnull action) {
        
        if (regisInt == 1) {
            if ([self.areaCode.text isEqualToString:@"+86"]) {
                if ([isPhoneNumber isMobileNumber:_iphoneNum.text] == NO) {
                   // [self resignRespondersForText];
                      [MBProgressHUD yty_showErrorWithTitle:nil detailsText:NSLocalizedString(@"RAVCP3", nil) toView:nil];
                }
                else {
                    [self SMSSDK];
                }
            }
            else {
                [self SMSSDK];
            }
        }
        else {
            if (regisInt == 2) {
                if ([_RAVCsignStr isEqualToString:@"Forget"]) {
                    [self SMSSDK];
                }
            }
        }

    }];

    NSString* str2 = [self.areaCode.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
    NSDictionary *dic = @{@"loginName":self.iphoneNum.text,@"zone":str2};
    [LoginViewModel requestWithUrlForRegisteredAccountSign:self.RAVCsignStr andParam:dic success:^(id model) {
        NSString* str = [NSString stringWithFormat:@"%@:%@ %@", NSLocalizedString(@"RAVCwillsend", nil), self.areaCode.text, self.iphoneNum.text];
         [self resignRespondersForText];
        alert = [UtilToolsClss addViewController:self withTitleStr:NSLocalizedString(@"RAVCsurephonenumber", nil) withMessage:str withAction:cancelAction withOKAction:okAction withStyle:UIAlertControllerStyleAlert];
        regisInt = [model intValue];
    } failure:^(NSString *err) {
        [MBProgressHUD yty_showErrorWithTitle:nil detailsText:err toView:nil];
    }];
}
- (void)SMSSDK
{
        NSString* str2 = [self.areaCode.text stringByReplacingOccurrencesOfString:@"+" withString:@""];
        [SMSSDK getVerificationCodeByMethod:SMSGetCodeMethodSMS phoneNumber:self.iphoneNum.text zone:str2 customIdentifier:nil result:^(NSError *error) {
    
            if (!error) {
                NSLog(@"获取验证码成功");
                // 成功获取
                if ([self.RAVCsignStr isEqualToString:@"Forget"]) {
                    UIBarButtonItem* backIetm = [[UIBarButtonItem alloc] init];
                    backIetm.title = NSLocalizedString(@"back", nil);
                    self.navigationItem.backBarButtonItem = backIetm;
                }

                [self performSegueWithIdentifier:@"RegisteredVerification" sender:self];
                
            }else {
               NSString *messageStr = [NSString stringWithFormat:@"%zidescription",error.code];
                [MBProgressHUD yty_showErrorWithTitle:nil detailsText:NSLocalizedString(messageStr, nil) toView:nil];
            }
      }];
}

// 电话验证
- (BOOL)checkPhoneNumber:(NSString*)phoneNumber
{
    NSError* error = NULL;
    NSDataDetector* detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];

    NSRange inputRange = NSMakeRange(0, [phoneNumber length]);
    NSArray* matches = [detector matchesInString:phoneNumber options:0 range:inputRange];

    BOOL verified = NO;

    if ([matches count] != 0) {
        // found match but we need to check if it matched the whole string
        NSTextCheckingResult* result = (NSTextCheckingResult*)[matches objectAtIndex:0];

        if ([result resultType] == NSTextCheckingTypePhoneNumber && result.range.location == inputRange.location && result.range.length == inputRange.length) {
            // it matched the whole string
            verified = YES;
        }
        else {
            // it only matched partial string
            verified = NO;
            ;
        }
    }
    return verified;
}

//传值
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSString* str2 = [self.areaCode.text stringByReplacingOccurrencesOfString:@"+" withString:@""];

    if ([segue.identifier compare:@"RegisteredVerification"] == NO) {

        id vc = segue.destinationViewController;
        [vc setValue:_RAVCsignStr forKey:@"VVCsignStr"];
        [vc setValue:_iphoneNum.text forKey:@"VVC_phoneNumber"];
        [vc setValue:str2 forKey:@"VVC_areaNam"];
        [vc setValue:_window forKey:@"VVC_window"];
    }
}

#pragma mark - resign responders
- (void)resignRespondersForText
{
        if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"568.0")) {
            POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewFrame];
            anim.toValue  =  [NSValue valueWithCGRect:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height)];
            anim.beginTime = CACurrentMediaTime() +0.1;
            [self.view pop_addAnimation:anim forKey:@"viewAnim"];
            
        }
     [_iphoneNum resignFirstResponder];
}


// 选择国家区号
- (IBAction)clickArea:(UIButton*)sender
{
    [self resignRespondersForText];
    
    SectionsViewController* country2 = [[SectionsViewController alloc] init];
    country2.delegate = self;

    //读取本地countryCode
    if (_areaArray.count == 0) {
        NSMutableArray* dataArray = [YJLocalCountryData localCountryDataArray];

        _areaArray = dataArray;
    }
    [country2 setareaArray:_areaArray];

    UIBarButtonItem* backIetm = [[UIBarButtonItem alloc] init];
    backIetm.title = NSLocalizedString(@"back", nil);
    self.navigationItem.backBarButtonItem = backIetm;
   // dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.14 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController pushViewController:country2 animated:YES];
   // });
   
}
#pragma mark - SecondViewControllerDelegate的方法
- (void)setSecondData:(SMSSDKCountryAndAreaCode*)data
{
    NSLog(@"the area data：%@,%@", data.areaCode, data.countryName);
    self.areaName.text = [NSString stringWithFormat:@"%@", data.countryName];
    self.areaCode.text = [NSString stringWithFormat:@"+%@", data.areaCode];
}


- (UIToolbar *)topview {
    if (!_topview) {
        _topview = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
        _topview.backgroundColor = RGB(28, 28, 28, 1);
        [_topview setBarStyle:UIBarStyleBlackOpaque];
        [_topview addSubview:self.baritem];
    }
    return _topview;
}

- (UIButton *)baritem {
    if (!_baritem) {
        _baritem = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 38 - 20, 0, 50, 30)];
        [_baritem setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
        _baritem.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [_baritem addTarget:self action:@selector(clickSMSPinCodeBtn:) forControlEvents:UIControlEventTouchDown];
        _baritem.userInteractionEnabled = NO;
        [_baritem setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    return _baritem;
}

- (UITextField *)iphoneNum {
    if (!_iphoneNum) {
        
    }
    [_iphoneNum addObserverForWillKeyBoard:self];
    [_iphoneNum addYTYanKTextFielStyle1:_iphoneNum withString:NSLocalizedString(@"RAVCiphone", nil)];
    [_iphoneNum addTarget:self action:@selector(textLengthMethod:) forControlEvents:UIControlEventEditingChanged];
    [_iphoneNum setInputAccessoryView:self.topview];
    return _iphoneNum;
}

@end
