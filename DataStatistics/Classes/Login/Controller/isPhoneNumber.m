//
//  isPhoneNumber.m
//  DataStatistics
//
//  Created by Kang on 16/1/5.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "isPhoneNumber.h"
#import <CommonCrypto/CommonDigest.h>

@implementation isPhoneNumber

 /*手机号码验证 MODIFIED BY HELENSONG*/
+ (BOOL)isMobileNumber:(NSString *)mobileNum
{
    
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10 * 中国移动：China Mobile
     11 * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12 */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15 * 中国联通：China Unicom
     16 * 130,131,132,152,155,156,185,186
     17 */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20 * 中国电信：China Telecom
     21 * 133,1349,153,180,189
     22 */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25 * 大陆地区固话及小灵通
     26 * 区号：010,020,021,022,023,024,025,027,028,029
     27 * 号码：七位或八位
     28 */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma 正则匹配用户姓名,20位的中文或英文
+ (BOOL)checkUserName : (NSString *) userName
{
    NSString *pattern = @"^[a-zA-Z一-龥]{1,20}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:userName];
    return isMatch;
    
}

#pragma 正则匹配用户密码6-18位数字和字母组合
+ (BOOL)checkPassword:(NSString *) password
{
    NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{6,18}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return isMatch;
    
}
+ (BOOL)checkPasswordAndNum:(NSString *) password {
    NSString *patern = @"^[a-zA-Z0-9_]{6,12}$";//\w
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",patern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return isMatch;
}


// MD5 加密
-(NSString *)getMD5:(NSString *)str{
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, strlen(cStr), result); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}
+(NSString *)getMD5:(NSString *)str{
    return [self getMD5:str];
}


-(void)showAlert:(NSString *)str{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"提示" message:str delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

+(void)showAlert:(NSString *)str{
    [self showAlert:str];
}

#pragma 正则匹配手机号
-(BOOL) validateMobile:(NSString *)mobile
{
    //手机号以13， 15，18开头，八个 \d 数字字符
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}

+(BOOL)validateMobile:(NSString *)mobile
{
    return [self validateMobile:mobile];
}

//密码
-(BOOL) validatePassword:(NSString *)passWord
{
    NSString *passWordRegex = @"^[a-zA-Z0-9]{6,18}+$";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passWordRegex];
    return [passWordPredicate evaluateWithObject:passWord];
}
+(BOOL) validatePassword:(NSString *)passWord{
    return [self validatePassword:passWord];
}


//身份证号
- (BOOL) validateIdentityCard: (NSString *)identityCard
{
    BOOL flag;
    if (identityCard.length <= 0) {
        flag = NO;
        return flag;
    }
    NSString *regex2 = @"^(\\d{14}|\\d{17})(\\d|[xX])$";
    NSPredicate *identityCardPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex2];
    return [identityCardPredicate evaluateWithObject:identityCard];
}
+(BOOL) validateIdentityCard: (NSString *)identityCard{
    return [self validateIdentityCard:identityCard];
}

//地址只能输入汉字和数字
-(BOOL) validateAddress:(NSString *)address
{
    NSString *addressRegex = @"[u4e00-\u9fa5][0-9\u4e00-\u9fa5]+";
    NSPredicate *addressPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",addressRegex];
    return [addressPredicate evaluateWithObject:address];
}
+(BOOL) validateAddress:(NSString *)address{
    return [self validatePassword:address];
}

//固定电话
-(BOOL) validateFixnum:(NSString *)fixnum
{
    NSString *fixnumRegex = @"^((d{3,4})|d{3,4}-)?d{7,8}$";
    NSPredicate *fixnumPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",fixnumRegex];
    return [fixnumPredicate evaluateWithObject:fixnum];
}
+(BOOL) validateFixnum:(NSString *)fixnum{
    return [self validateFixnum:fixnum];
}

//QQ
-(BOOL) validateQQ:(NSString *)qq
{
    NSString *qqRegex = @"[1-9][0-9]{4,}";
    NSPredicate *qqPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",qqRegex];
    return [qqPredicate evaluateWithObject:qq];
}
+(BOOL) validateQQ:(NSString *)qq{
    return [self validateQQ:qq];
}

//Email
-(BOOL) validateEmail:(NSString *)email
{
    NSString *emailRegex = @"w+([-+.]w+)*@w+([-.]w+)*.w+([-.]w+)*";
    NSPredicate *emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex];
    return [emailPredicate evaluateWithObject:email];
}
+(BOOL) validateEmail:(NSString *)email{
    return [self validateEmail:email];
}

//jiage
-(BOOL) validatePrice:(NSString *)price
{
    NSString *priceRegex = @"^([1-9][0-9]*)?[0-9](\.[0-9]{1,2})?$";
    NSPredicate *pricePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",priceRegex];
    return [pricePredicate evaluateWithObject:price];
}

+(BOOL) validatePrice:(NSString *)price
{
    return [self validatePrice:price];
}

-(BOOL) validateNumber:(NSString *)number
{
    NSString *numberRegex = @"^[1-9][0-9]*$";
    NSPredicate *numberPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",numberRegex];
    return [numberPredicate evaluateWithObject:number];
}

+(BOOL) validateNumber:(NSString *)number
{
    return [self validateNumber:number];
}

+(BOOL) illegalCharacter:(NSString *)string
{
    NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_"];
    s = [s invertedSet];
    NSRange r = [string rangeOfCharacterFromSet:s];
    if (r.location !=NSNotFound) { NSLog(@"the string contains illegal characters");}
    return r.location !=NSNotFound;
}

@end
