//
//  ForgetPasswordVC.m
//  DataStatistics
//
//  Created by Kang on 16/1/4.
//  Copyright © 2016年 YTYangK. All rights reserved.
// 实验git
#import "ForgetPasswordVC.h"
#import "isPhoneNumber.h"
#import "LoginViewModel.h"

#define GeneralColor RGB(14, 118, 199, 1)

@interface ForgetPasswordVC () <UITextFieldDelegate, UIAlertViewDelegate> {
    UIToolbar* FPVCtoolBar;
    UIButton* FPVCbut;
    CGRect frameDefault;
    int second;
    BOOL isFPVCClick;
}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint* V_iphone_H;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint* V_SendView_H;

@end

@implementation ForgetPasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateUI];
    if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"568.0")) {
        _V_SendView_H.constant = 30;
        if (SCREEN_HEIGHT == 480) {
            _V_iphone_H.constant += 9;
        }
    }
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _newpassword.delegate = self;
    _originalPassword.delegate = self;
    [_newpassword addTarget:self action:@selector(textLengthMethod:) forControlEvents:UIControlEventEditingChanged];
    [_originalPassword addTarget:self action:@selector(textLengthMethod:) forControlEvents:UIControlEventEditingChanged];
}
- (void)viewDidDisappear:(BOOL)animated {}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 设置内容
- (void)updateUI
{
    self.title = NSLocalizedString(@"FPVCtitle", nil);
    self.FPVCguideLabel.text = NSLocalizedString(@"FPVCguide", nil);
    frameDefault = self.view.frame;
   // _revealPasBtn.hidden = YES;
   // [_revealPasBtn setTitleColor:GeneralColor forState:UIControlStateNormal];
  //  _revealPasBtn.titleLabel.font = [UIFont systemFontOfSize:14.f];

    UITapGestureRecognizer* tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    tapGestureRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGestureRecognizer];
    [self addkeyboardRingBtn];

    _iphoneNum.textColor = [UIColor whiteColor];
    _iphoneNum.userInteractionEnabled = NO;
    [_iphoneNum addYTYanKTextFielStyle1:_iphoneNum withString:[NSString stringWithFormat:@"+%@ %@", _FPVC_areaNam, _FPVC_phoneNumber]];

    _newpassword.secureTextEntry = YES;
    [_newpassword addYTYanKTextFielStyle1:_newpassword withString:NSLocalizedString(@"FPVCorigPas", nil)];

    _originalPassword.secureTextEntry = YES;
    [_originalPassword addYTYanKTextFielStyle1:_originalPassword withString:NSLocalizedString(@"FPVCpas", nil)];

    _forgetPasswordBtn.userInteractionEnabled = NO;
    [_forgetPasswordBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [_forgetPasswordBtn setTitle:NSLocalizedString(@"FPVCforgetBtn", nil) forState:UIControlStateNormal];
}
// 键盘－ 按钮
- (void)addkeyboardRingBtn
{
    FPVCtoolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    FPVCtoolBar.backgroundColor = RGB(28, 28, 28, 1);
    [FPVCtoolBar setBarStyle:UIBarStyleBlack];

    FPVCbut = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 32 - 20, 1, 34, 30)];
    [FPVCbut setTitle:NSLocalizedString(@"FPVCforgetBtn", nil) forState:UIControlStateNormal];
    FPVCbut.titleLabel.font = [UIFont systemFontOfSize:14.f];
    [FPVCbut addTarget:self action:@selector(clickForgetPasswordBtn:) forControlEvents:UIControlEventTouchDown];
    FPVCbut.userInteractionEnabled = NO;
    [FPVCbut setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];

    [FPVCtoolBar addSubview:FPVCbut];
    [self.newpassword setInputAccessoryView:FPVCtoolBar];
    // [self.originalPassword setInputAccessoryView:FPVCtoolBar];
}

- (void)keyboardHide:(UIGestureRecognizer*)gestureRecognizer
{
    NSLog(@"----->%@--->",gestureRecognizer);
    [self resignRespondersForTextFields];
}
- (void)textLengthMethod:(UITextField*)textField
{
    [self setSendViewShow];
    if (textField == self.originalPassword) {
        if (textField.text.length > 12) {
            textField.text = [textField.text substringToIndex:12];
        }
    }
    if (textField == self.newpassword) {
        if (textField.text.length > 12) {
            textField.text = [textField.text substringToIndex:12];
        }
    }
}
- (void)setSendViewShow
{
    if (self.originalPassword.text.length > 0 && self.newpassword.text.length > 0) {
        self.forgetPasswordBtn.userInteractionEnabled = YES;
        [self.forgetPasswordBtn setTitleColor:RGB(14, 118, 199, 1) forState:UIControlStateNormal];
        FPVCbut.userInteractionEnabled = YES;
        [FPVCbut setTitleColor:RGB(14, 118, 199, 1) forState:UIControlStateNormal];

      //  _revealPasBtn.hidden = NO;
     //   [_revealPasBtn setTitle:NSLocalizedString(@"revealBtn1", nil) forState:UIControlStateNormal];
    }
    else {
        self.forgetPasswordBtn.userInteractionEnabled = NO;
        [self.forgetPasswordBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        FPVCbut.userInteractionEnabled = NO;
        [FPVCbut setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
       // _revealPasBtn.hidden = YES;
    }
}

- (void)resignRespondersForTextFields
{
    if (isFPVCClick) {
        isFPVCClick = NO;
        
        if (SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"568.0")) {
            CGRect frame = self.view.frame;
            frame.origin.y = 64;
            [UIView animateWithDuration:0.24 animations:^{
                [self.view setFrame:frame];
            }
                             completion:nil];
        }
        
        [_newpassword resignFirstResponder];
        [_originalPassword resignFirstResponder];

    }
}
- (void)textFieldDidBeginEditing:(UITextField*)textField
{
    if (!isFPVCClick) {
        isFPVCClick = YES;
    }
    
    if (SYSTEM_VERSION_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"480.0")) {
        CGRect newFrome = self.view.frame;
        newFrome.origin.y = -textField.tag * textField.frame.size.height;
        [UIView animateWithDuration:0.24 animations:^{
            [self.view setFrame:newFrome];
        }
        completion:^(BOOL finished){
        }];

    }else if(SYSTEM_VERSION_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"568.0")) {
        if ( textField ==_newpassword) {
            CGRect newFrome = self.view.frame;
            newFrome.origin.y = 40;
            [UIView animateWithDuration:0.24 animations:^{
                [self.view setFrame:newFrome];
            } completion:^(BOOL finished) {
                
            }];
        }
    }else if (SYSTEM_VERSION_EQUAL_TO_W_orH(SCREEN_HEIGHT, @"568.0")) {
        if (textField == _newpassword) {
            CGRect newFrome = self.view.frame;
            newFrome.origin.y = 40;
            [UIView animateWithDuration:0.24 animations:^{
                [self.view setFrame:newFrome];
            }
                completion:^(BOOL finished){

                }];
        }
    }
}
- (BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if (textField == _originalPassword) {
        [_newpassword becomeFirstResponder];
    }
    else {
        [self resignRespondersForTextFields];
    }
    return YES;
}

- (IBAction)clickForgetPasswordBtn:(UIButton*)sender
{
    [self checkUserDetails];
}
- (void)checkUserDetails
{

    NSCharacterSet* whithNewChars = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString* newpassword = [_newpassword.text stringByTrimmingCharactersInSet:whithNewChars];
    NSString* password = [_originalPassword.text stringByTrimmingCharactersInSet:whithNewChars];

    [self resignRespondersForTextFields];

    if (![isPhoneNumber checkPasswordAndNum:password]) {
         [MBProgressHUD yty_showErrorWithTitle:nil detailsText:NSLocalizedString(@"FPVCP1", nil) toView:nil];
    }
    else {
        if (![isPhoneNumber checkPasswordAndNum:newpassword]) {
              [MBProgressHUD yty_showErrorWithTitle:nil detailsText:NSLocalizedString(@"FPVCP1", nil) toView:nil];
        }
        else {
            if (![password isEqualToString:newpassword]) {
                  [MBProgressHUD yty_showErrorWithTitle:nil detailsText:NSLocalizedString(@"FPVCP2", nil) toView:nil];
            }
            else {

                NSDictionary* dic = @{ @"loginName" : _FPVC_phoneNumber,
                    @"verifyCode" : _FPVC_SMSCode,
                    @"zone" : _FPVC_areaNam,
                    @"password" : password };

               /* [[NetRequestClss sharedNetRequestClss] requestWithUrl:@"userInfo/userReset.asp" requestWithHeader:nil requestWithParameters:dic requestWithViewController:self requestWithApiID:@"userReset.asp" isMethod:true isListLoaded:false result:^(id objs, int status, NSString* msg) {

                    NSDictionary* body = [objs objectForKey:@"body"];
                    int result = [[body objectForKey:@"result"] intValue];

                    if (result == 0) {
                          [MBProgressHUD yty_showErrorWithTitle:nil detailsText:NSLocalizedString(@"FPVCP3", nil) toView:nil];
                    }
                    else if (result == 1) {
                        // 返回登录页
                        [MBProgressHUD showSuccess:NSLocalizedString(@"FPVCP4", nil)];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [self.navigationController popToRootViewControllerAnimated:YES];
                        });
                    }
                }]; */
                [LoginViewModel requestWithUrlForForgetPassword:self andParam:dic success:^(id model) {
                    // 返回登录页
                    [MBProgressHUD showSuccess:NSLocalizedString(@"FPVCP4", nil)];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    });
                } failure:^(NSString *err) {
                    [MBProgressHUD yty_showErrorWithTitle:nil detailsText:err toView:nil];
                }];
            }
        }
    }
}


/*- (IBAction)clickRevealPas:(UIButton*)sender
{
    if ([sender.titleLabel.text isEqualToString:NSLocalizedString(@"revealBtn1", nil)]) {
        _newpassword.secureTextEntry = NO;
        _originalPassword.secureTextEntry = NO;
        [_revealPasBtn setTitle:NSLocalizedString(@"revealBtn2", nil) forState:UIControlStateNormal];
    }
    else {
        _newpassword.secureTextEntry = YES;
        _originalPassword.secureTextEntry = YES;
        [_revealPasBtn setTitle:NSLocalizedString(@"revealBtn1", nil) forState:UIControlStateNormal];
    }
}*/

@end
