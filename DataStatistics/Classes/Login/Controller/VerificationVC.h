//
//  VerificationVC.h
//  DataStatistics
//
//  Created by Kang on 16/3/10.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextField+LolitaText.h"

@interface VerificationVC : UIViewController
/** VerificationVC 接收 标识*/
@property (strong, nonatomic) NSString *VVCsignStr;
/** VerificationVC 接收 手机号码*/
@property (strong, nonatomic) NSString *VVC_phoneNumber;
/** VerificationVC 接收 手机区号*/
@property (strong, nonatomic) NSString *VVC_areaNam;
/** 验证码输入框*/
@property (strong, nonatomic) IBOutlet UITextField *VerificationText;
/** 计时导语*/
@property (strong, nonatomic) IBOutlet UILabel *textContent;
/** next 按钮*/
@property (strong, nonatomic) IBOutlet UIButton *nextBtn;
/** 再次获取验证码 按钮*/
@property (strong, nonatomic) IBOutlet UIButton *againGetSMSBtn;
/** VerificationVC 接收 窗口 */
@property (nonatomic,strong) UIWindow* VVC_window;


/// 下一页
- (IBAction)verificationNext:(id)sender;
/// 再来一次获取验证码
- (IBAction)againGetSMS:(UIButton *)sender;

@end
