//
//  isPhoneNumber.h
//  DataStatistics
//
//  Created by Kang on 16/1/5.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface isPhoneNumber : NSObject
+ (BOOL)isMobileNumber:(NSString *)mobileNum;
/** 用户名验证 */
+ (BOOL)checkUserName : (NSString *) userName;
/** 密码验证（数字英文结合） */
+ (BOOL)checkPassword:(NSString *) password;
/** 密码验证（任何数字英文） */
+ (BOOL)checkPasswordAndNum:(NSString *) password;

-(NSString *)getMD5:(NSString *)str;
+(NSString *)getMD5:(NSString *)str;

-(BOOL)validateMobile:(NSString *)mobile;
+(BOOL)validateMobile:(NSString *)mobile;

-(void)showAlert:(NSString *)str;
+(void)showAlert:(NSString *)str;

-(BOOL) validatePassword:(NSString *)passWord;
+(BOOL) validatePassword:(NSString *)passWord;

-(BOOL) validateIdentityCard: (NSString *)identityCard;
+(BOOL) validateIdentityCard: (NSString *)identityCard;

-(BOOL) validateAddress:(NSString *)address;
+(BOOL) validateAddress:(NSString *)address;

-(BOOL) validateFixnum:(NSString *)fixnum;
+(BOOL) validateFixnum:(NSString *)fixnum;

-(BOOL) validateQQ:(NSString *)qq;
+(BOOL) validateQQ:(NSString *)qq;

-(BOOL) validateEmail:(NSString *)email;
+(BOOL) validateEmail:(NSString *)email;

-(BOOL) validatePrice:(NSString *)price;
+(BOOL) validatePrice:(NSString *)price;
//正整数
-(BOOL) validateNumber:(NSString *)number;
+(BOOL) validateNumber:(NSString *)number;

//判断是否有非法字符
+(BOOL) illegalCharacter:(NSString *)string;



@end
