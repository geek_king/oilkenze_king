//
//  ForgetPasswordVC.h
//  DataStatistics
//
//  Created by Kang on 16/1/4.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton+countDown.h"
#import "UITextField+LolitaText.h"
@interface ForgetPasswordVC : UIViewController

/** ForgetPasswordVC 接收 标识*/
@property (strong, nonatomic) NSString *FPVCsignStr;
/** ForgetPasswordVC 接收 手机号码*/
@property (strong, nonatomic) NSString *FPVC_phoneNumber;
/** ForgetPasswordVC 接收 手机区号*/
@property (strong, nonatomic) NSString *FPVC_areaNam;
/** ForgetPasswordVC 接收 验证码 */
@property (strong, nonatomic) NSString *FPVC_SMSCode;

/** 导语*/
@property (strong, nonatomic) IBOutlet UILabel *FPVCguideLabel;
/** 账号输入框 */
@property (strong, nonatomic) IBOutlet UITextField *iphoneNum;
/** 原密码输入框 */
@property (strong, nonatomic) IBOutlet UITextField *originalPassword;
/** 新密码输入框 */
@property (strong, nonatomic) IBOutlet UITextField *newpassword;
/** 修改按钮 */
@property (strong, nonatomic) IBOutlet UIButton    *forgetPasswordBtn;
/** 显示密码  */
//@property (strong, nonatomic) IBOutlet UIButton *revealPasBtn;
/// 点击修改修改密码方法
- (IBAction)clickForgetPasswordBtn:(UIButton *)sender;
//- (IBAction)clickRevealPas:(UIButton *)sender;



@end
