//
//  UITextField+LolitaText.m
//  DataStatistics
//
//  Created by Kang on 16/3/21.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "UITextField+LolitaText.h"

@implementation UITextField (LolitaText)

// 键盘监控
- (void)addObserverForWillKeyBoard:(id)obj {
    
    //增加监听，当键盘出现或改变时接收出消息
    [[NSNotificationCenter defaultCenter] addObserver:obj selector:@selector(LTkeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    //增加监听，当键盘退出时接收出消息
    [[NSNotificationCenter defaultCenter] addObserver:obj selector:@selector(LTkeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)removeObserverForKeyBoard:(id)obj {
    [[NSNotificationCenter defaultCenter] removeObserver:obj name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:obj name:UIKeyboardWillShowNotification object:nil];
}
/** 子类重写 -当键盘出现或改变时调用 */
- (void)LTkeyboardWillShow:(NSNotification *)not {}
/** 子类重写 -当键盘退出时调用 */
- (void)LTkeyboardWillHide:(NSNotification *)not {}






// text输入监控
- (void)addTextFiledNotDidFortext {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFiledDidBeginEditingNot:) name:UITextFieldTextDidBeginEditingNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidEndEditingNot:) name:UITextFieldTextDidEndEditingNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChangeNot:) name:UITextFieldTextDidChangeNotification object:nil];
}
- (void)removeTextFiledNotDidFortext {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidBeginEditingNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidEndEditingNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
}

/** 子类重写 -失去焦点时调用 */
- (void)textFieldDidEndEditingNot:(NSNotification *)not {
//    if (_lolitadelegate != nil && [_lolitadelegate respondsToSelector:@selector(textFieldDidEndEditingNotification:)]) {
//        [self.lolitadelegate textFieldDidEndEditingNotification:not];
//    }
}
/** 子类重写 -持续获取焦点时调用 */
- (void)textFieldDidChangeNot:(NSNotification *)not {
//    if (_lolitadelegate != nil && [_lolitadelegate respondsToSelector:@selector(textFieldDidChangeNotification:)]) {
//        [self.lolitadelegate textFieldDidChangeNotification:not];
//    }
}
/** 子类重写 -获取焦点时调用 */
- (void)textFiledDidBeginEditingNot:(NSNotification *)not {
//    if (_lolitadelegate != nil && [_lolitadelegate respondsToSelector:@selector(textFiledDidBeginEditingNotification:)]) {
//        [self.lolitadelegate textFiledDidBeginEditingNotification:not];
//    }
}

// 样式 1
- (void)addYTYanKTextFielStyle1:(UITextField *)text withString:(nullable NSString *)str{
//    if (SCREEN_WIDTH == 320) {
//        [self setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:15.f]];
//    }else {
//        [self setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18.f]];
//    }


    [text setClearButtonMode:UITextFieldViewModeWhileEditing]; // 清楚按钮

    [text setTintColor:[UIColor whiteColor]];//光标
    [text setBorderStyle:UITextBorderStyleNone];// 边框样式
    [text setAutocorrectionType:UITextAutocorrectionTypeNo]; // 自动校正型
    [text setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [text setTextColor:[UIColor whiteColor]]; //字体颜色
    
   // UIImage *imageBack = [UIImage imageNamed:@"textFieldBack"]; //需要换成一条颜色
  //  [self setBackground:imageBack];
    //[self setValue:placeholerLabelFont forKeyPath:@"_placeholderLabel.font"];
    //[self setValue:placeholderLabelColor forKeyPath:@"_placeholderLabel.textColor"];
    
    NSMutableParagraphStyle *style = [text.defaultTextAttributes[NSParagraphStyleAttributeName] mutableCopy];
    
    if (SCREEN_WIDTH == 320) {
       style.minimumLineHeight = text.font.lineHeight - (text.font.lineHeight - [UIFont systemFontOfSize:15.f].lineHeight) / 2.0;  // 判断大小选择字体
    }else {
        style.minimumLineHeight =  text.font.lineHeight - (text.font.lineHeight - [UIFont systemFontOfSize:18.f].lineHeight) /2.0;
    }
     UIColor *color = [UIColor colorWithWhite:144/255.0f alpha:1.0f];
    
    text.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName:color,NSFontAttributeName:[UIFont systemFontOfSize:15.f],NSParagraphStyleAttributeName:style}];
    
    [text setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];  // 垂直对齐
    [text setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];// 水平对齐
    
   // [text addTarget:target action:action forControlEvents:UIControlEventEditingChanged];
    
    //[self addTextFiledNotDidFortext];
    UIButton* clearButton = [text valueForKey:@"_clearButton"];
    [clearButton setImage:[UIImage imageNamed:@"关闭按钮"] forState:UIControlStateNormal];
    [clearButton setImage:[UIImage imageNamed:@"关闭按钮-on"] forState:UIControlStateHighlighted];
}
// 样式 2
- (void)addYTYanKTextFielStyle2:(UITextField *)text withString:(NSString *)str {
    
    text.clearButtonMode       = UITextFieldViewModeUnlessEditing;
    NSMutableParagraphStyle *style = [text.defaultTextAttributes[NSParagraphStyleAttributeName] mutableCopy];
    style.minimumLineHeight = text.font.lineHeight - (text.font.lineHeight - [UIFont systemFontOfSize:15.f].lineHeight) / 2.0;
    text.attributedPlaceholder = [[NSAttributedString alloc] initWithString:str attributes:@{NSForegroundColorAttributeName:[UIColor grayColor],NSFontAttributeName:[UIFont systemFontOfSize:15.f],NSParagraphStyleAttributeName:style}];
    
}

// 添加toolBar ---未完成
- (void)addYTYanKToolBarView:(UIToolbar *)toolView withItem:(UIButton *)itemBtn {
    toolView =  [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 26)];
    toolView.backgroundColor = RGB(28, 28, 28, 1);
    [toolView setBarStyle:UIBarStyleBlack];
    
    itemBtn = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 32 - 20, -2, 32, 30)];
    [itemBtn setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    itemBtn.titleLabel.font = [UIFont systemFontOfSize:14.f];
    //[itemBtn addTarget:self action:@selector(clickItemCodeBtn:) forControlEvents: UIControlEventTouchDown];
    //itemBtn.userInteractionEnabled = NO;
    //[itemBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    
    [toolView addSubview:itemBtn];
    [self setInputAccessoryView:toolView];
}


// 中文监控输入
- (void)addEditChangedNotificationWithObserver:(UITextField *)field {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textfiledEditChangedMeth:) name:UITextFieldTextDidChangeNotification object:field];
}



// 监控NickName中文输入方法(限制长度)
- (void)textfiledEditChangedMeth:(NSNotification *)obj {
    UITextField *textfield = (UITextField *)obj.object;
    NSString *toBeString = textfield.text;
    // 键盘输入模式
    NSString *lang =[[UITextInputMode currentInputMode] primaryLanguage];
    // 简体中文输入，包括简体拼音，健体五笔，简体手写
    if ([lang isEqualToString:@"zh-Hans"]) {
        UITextRange *selectedRange = [textfield markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textfield positionFromPosition:selectedRange.start offset:0];
        // 没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if (!position) {
            if (toBeString.length > 10) {
                // textfield.text = [toBeString substringFromIndex:10]; //如果超过十个重新清空内容，获取后面输入的内容。
                textfield.text = [toBeString substringToIndex:10];
            }
        }else {
            // 有高亮选择的字符串，则暂不对文字进行统计和限制
        }
    }
    // 中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
    else{
        if (toBeString.length > 20) {
            textfield.text = [toBeString substringToIndex:20];
        }
    }
    
}


@end
