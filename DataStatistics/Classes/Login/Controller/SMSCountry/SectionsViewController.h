//
//  SectionsViewController.h
//  DataStatistics
//
//  Created by Kang on 16/3/8.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SMSSDKCountryAndAreaCode;
@protocol SectionsViewControllerDelegate;



@interface SectionsViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
{
    UITableView *table;
    UISearchBar *search;
    NSDictionary *allNames;
    NSMutableDictionary *names;
    NSMutableArray *keys;
    
    BOOL isSearching;
}
@property (strong, nonatomic) id<SectionsViewControllerDelegate>delegate;

@property (strong, nonatomic) UITableView *table;
@property (strong, nonatomic) UISearchBar *search;
@property (strong, nonatomic) NSDictionary *allNames;
@property (strong, nonatomic) NSMutableDictionary *names;
@property (strong, nonatomic) NSMutableArray *keys;
@property (strong, nonatomic) UIToolbar *toolBar;

- (void)resetSearch;
- (void)HandleSearchForTerm:(NSString *)searcherm;
- (void)setareaArray:(NSMutableArray *)array;


@end

@protocol SectionsViewControllerDelegate <NSObject>
- (void)setSecondData:(SMSSDKCountryAndAreaCode *)data;
@end