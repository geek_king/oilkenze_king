//
//  PPQueryGpsModel.h
//  DataStatistics
//
//  Created by oilklenze on 15/12/8.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "JSONModel.h"


//单个设备数据模型

@protocol PPQueryGpsModel

@end

@interface PPQueryGpsModel : JSONModel
@property (nonatomic       ) NSString  *deviceID;

@property (nonatomic,strong) NSString  *deviceCode;
@property (nonatomic,strong) NSString  *c_name;

@property (nonatomic       ) float     longitude;
@property (nonatomic       ) float     latitude;
@property (nonatomic,strong) NSString  *upload_time;
@property (nonatomic       ) int       gtype;
@property (nonatomic       ) int       running_state;

@end

//@interface GPSSingleDetails : JSONModel
//
//@property (strong ,nonatomic)PPQueryGpsModel  *body;
//
//@end