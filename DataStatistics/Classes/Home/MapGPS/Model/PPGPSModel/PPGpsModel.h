//
//  PPGpsModel.h
//  DataStatistics
//
//  Created by oilklenze on 15/12/1.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"


//所有的地图设备数据模型

@protocol PPGpsModel
@end

@interface PPGpsModel : JSONModel

@property (nonatomic,strong) NSString  *deviceID;

@property (nonatomic,strong) NSString  *deviceCode;
@property (nonatomic,strong) NSString  *c_name;

@property (nonatomic       ) float     longitude;
@property (nonatomic       ) float     latitude;
@property (nonatomic,strong) NSString  *upload_time;
@property (nonatomic       ) int       gtype;
@property (nonatomic       ) int       running_state;

@end


//@interface DeviceListGps :JSONModel
//
//@property (nonatomic,strong) NSArray <PPGpsModel,ConvertOnDemand>  *ppGpsModel;
//
//
//@end