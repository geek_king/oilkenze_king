//
//  OneMapGPSViewModel.m
//  DataStatistics
//
//  Created by 123456 on 16/6/22.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "OneMapGPSViewModel.h"

@implementation OneMapGPSViewModel


+ (void) requestWithUrlForLoginIn:(id)obj  andParam:(NSDictionary *)param success:(void(^)(MapGPSModel * model))success failure:(void(^)(NSString * error))failure{
    
    
    [NetRequestClss requestWithUrl:@"deviceList/deviceListGps.asp?deviceCode" requestWithParameters:param method:1 returnSuccess:^(id objs, int status, NSString *mag) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [[UtilToolsClss getUtilTools] removeDoLoading];
        NSLog(@"objs--->%@--->%d-->%@",objs,status,mag);
        
//        
//        MapGPSModel *list = [MapGPSModel mj_keyValuesArrayWithObjectArray:objs[@"body"][@"deviceListGps"]];
//
        
        
        if (objs) {
            success(objs);
            return;
        }
        
        
    } returnError:^(NSString *err) {
        
        failure(err);
        
    }];
    
}




@end
