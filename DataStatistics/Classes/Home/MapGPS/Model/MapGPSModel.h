//
//  MapGPSModel.h
//  DataStatistics
//
//  Created by 123456 on 16/6/22.
//  Copyright © 2016年 YTYangK. All rights reserved.
//
#import <Foundation/Foundation.h>

#import "JSONModel.h"
//所有的地图设备数据模型

@protocol MapGPSModel
@end

@interface MapGPSModel : JSONModel
singleton_for_interface(MapGPSModel)

@property (nonatomic       ) NSString  *deviceID;

@property (nonatomic,strong) NSString  *deviceCode;
@property (nonatomic,strong) NSString  *c_name;

@property (nonatomic       ) float     longitude;
@property (nonatomic       ) float     latitude;
@property (nonatomic,strong) NSString  *upload_time;
@property (nonatomic       ) int       gtype;
@property (nonatomic       ) int       running_state;

@end

@interface DeviceListGps :JSONModel

@property (nonatomic,strong) NSArray <MapGPSModel,ConvertOnDemand>  *mapGPSModel;



@end

@interface GPSSingleDetails : JSONModel

@property (strong ,nonatomic)MapGPSModel  *body;

@end

