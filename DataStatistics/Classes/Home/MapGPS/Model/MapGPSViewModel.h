//
//  MapGPSViewModel.h
//  DataStatistics
//
//  Created by 123456 on 16/6/22.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "ViewModelClass.h"
#import "MapGPSModel.h"


@interface MapGPSViewModel : ViewModelClass


+ (void) requestWithUrlForLoginIn:(id)obj  andParam:(NSDictionary *)param success:(void(^)(MapGPSModel * model))success failure:(void(^)(NSString * error))failure;




@end
