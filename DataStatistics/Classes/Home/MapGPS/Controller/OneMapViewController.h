//
//  OneMapViewController.h
//  DataStatistics
//
//  Created by oilklenze on 15/11/13.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "KCAnnotation.h"
#import "Masonry.h"
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>
#import "AddressView.h"

@interface OneMapViewController : UIViewController
@property (retain, nonatomic) NSString         *mapDeviceCode;
@property (strong, nonatomic) IBOutlet MKMapView        *mapview;
@property (strong, nonatomic) AddressView      *addressView;


@end
