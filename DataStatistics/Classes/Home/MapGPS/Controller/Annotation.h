//
//  Annotation.h
//  DataStatistics
//
//  Created by oilklenze on 15/12/8.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import <MapKit/MapKit.h>

typedef void (^placemarks2)(NSDictionary* placemark);

@interface Annotation : MKPointAnnotation
@property (nonatomic,strong) UIImage           *icon;
@property (nonatomic,strong) NSString          *c_name;
@property (nonatomic,strong) NSString          *deviceCode;
@property (nonatomic       ) int               running_state;
@property (nonatomic,copy  ) NSString          *upload_time;
@property (nonatomic,      ) int               index;
@property (nonatomic,strong) NSNumber          * sort;

+ (void)getAddressByLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude addrees:(placemarks2)addressDictionary; //回调一个地址字典

@end
