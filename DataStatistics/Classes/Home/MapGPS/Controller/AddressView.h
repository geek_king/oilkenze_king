//
//  AddressView.h
//  DataStatistics
//
//  Created by oilklenze on 16/3/25.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>

@import CoreLocation;

typedef void (^place)(NSString* placemark);

@interface AddressView : UIImageView
@property (strong, nonatomic) UILabel          *address;
@property (strong, nonatomic) UIButton         *navigationBtm;
@property (nonatomic,strong)UIActivityIndicatorView *activity;

+ (void)getAddressByLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude addrees:(place)address; //回调一个地址字典

- (void)addAppearAnimationByBeginFrame:(CGRect)begin finishFrame:(CGRect)finsish;
- (void)labelSetAddress:(NSString *)newAddress;

@end
