//
//  AddressView.m
//  DataStatistics
//
//  Created by oilklenze on 16/3/25.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import "AddressView.h"
#import "Masonry.h"
#import <pop/POP.h>

@import AddressBookUI;
@interface AddressView ()
@end

@implementation AddressView


- (instancetype)init
{
    self = [super init];
    if (self !=nil) {
        
        _activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _activity.center =self.center;
        _activity.width  = 20;
        _activity.height = 20;
        [_activity startAnimating];
        [self setImage:[UIImage imageNamed:@"地址背景"]];
         self.userInteractionEnabled = YES;
        
        if (!_address) {
            _address = [[UILabel alloc] init];
            [_address setTextColor:RGB(200, 200, 200, 0.95)];
            [self addSubview:_address];
            [_address setNumberOfLines:0];
            [_address mas_makeConstraints:^(MASConstraintMaker* make) {
                make.left.equalTo(self).with.offset(60);
                make.bottom.equalTo(self);
                make.right.equalTo(self).with.offset(-8);
                make.top.equalTo(self);
            }];
            [_address setFont:[UIFont systemFontOfSize:12]];
            [_address addSubview:_activity];
            [_activity mas_makeConstraints:^(MASConstraintMaker *make) {
                make.center.equalTo(self);
                make.size.mas_equalTo(CGSizeMake(20, 20));
            }];
        }
        if (!_navigationBtm) {
            _navigationBtm = [[UIButton alloc] init];
            [self addSubview:_navigationBtm];
            [_navigationBtm setBackgroundImage:[UIImage imageNamed:@"导航按钮"] forState:UIControlStateNormal];
            [_navigationBtm mas_makeConstraints:^(MASConstraintMaker* make) {
                make.left.equalTo(self);
                make.top.equalTo(self);
                make.bottom.equalTo(self);
                make.width.mas_equalTo(@52);
            }];
        }

    }
    return self;
}

+ (void)getAddressByLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude addrees:(place)address
{
    //反地理编码
    CLGeocoder* geocoder = [[CLGeocoder alloc] init];
    CLLocation* location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray* placemarks, NSError* error) {
        NSLog(@"---->%@",placemarks);
        CLPlacemark* placemark = [placemarks firstObject];
        NSLog(@"-------%@",placemark);
        NSDictionary *ds = placemark.addressDictionary;
        NSLog(@"-------->%@",ds);
        
        NSString *address1 =@"";
//        address1 = ABCreateStringWithAddressDictionary(placemark.addressDictionary, NO);
//        address1 =  [address1 stringByReplacingOccurrencesOfString:@"\n" withString:@","];
//        address1 = [address1 stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
//        address(address1);
        NSLog(@"----%@",error);
//            address1 = [NSString stringWithFormat:@"%@ %@ %@ %@ %@",
//                        ds[@"SubThoroughfare"]?ds[@"SubThoroughfare"]:@"",
//                        ds[@"Thoroughfare"]?ds[@"Thoroughfare"]:@"",
//                        ds[@"City"]?ds[@"City"]:@"",
//                        ds[@"State"]?ds[@"State"]:@"",
//                        ds[@"Country"]?ds[@"Country"]:@""];
//        address1 = [address1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//        address1 = [address1 stringByAppendingString:ds[@"Name"]];
        for (NSString *address in ds[@"FormattedAddressLines"]) {
            NSString *temaddr = [address stringByAppendingString:@" "];
            address1=[address1 stringByAppendingString:temaddr];
        }

        address(address1);
        
    }];
    
}

- (void)addAppearAnimationByBeginFrame:(CGRect)begin finishFrame:(CGRect)finsish
{
    POPBasicAnimation* anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewFrame];
    anim.fromValue = [NSValue valueWithCGRect:begin];
    anim.toValue = [NSValue valueWithCGRect:finsish];
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    anim.duration = 0.5;
    [self pop_addAnimation:anim forKey:nil];
    
    
}



- (void)labelSetAddress:(NSString *)newAddress{
    
    if ([newAddress isEqualToString:@""] || !newAddress) {
        [self.activity startAnimating];
    }else{
        [self.activity stopAnimating];
    }

    self.address.text =newAddress;
}

@end
