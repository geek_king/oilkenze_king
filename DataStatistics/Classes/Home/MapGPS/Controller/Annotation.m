//
//  Annotation.m
//  DataStatistics
//
//  Created by oilklenze on 15/12/8.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "Annotation.h"




@implementation Annotation


+ (void)getAddressByLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude addrees:(placemarks2)addressDictionary
{
    
    //反地理编码
    CLGeocoder* geocoder = [[CLGeocoder alloc] init];
    CLLocation* location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray* placemarks, NSError* error) {
        CLPlacemark* placemark = [placemarks firstObject];
        
        NSLog(@"详细信息:%@", placemark.addressDictionary);
        
        addressDictionary(placemark.addressDictionary);
        
    }];
    
}
@end

