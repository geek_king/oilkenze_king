//
//  CanlloutAnnotationView.h
//  DataStatistics
//
//  Created by oilklenze on 15/12/7.
//  Copyright © 2015年 YTYangK. All rights reserved.
//
#import "KCAnnotation.h"
#import <MapKit/MapKit.h>

@protocol passDeviceCodeInMapView <NSObject>

- (void)passDeviceCodeInMapView:(NSString*)DeviceCode; //传递DeviceCode值去mapView 然后页面跳转==》push 去列表详情
- (void)addTableViewInMapView:(NSNumber*)sort;
@end

@interface CustomView : UIView;
@end

@interface CanlloutAnnotationView : MKAnnotationView
@property (strong, nonatomic) id<passDeviceCodeInMapView> delegate;
@property (strong, nonatomic) KCAnnotation* annotation;
@property (strong, nonatomic) UIImageView* view;
@property (strong, nonatomic) UIButton* btn; //自定义弹框右边按钮

- (void)setSingleLabelText:(KCAnnotation*)annotation;
- (void)initSubView;
- (void)setMultipleLabelTextAndOnlineStats:(NSArray*)multipleAnnotationGrouds;

@end
