//
//  CanlloutAnnotationView.m
//  DataStatistics
//
//  Created by oilklenze on 15/12/7.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "Annotation.h"
#import "CanlloutAnnotationView.h"
#import "Masonry.h"

#pragma mark-------自定义view 的实现、

@implementation CustomView

- (void)drawRect:(CGRect)rect
{

    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor* color = RGB(40, 40, 40, 1);
    [color setFill];
    UIBezierPath* apath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 300, 60) cornerRadius:150 / 12];
    [apath fill];
    CGContextMoveToPoint(context, 300 * 0.7 - 10, 60);
    CGContextAddQuadCurveToPoint(context, 300 * 0.7, 60, 300 * 0.7 + 10, 80);
    CGContextAddQuadCurveToPoint(context, 300 * 0.7 + 20, 60, 300 * 0.7 + 20 + 10, 60);
    [color setFill];
    CGContextDrawPath(context, kCGPathFill);
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        self.opaque = NO;
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.opaque = NO;
    }
    return self;
}

@end

@interface CanlloutAnnotationView ()
@property (strong, nonatomic) UILabel  * c_nameLabel;
@property (strong, nonatomic) UILabel  * deviceCodeLabel;
@property (strong, nonatomic) UILabel  * up_tameLabel;
@property (strong, nonatomic) UIButton * onlineEquipment;//多台设备叠在一起 在线显示的图标；
@property (strong, nonatomic) UIButton * offlineEqueipment;//多台设备叠在一起离线显示的图标；

@end

@implementation CanlloutAnnotationView

//设置单台UI
- (void)setSingleLabelText:(KCAnnotation*)annotation
{
    
    [self.btn setImage:[UIImage imageNamed:@"进入图标"] forState:UIControlStateNormal];
    self.c_nameLabel.text     = annotation.c_name;
    self.deviceCodeLabel.text = annotation.deviceCode;
    self.up_tameLabel.text    =  [UtilToolsClss timestampStringTransformDateString:annotation.upload_time];
    
    
}


//设置多台UI
- (void)setMultipleLabelTextAndOnlineStats:(NSArray*)multipleAnnotationGrouds
{
    [self.onlineEquipment setImage:[UIImage imageNamed:@"在线机器"] forState:UIControlStateNormal];
    [self.offlineEqueipment setImage:[UIImage imageNamed:@"离线机器"] forState:UIControlStateNormal];
    [self.btn setImage:[UIImage imageNamed:@"进入列表按钮"] forState:UIControlStateNormal];
    self.c_nameLabel.text        = self.annotation.c_name;
    NSArray* multipleAnnotations = nil;

    for (int i = 0; i < multipleAnnotationGrouds.count; i++) {
        if (i == self.annotation.sort.intValue - 1) {
            multipleAnnotations = (NSArray*)multipleAnnotationGrouds[i];
        }
    }

    int online  = 0;
    int offline = 0;

    for (Annotation* annotation in multipleAnnotations) {
        if (annotation.running_state == 1) {
            online++;
        }
        else {
            offline++;
        }
    }

    [self.onlineEquipment setTitle:[NSString stringWithFormat:@"%d", online] forState:UIControlStateNormal];
    [self.offlineEqueipment setTitle:[NSString stringWithFormat:@"%d", offline] forState:UIControlStateNormal];
}
//布局单台UI
- (void)initSubView
{
    [self.btn addTarget:self action:@selector(buttonHandlerCallOut:) forControlEvents:UIControlEventTouchUpInside];
    self.btn.frame = CGRectMake(247, 0, 53, 62.5);
    [self.view addSubview:self.btn];
    [self.btn mas_makeConstraints:^(MASConstraintMaker* make) {
        make.top.equalTo(self.view);
        make.right.equalTo(self.view);
        make.left.equalTo(self.view).with.offset(247);
        make.bottom.equalTo(self.view).with.offset(62.5 - 80);

    }];

    if (self.annotation.sort) {
        [self initMultipleCanlloutView];
        [self.btn setTag:2];
    }
    else{
        [self setSelected:YES animated:YES];
        [self.btn setTag:1];
    }
}
//布局多台UI
- (void)initMultipleCanlloutView
{
    [self addSubview:self.view];
    self.c_nameLabel.frame = CGRectMake(10, 0, 237, 30);
    [self.c_nameLabel setTextColor:[UIColor whiteColor]];
    [self.c_nameLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCallout]];
    [self.view addSubview:self.c_nameLabel];
    [self.c_nameLabel mas_makeConstraints:^(MASConstraintMaker* make) {
        make.top.equalTo(self.view);
        make.left.equalTo(self.view).with.offset(10);
        make.right.equalTo(self.view).with.offset( -53);
        make.bottom.equalTo(self.view).with.offset(30 + 10 - 80);

    }];
    [self.view setUserInteractionEnabled:YES];
    self.onlineEquipment.frame = CGRectMake(10, 30, 38, 30);
    [self.view addSubview:self.onlineEquipment];
    [self.onlineEquipment mas_makeConstraints:^(MASConstraintMaker* make) {
        make.top.equalTo(self.view).with.offset(30);
        make.left.equalTo(self.view).with.offset(10);
        make.right.equalTo(self.view).with.offset(38 + 10 - 300);
        make.bottom.equalTo(self.view).with.offset(30 + 30 - 80);
    }];
    [self.onlineEquipment.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]];
    self.offlineEqueipment.frame = CGRectMake(48, 30, 38, 30);
    [self.offlineEqueipment.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]];
    [self.view addSubview:self.offlineEqueipment];
    [self.offlineEqueipment mas_makeConstraints:^(MASConstraintMaker* make) {
        make.top.equalTo(self.view).with.offset(30);
        make.left.equalTo(self.view).with.offset(48);
        make.right.equalTo(self.view).with.offset(38 + 48 - 300);
        make.bottom.equalTo(self.view).with.offset(30 + 30 - 80);

    }];
}
//复写选择弹出框方法。
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected) {
        [self addSubview:self.view];
        self.c_nameLabel.frame = CGRectMake(10, 5, 237, 25);
        [self.c_nameLabel setTextColor:[UIColor whiteColor]];
        [self.c_nameLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCallout]];
        self.deviceCodeLabel.frame = CGRectMake(10, 30, 105, 25);
        [self.deviceCodeLabel setTextColor:RGB(225, 225, 225, 0.8)];
        [self.deviceCodeLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCallout]];

        self.up_tameLabel.frame = CGRectMake(115, 30, 132, 25);
        [self.up_tameLabel setTextColor:RGB(225, 225, 225, 0.8)];
        [self.up_tameLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleCaption1]];

        [self.view addSubview:self.c_nameLabel];
        [self.c_nameLabel mas_makeConstraints:^(MASConstraintMaker* make) {
            make.top.equalTo(self.view).with.offset(5);
            make.left.equalTo(self.view).with.offset(10);
            make.right.equalTo(self.view).with.offset(237 + 10 - 300);
            make.bottom.equalTo(self.view).with.offset(25 + 5 - 80);
        }];
        [self.view addSubview:self.deviceCodeLabel];
        [self.deviceCodeLabel mas_makeConstraints:^(MASConstraintMaker* make) {
            make.top.equalTo(self.view).with.offset(30);
            make.left.equalTo(self.view).with.offset(10);
            make.right.equalTo(self.view).with.offset(105 + 10 - 300);
            make.bottom.equalTo(self.view).with.offset(25 + 30 - 80);

        }];
        
        [self.view addSubview:self.up_tameLabel];
        [self.up_tameLabel mas_makeConstraints:^(MASConstraintMaker* make) {
            make.top.equalTo(self.view).with.offset(30);
            make.left.equalTo(self.view).with.offset(115);
            make.right.equalTo(self.view).with.offset(132 + 115 - 300);
            make.bottom.equalTo(self.view).with.offset(25 + 30 - 80);
        }];
        [self.view setUserInteractionEnabled:YES];
    }
    else {
        //Remove your custom view...
        [self.view setUserInteractionEnabled:NO];
        [self.view removeFromSuperview];
        self.view = nil;
    }
}


#pragma mark--------map 页面跳转PUSH 响应代理方法

- (void)buttonHandlerCallOut:(UIButton*)button
{

    if (button.tag == 1) {
        if ([_delegate respondsToSelector:@selector(passDeviceCodeInMapView:)]) {
            [self.delegate passDeviceCodeInMapView:self.annotation.deviceCode];
        }
    }
    else {
        if ([_delegate respondsToSelector:@selector(addTableViewInMapView:)]) {
            [self.delegate addTableViewInMapView:self.annotation.sort];
        }
    }
}

//激活手势
- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent*)event
{
    UIView* v = [super hitTest:point withEvent:event];
    if (v != nil) {
        [self.superview bringSubviewToFront:self];
    }
    return v;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent*)event
{
    CGRect rec = self.bounds;
    BOOL isIn = CGRectContainsPoint(rec, point);
    if (!isIn) {
        for (UIView* v in self.subviews) {
            isIn = CGRectContainsPoint(v.frame, point);
            if (isIn)
                break;
        }
    }
    return isIn;
}

//getter setter
- (UIImageView*)view
{

    if (!_view) {
        _view = [[UIImageView alloc] init];
        _view = [[UIImageView alloc] init];
        _view.userInteractionEnabled = YES;
        [_view setImage:[UIImage imageNamed:@"标释背景"]];
    }

    return _view;
}

- (UILabel*)c_nameLabel
{
    if (!_c_nameLabel) {
        _c_nameLabel = [[UILabel alloc] init];
    }
    return _c_nameLabel;
}

- (UILabel*)deviceCodeLabel
{
    if (!_deviceCodeLabel) {
        _deviceCodeLabel = [[UILabel alloc] init];
    }
    return _deviceCodeLabel;
}

- (UILabel*)up_tameLabel
{
    if (!_up_tameLabel) {
        _up_tameLabel = [[UILabel alloc] init];
    }
    return _up_tameLabel;
}

- (UIButton*)btn
{
    if (!_btn) {
        _btn = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    return _btn;
}


- (UIButton*)onlineEquipment
{
    if (!_onlineEquipment) {
        _onlineEquipment = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    _onlineEquipment.userInteractionEnabled = NO;

    return _onlineEquipment;
}

- (UIButton*)offlineEqueipment
{

    if (!_offlineEqueipment) {
        _offlineEqueipment = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    _offlineEqueipment.userInteractionEnabled = NO;
    return _offlineEqueipment;
}

@end
