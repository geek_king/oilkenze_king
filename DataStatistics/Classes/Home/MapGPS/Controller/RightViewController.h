//
//  RightViewController.h
//  DataStatistics
//
//  Created by oilklenze on 15/12/17.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightTableViewCell.h"
#import "CanlloutAnnotationView.h"

@interface RightViewController : UIViewController
@property (nonatomic, strong) NSArray              * annotations;
@property (weak, nonatomic  ) IBOutlet UITableView * tabelView;
@property (nonatomic, strong) RightTableViewCell   * cell;
@property (weak, nonatomic) IBOutlet UIView        * tempView;

@property (strong, nonatomic) id<passDeviceCodeInMapView> delegate;

@end
