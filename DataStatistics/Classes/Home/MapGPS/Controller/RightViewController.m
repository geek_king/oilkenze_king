//
//  RightViewController.m
//  DataStatistics
//
//  Created by oilklenze on 15/12/17.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import "Annotation.h"
#import "RightTableViewCell.h"
#import "RightViewController.h"
@interface RightViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation RightViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tabelView.delegate   = self;
    self.tabelView.dataSource = self;
    UINib* nib = [UINib nibWithNibName:@"RightTableViewCell" bundle:nil];
    [self.tabelView registerNib:nib forCellReuseIdentifier:@"RightTableViewCell"];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView

{
    return 1;
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.annotations.count;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath

{

    [tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    Annotation* annotation = nil;
    if ([self.annotations[indexPath.row] isKindOfClass:[Annotation class]]) {
        annotation = self.annotations[indexPath.row];
    }
    
    
    static NSString* cellIdentifier = @"RightTableViewCell";
    self.cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (annotation.running_state == 1) {
        self.cell.judgeOnlionOrNotInColor.backgroundColor = RGB(39, 201, 143, 1);
    }
    else {
        self.cell.judgeOnlionOrNotInColor.backgroundColor = RGB(138, 139, 140, 1);
    }
    self.cell.deviceCode.text = annotation.deviceCode;
    NSDate *date = [UtilToolsClss timestampStringTransformTimeCurrentPhoneTimeZooe:annotation.upload_time];
    annotation.upload_time?self.cell.lastTame.text = [UtilToolsClss timeTransFormStringFormatForLess24h:date]:(self.cell.lastTame.text = @"---");
    
    return self.cell;
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{

    Annotation* annotation = nil;
    if ([self.annotations[indexPath.row] isKindOfClass:[Annotation class]]) {
        annotation = self.annotations[indexPath.row];
    }

    if ([_delegate respondsToSelector:@selector(passDeviceCodeInMapView:)]) {
        [self.delegate passDeviceCodeInMapView:annotation.deviceCode];
    }
}


- (IBAction)touchRemoveView:(UITapGestureRecognizer*)sender

{
    self.tempView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionLayoutSubviews animations:^{
        self.tempView.alpha  = 0;
        self.tabelView.frame = CGRectMake(self.view.width, 0, self.tabelView.size.width, self.tabelView.size.height);
    }
        completion:^(BOOL finished) {
            [self.view removeFromSuperview];
            self.view = nil;
        }];
}

- (NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section
{
    return [NSString stringWithFormat:NSLocalizedString(@"Total:%lu", "总台数") , (unsigned long)self.annotations.count];
}

- (IBAction)SwipeRightRemoveView:(UISwipeGestureRecognizer*)sender
{
    self.tempView.frame  = CGRectMake(0, 0, self.view.width, self.view.height);
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionLayoutSubviews animations:^{
    self.tempView.alpha  = 0;
    self.tabelView.frame = CGRectMake(self.view.width, 0, self.tabelView.size.width, self.tabelView.size.height);
    }
        completion:^(BOOL finished) {
            [self.view removeFromSuperview];
    self.view = nil;

        }];
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 46.0;
}

- (CGFloat)tableView:(UITableView*)tableView estimatedHeightForHeaderInSection:(NSInteger)section

{
    return 35.0;
}
- (void)tableView:(UITableView*)tableView willDisplayHeaderView:(UIView*)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView* header = (UITableViewHeaderFooterView*)view;
    [header.textLabel setTextColor:RGB(200, 200, 200, 0.9)];
    [header.textLabel setBackgroundColor:RGB(23, 26, 27, 1)];
    UIFont* bodyFont = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote];
    [header.textLabel setFont:bodyFont];
    header.contentView.backgroundColor = RGB(23, 26, 27, 1);
}

// 獲取當前的時間
- (NSString*)getDifferenceDate:(NSString*)lastUseTime
{

    NSString* dateTime = @"";
    if ([lastUseTime isEqualToString:@""] || lastUseTime == nil || [lastUseTime isEqual:[NSNull null]]) {
        dateTime = @"---";
    }
    else {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate* useDate = [formatter dateFromString:lastUseTime]; // 传过来的时间

        // 新加
        NSTimeInterval late = [useDate timeIntervalSince1970] * 1;
        NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0]; // 当前时间
        NSTimeInterval now = [dat timeIntervalSince1970] * 1;

        NSTimeInterval interval = now - late;

        if (interval / 3600 < 1) {
            dateTime = [NSString stringWithFormat:@"%f", interval / 60];
            dateTime = [dateTime substringToIndex:dateTime.length - 7];
            dateTime = [NSString stringWithFormat:@"%@ Minute", dateTime];
        }
        if (interval / 3600 > 1 && interval / 86400 < 1) {
            dateTime = [NSString stringWithFormat:@"%f", interval / 3600];
            dateTime = [dateTime substringToIndex:dateTime.length - 7];
            dateTime = [NSString stringWithFormat:@"%@ Hour", dateTime];
        }
        if (interval / 86400 > 1) {
            dateTime = [NSString stringWithFormat:@"%f", interval / 86400];
            dateTime = [dateTime substringToIndex:dateTime.length - 7];
            dateTime = [NSString stringWithFormat:@"%@ Day", dateTime];
        }
        //  NSTimeInterval interval = [useDate timeIntervalSinceNow]/60/60/24; // 与当前时间比较  也可用使用 [时间(date) timeIntervalSinceDate: 时间（date）]比较;
        //dateTime =[NSString stringWithFormat:@"%d",(int)interval *(-1)];
    }
    return dateTime;
}

@end
