//
//  KCAnnotation.h
//  DataStatistics
//
//  Created by oilklenze on 15/11/3.
//  Copyright © 2015年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface KCAnnotation :MKPointAnnotation;


@property (nonatomic,strong) UIImage  *icon;
@property (nonatomic,strong) NSString *c_name;
@property (nonatomic,strong) NSString *deviceCode;
@property (nonatomic       ) int      running_state;
@property (nonatomic,copy  ) NSString *upload_time;
@property (nonatomic,      ) int      index;
@property (nonatomic,strong) NSNumber * sort;
@end