//
//  AppDelegate.h
//  DataStatistics
//
//  Created by Kang on 15/5/22.
//  Copyright (c) 2015年 YTYangK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <SMS_SDK/SMSSDK.h> //mob SMS
//#import <ShareSDK/ShareSDK.h> //mob分享API
//#import "WXApi.h"

#import "LoginVC.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow               *window;
@property (retain, nonatomic) UIImageView *startpage;
/// 时间..
@property (retain, nonatomic) NSTimer *Timer;
@property (retain, nonatomic) UIButton *RemoveStartPageBtn;
/// 判断是否有通知信息
@property (nonatomic) BOOL isLaunchedByNotification;

/// 判断是否登录
@property (assign, nonatomic)  BOOL isInLogin;

//typedef id (^ReceiveVlueBlock)(id receiveValue);
//@property (strong, nonatomic)  ReceiveVlueBlock  appDelegateReceiveValue;
//- (void)appDelegateReceiveVlueBlock:(ReceiveVlueBlock)block;


/// 管理对象上下文
@property (readonly,strong, nonatomic) NSManagedObjectContext *managedObjectContext;
/// 管理对象模型
@property (readonly,strong, nonatomic) NSManagedObjectModel   *managedObjectModel;
/// 持久性存储协调器
@property (readonly,strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

/// 保存上下文
- (void)saveContext;
/// 文件目录
- (NSURL *)applicationDocumentsDirectory;
@end

