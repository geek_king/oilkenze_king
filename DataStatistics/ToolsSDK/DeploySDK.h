//
//  DeploySDK.h
//  latheDataTrack
//
//  Created by Kang on 16/7/6.
//  Copyright © 2016年 YTYangK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeploySDK : NSObject
/**
 * 推送的汇总方法
 */
- (void)registerPushServiceWithApplication:(nonnull UIApplication *)app :(nonnull NSDictionary *)launch;

/* *
 *  推送的业务层数据处理
 */
- (void)callMethodPuchApplication:(nonnull UIApplication*)application receiveRemoteNot:(nonnull NSDictionary*)userInfo enclosureInfo:(void  (^ _Nullable ) (_Nullable id enclosure)) info;

/**
 * 推送的统计回调
 */
//+  (void)receiveRemoteNotification:(nonnull NSDictionary *)user;
// 设置 访问密匙 对取出的cookie进行反归档处理
- (void)updateSession;
@end
